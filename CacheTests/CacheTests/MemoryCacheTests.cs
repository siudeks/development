﻿namespace CacheTests
{
    using NUnit.Framework;
    using System;
    using System.Linq;
    using System.Collections.Specialized;
    using System.Runtime.Caching;
    using System.Runtime.Caching.Configuration;
    using System.Threading;
    using System.Threading.Tasks;

    [TestFixture]
    public class MemoryCacheTest
    {
        private MemoryCache cache;

        [SetUp]
        public void Init()
        {
            cache = new MemoryCache("mycache");
        }

        [TearDown]
        public void Dispose()
        {
            cache.Dispose();
        }

        [Test]
        public void ShouldFindItemInCacheByProperty()
        {
            var cacheItem = new CacheItem("1", new PersonModel { Name = "Anna", Age = 20 });
            cache.Add(cacheItem, new CacheItemPolicy { });
            var results = cache
                .Select(o => o.Value)
                .OfType<PersonModel>()
                .Where(o => o.Age == 20);

            var cachedItems = results.ToArray();
            Assert.That(cachedItems.Length, Is.EqualTo(1));
            Assert.That(cachedItems[0].Age, Is.EqualTo(20));
        }

        [Test]
        public void ShouldAddItemToCache()
        {
            var expectedValue = new object();
            var item = new CacheItem("item1", expectedValue);
            var itemPolicy = new CacheItemPolicy
            {
            };
            cache.Add(item, itemPolicy);

            var actualValue = cache.Get("item1");
            Assert.That(actualValue, Is.SameAs(expectedValue));
        }

        [Test]
        public void ShouldRemoveTimeoutedItemFromCache()
        {
            var expectedValue = new object();
            var item = new CacheItem("item1", expectedValue);
            var itemPolicy = new CacheItemPolicy
            {
                SlidingExpiration = TimeSpan.FromMilliseconds(10)
            };
            cache.Add(item, itemPolicy);

            Thread.Sleep(100);

            var actualValue = cache.Get("item1");
            Assert.That(actualValue, Is.Null);
        }

        [Test]
        public void ShouldRemoveItemsWhenLackOfMemory()
        {
            var config = new NameValueCollection();
            config.Add("cacheMemoryLimitMegabytes", "1");
            config.Add("pollingInterval", "00:00:00.01");
            var itemsCount = Math.Pow(2, 20); // 1 MB
            var overlimiteditemsCount = itemsCount * 2;
            var cache = new MemoryCache("mycache", config);
            var policy = new CacheItemPolicy { };
            for (int i = 0; i < overlimiteditemsCount; i++)
            {
                cache.Add(new CacheItem(i.ToString(), i), policy);
            }

            var a = cache.GetCount();
            Thread.Sleep(1000);
            Assert.That(cache.GetCount(), Is.EqualTo(itemsCount));
        }

        [Test]
        public void ShouldNotAddDuplicatedItem()
        {
            var item = new object();
            var cache = new MemoryCache("cache");
            var cacheItem = new CacheItem("key", item);
            var cachePolicy = new CacheItemPolicy();
            Assert.That(cache.Add(cacheItem, cachePolicy), Is.True);
            Assert.That(cache.Add(cacheItem, cachePolicy), Is.False);
        }

        [Test]
        public void ShouldAddDuplicatedItemToDifferentRegions()
        {
            var item = new object();
            var cache = new MemoryCache("cache");
            var cacheItemForRegionA = new CacheItem("key", item, "A");
            var cacheItemForRegionB = new CacheItem("key", item, "B");
            var cachePolicy = new CacheItemPolicy();
            Assert.That(cache.Add(cacheItemForRegionA, cachePolicy), Is.True);
            Assert.That(cache.Add(cacheItemForRegionB, cachePolicy), Is.False);
        }

        [Test]
        public void ShouldExpireCacheItems()
        {
            var item = new object();
            var cache = new MemoryCache("cache");
            var cacheItem = new object();
            var cachePolicy = new CacheItemPolicy { AbsoluteExpiration = DateTime.Now.AddMilliseconds(100) };
            cache.Add("key", cacheItem, cachePolicy);

            Assert.That(cache.Contains("key"), Is.True);
            Thread.Sleep(200);
            Assert.That(cache.Contains("key"), Is.False);
        }
    }
}
