﻿using NUnit.Framework;
namespace Strategies
{
    [TestFixture]
    public sealed class LicencePlateParserTests
    {
        [Test]
        public void ShouldRecognizeNumber()
        {
            var assetName = @"assets\LicencePlate-KOL 94W9.png";
            var parser = new LicencePlateParser();

            Assert.That(parser.Parse(assetName), Is.EqualTo("KOL 94W9"));
        }
    }
}
