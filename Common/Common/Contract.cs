﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public static class Contract
    {
        public static void ArgumentRequired(object arg, string argName)
        {
            if (arg == null) throw new ArgumentNullException(argName);

            return;
        }
    }
}
