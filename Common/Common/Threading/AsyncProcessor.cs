﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Common.Threading
{
    internal sealed class AsyncProcessor : IAsyncProcessor
    {
        private const int ProcessingStateWaiting = 1;
        private const int ProcessingStateWorking = 2;
        private const int ProcessingStateWorkingAndRepeat = 3;

        private readonly Action _action;
        private bool _disposed;
        private readonly IExecutor _executor;
        private readonly IAtomic _atomic;

        private int processingState = ProcessingStateWaiting;

        public AsyncProcessor(Action action, IExecutor executor, IAtomic atomic)
        {
            _action = action;
            _executor = executor;
            _atomic = atomic;
        }

        public void Process()
        {
            if (_disposed) return;

            if (!CanExecute()) return;

            _executor.Execute(ProcessingStrategy);
        }

        private bool CanExecute()
        {
            while (true)
            {
                int knownState = Thread.VolatileRead(ref processingState);

                switch (processingState)
                {
                    case ProcessingStateWorking:
                        _atomic.Exchange(ref processingState, ProcessingStateWorkingAndRepeat, knownState);
                        return false;
                    case ProcessingStateWorkingAndRepeat:
                        return false;
                    case ProcessingStateWaiting:
                    default:
                        if (!_atomic.Exchange(ref processingState, ProcessingStateWorking, knownState)) continue;
                        return true;
                }
            }
        }

        private void ProcessingStrategy()
        {
            while (true)
            {
                if (_disposed) return;

                if (ShouldStopProcess()) return;

                _action();
            }
        }

        private bool ShouldStopProcess()
        {
            while (true)
            {
                var knownState = Thread.VolatileRead(ref processingState);
                switch (knownState)
                {
                    case ProcessingStateWorkingAndRepeat:
                        if (!_atomic.Exchange(ref processingState, ProcessingStateWorking, knownState)) continue;
                        return false;
                    case ProcessingStateWorking:
                        if (!_atomic.Exchange(ref processingState, ProcessingStateWaiting, knownState)) continue;
                        return false;
                    default:
                        return true;
                }
            }
        }

        public void Dispose()
        {
            _disposed = true;
        }
    }
}
