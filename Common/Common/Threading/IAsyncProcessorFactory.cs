﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Threading
{
    /// <summary>
    /// Creates an async processor for handling given delegate
    /// </summary>
    public interface IAsyncProcessorFactory
    {
        IAsyncProcessor CreateProcessor(Action action);
    }
}
