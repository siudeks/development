﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Threading
{
    /// <summary>
    /// Allows to process an action in an anonymous thread with single-thread manner.
    /// 
    /// Because Processing will be done every time in a potentially different thread, you should 
    /// take into account refreshing data in the current thread.
    /// </summary>
    public interface IAsyncProcessor : IDisposable
    {
        /// <summary>
        /// Invokes the action in a thread.
        /// </summary>
        /// <remarks>
        /// Async processor can be in one of the following states:
        /// RESTING, EXECUTING, REPEATING
        /// RESTING   : the processor is not executing any thread.
        /// EXECUTNIG : the processor started a thread to execute the action once.
        /// REPEATING : the processor had started a thread to execute the action, and will repeat the action once again when the thread will end execution.
        /// DISPOSED  : the processor will end current operation (if is in EXECUTING or REPEATING state) and cancel any pending operations.
        /// 
        /// State machine
        /// State -> Action -> State
        /// DISPOSED -> Any action -> Exception
        /// RESTING -> Process() -> EXECUTING
        /// EXECUTING -> Process() -> REPEATING
        /// REPEATING -> Process() -> REPEATING
        /// </remarks>
        void Process();
    }
}
