﻿namespace Common.Threading
{
    using System.Threading;
    using System;

    public sealed class ThreadPoolExecutor : IExecutor
    {
        public void Execute(Action action)
        {
            ThreadPool.QueueUserWorkItem(o => action());
        }
    }
}
