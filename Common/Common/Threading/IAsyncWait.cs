﻿namespace Common.Threading
{
    using System;

    /// <summary>
    /// handles known count of callbacks and produces readers for handled callbacks.
    /// </summary>
    public interface IAsyncWait
    {
        /// <summary>
        /// Creates a callback handler for incoming results of asynchronous operation.
        /// </summary>
        Action<TResponse> NewCallbackHandler<TResponse>();

        /// <summary>
        /// Returns reader for already registered callback handler.
        /// </summary>
        /// <param name="callbackHandler">A callback handler created with <see cref="NewCallbackHandler{T}"/> method.</param>
        /// <exception cref="InvalidArgumentException"><paramref name="callbackHandler"/><paramref name="callbackHandler"/> is not created in <see cref="NewCallbackHandler{T}"/> method.</exception>
        Func<TResponse> ResultReader<TResponse>(Action<TResponse> callbackHandler);
    }
}
