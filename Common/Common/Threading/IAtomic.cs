﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Threading
{
    public interface IAtomic
    {
        /// <summary>
        ///     Compares two 32-bit signed integers for equality and, if they are equal, replaces one of the values.
        /// </summary>
        /// <param name="location">The destination, whose value is compared with comparand and possibly replaced.</param>
        /// <param name="value">The value that replaces the destination value if the comparison results in equality.</param>
        /// <param name="comparand">The value that is compared to the value at location.</param>
        /// <returns>True when value pointed by location was replaced, otherwise false.</returns>
        bool Exchange(ref int location, int value, int comparand);
    }
}
