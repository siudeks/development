﻿namespace Common.Threading
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading;

    /// <summary>
    /// Implements <see cref="IAsyncWait"/>
    /// </summary>
    internal class AsyncWait : IAsyncWait
    {
        private int handledTasksCount;
        private int continuationRequiresTasksCount;
        private Action continuation;
        private ConcurrentDictionary<object, object> callbacks = new ConcurrentDictionary<object, object>();

        /// <summary>
        /// <see cref="IAsyncWait.ResultReader{TResponse}"/>
        /// </summary>
        public Func<TResponse> ResultReader<TResponse>(Action<TResponse> callbackHandler)
        {
            object dummyPlaceholder;
            if (!callbacks.TryGetValue(callbackHandler, out dummyPlaceholder)) throw new ArgumentException();

            return () => GetResult<TResponse>(callbackHandler);
        }

        /// <summary>
        /// <see cref="IAsyncWait.NewCallbackHandler{TResponse}"/>
        /// </summary>
        public Action<TResponse> NewCallbackHandler<TResponse>()
        {
            var handle = new CallbackHandle<TResponse>();
            Action<TResponse> result = null;
            result = o => OnCallback(result, o);

            callbacks.TryAdd(result, handle);

            return result;
        }

        /// <summary>
        /// Schedules <paramref name="continuation"/> to be invoked when AsyncVait will handle <paramref name="taskCount"/> responses.
        /// </summary>
        /// </remarks>
        /// <param name="tasksCount">Number of responses required to invoke continuation.</param>
        /// <param name="continuation">A method invoked when required number of tasks will be handled.</param>
        public void OnFinish(int tasksCount, Action continuation)
        {
            var handledTasksCount = tasksCount;
            this.continuation = continuation;
            if (handledTasksCount < tasksCount)
        }

        private TResponse GetResult<TResponse>(Action<TResponse> callback)
        {
            object handler ;
            callbacks.TryGetValue(callback, out handler);
            var callbackHandler = (CallbackHandle<TResponse>) handler;

            return callbackHandler.GetValue();
        }

        private void OnCallback<TValue>(object callback, TValue value)
        {
            object handler;
            callbacks.TryGetValue(callback, out handler);
            var callbackHandle = (CallbackHandle<TValue>)handler;

            var valueReader = Interlocked.Exchange(ref callbackHandle.GetValue, callbackHandle.ReadValue);
            if (valueReader == callbackHandle.ReadValue)
            {
                // Callback already handled
                return;
            }

            callbackHandle.Value = value;

            OnCallbackHandled();
        }

        private void OnCallbackHandled()
        {
            var pendingTasksCount =  Interlocked.Decrement(ref handledTasksCount);

            if (pendingTasksCount != 0)
            {
                return;
            }

            continuation();
        }

        private class CallbackHandle<T>
        {
            private static readonly Func<T> NoValue = () => { throw new InvalidOperationException(); };
            public Func<T> GetValue = NoValue;
            public T Value;
            public T ReadValue()
            {
                return Value;
            }

        }

        public void Handle(object handle)
        {
            throw new NotImplementedException();
        }
    }
}
