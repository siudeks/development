﻿namespace Common.Threading
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAsyncEnumerator
    {
        void Enumerate(Func<IAsyncWait, IEnumerable<IAsyncWait>> enumerator);
    }
}
