﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Threading
{
    public interface IExecutor
    {
        void Execute(Action action);
    }
}
