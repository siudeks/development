﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Common.Threading
{
    sealed class AsyncOp<TResult>
    {
        private readonly static Func<TResult> pendingForResult = delegate { throw new InvalidOperationException(); };
        
        private volatile Func<TResult> resultHandler = pendingForResult;
        
        private readonly Action dequeueTask;

        public AsyncOp(Action dequeueTask)
        {
            this.dequeueTask = dequeueTask;
        }

        public Func<TResult> GetResult
        {
            get
            {
                return resultHandler;
            }

        }

        public void SetResult(TResult result)
        {
            resultHandler = () => result;
        }

    }
}
