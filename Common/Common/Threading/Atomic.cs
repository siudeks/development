﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Common.Threading
{
    public sealed class Atomic : IAtomic
    {
        public static IAtomic Default = new Atomic();

        private Atomic() {}

        public bool Exchange(ref int location, int value, int comparand)
        {
            return (Interlocked.CompareExchange(ref location, value, comparand) == comparand);
        }
    }
}
