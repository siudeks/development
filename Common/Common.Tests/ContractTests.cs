﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Common.Tests
{
    [TestFixture]
    public sealed class ContractTests
    {
        [Test]
        public void ArgumentRequiredTest()
        {
            var argument = new object();

            Contract.ArgumentRequired(argument, "argumentName");
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void ArgumentRequiredTestNullArgument()
        {
            Contract.ArgumentRequired(null, "argumentName");
        }

    }
}
