﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.ConsoleRunner;
using System.Reflection;

namespace Common
{
    class Program
    {
        static void Main(string[] args)
        {
            var assemblyLocation = Assembly.GetExecutingAssembly().Location;
            Runner.Main(new[] {assemblyLocation});
        }
    }
}
