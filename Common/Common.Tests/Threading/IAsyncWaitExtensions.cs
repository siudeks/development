﻿namespace Common.Threading
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public static class IAsyncWaitExtensions
    {
        public static Func<TResult> Result<TResult>(this IAsyncWait wait, Action<Action<TResult>> service)
        {
            var callback = wait.NewCallbackHandler<TResult>();
            var getter = wait.ResultReader(callback);
            service(callback);
            return getter;
        }
    }
}
