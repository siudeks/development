﻿namespace Common.Threading
{
    using System;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public sealed class AsyncWaitTests
    {
        [Test]
        public void ShouldHandleCallback()
        {
            const string expectedValue = "my value";

            var wait = new AsyncWait();

            var callbackHandler = wait.NewCallbackHandler<string>();
            var resultReader = wait.ResultReader(callbackHandler);

            // Incoming message invokes callback
            callbackHandler(expectedValue);

            // So we can now read message.
            var result = resultReader();

            Assert.That(result, Is.EqualTo(expectedValue));
        }

        [Test]
        public void CantGetResultBeforeCallback()
        {
            var wait = new AsyncWait();

            var callbackHandler = wait.NewCallbackHandler<string>();
            var resultReader = wait.ResultReader(callbackHandler);

            Assert.Throws<InvalidOperationException>(() => resultReader());
        }

        [Test]
        public void CantGetResultFromInvalidCallbackHandler()
        {
            var wait = new AsyncWait();

            Action<string> callbackHandler = o => { };

            Assert.Throws<ArgumentException>(() => wait.ResultReader(callbackHandler));
        }

        [Test]
        public void ShouldHandleFinishImmediatelyWhenNoTasksHandled()
        {
            var continuation = MockRepository.GenerateMock<Action>();

            continuation.Expect(o => o.Invoke());

            var wait = new AsyncWait();

            wait.OnFinish(0, continuation);

            continuation.VerifyAllExpectations();
        }

        [Test]
        public void ShouldHandleFinishMultipleTimes()
        {
            var firstContinuation = MockRepository.GenerateMock<Action>();
            var secondContinuation = MockRepository.GenerateMock<Action>();

            firstContinuation.Expect(o => o.Invoke()).Repeat.Once();

            var wait = new AsyncWait();

            var firstCallbackHandler = wait.NewCallbackHandler<string>();
            firstCallbackHandler(string.Empty);
            wait.OnFinish(1, firstContinuation);

            var secondCallbackHandler = wait.NewCallbackHandler<string>();
            secondCallbackHandler(string.Empty);
            wait.OnFinish(1, secondContinuation);

            firstContinuation.VerifyAllExpectations();
            secondContinuation.VerifyAllExpectations();
        }
    }
}
