﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Common.Threading
{
    [TestFixture]
    public sealed class AsyncProcessorFactoryTests
    {
        [Test]
        public void CreateProcessorTest()
        {
            Action action = delegate {};
            var factory = new AsyncProcessorFactory();

            var processor = factory.CreateProcessor(action);

            Assert.IsNotNull(processor);
        }
    }
}
