﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Threading
{
    internal class AtomicStub : IAtomic
    {
        public bool Exchange(ref int location, int value, int comparand)
        {
            if (location != comparand) return false;

            location = value;
            return true;
        }
    }
}
