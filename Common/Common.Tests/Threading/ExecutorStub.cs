﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Threading
{
    internal class ExecutorStub : IExecutor
    {
        public Action LastAction { get; private set; }

        public void Execute(Action action)
        {
            LastAction = action;
        }
    }
}
