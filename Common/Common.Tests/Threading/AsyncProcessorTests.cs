﻿using System;
using NUnit.Framework;
using Rhino.Mocks;

namespace Common.Threading
{
    [TestFixture]
    public sealed class AsyncProcessorTests
    {
        [Test]
        public void ProcessTest()
        {
            // variables
            var repo = new MockRepository();
            var executor = new ExecutorStub();
            var atomic = new AtomicStub();
            Action action = repo.StrictMock<Action>();

            // scenario
            action();

            // test
            repo.ReplayAll();
            var processor = new AsyncProcessor(action, executor, atomic);

            processor.Process();

            executor.LastAction();

            // verify
            repo.VerifyAll();
        }

        [Test]
        public void ProcessTestInvokeTwice()
        {
            // variables
            var repo = new MockRepository();
            var executor = new ExecutorStub();
            var atomic = new AtomicStub();
            Action action = repo.StrictMock<Action>();

            // scenario
            action(); LastCall.Repeat.Twice();

            // test
            repo.ReplayAll();
            var processor = new AsyncProcessor(action, executor, atomic);

            processor.Process();
            processor.Process();

            executor.LastAction();

            // verify
            repo.VerifyAll();
        }

        [Test]
        public void ProcessWhenDisposed()
        {
            // variables
            var repo = new MockRepository();
            var executor = new ExecutorStub();
            var atomic = new AtomicStub();
            Action action = repo.StrictMock<Action>();

            // scenario

            // test
            repo.ReplayAll();
            var processor = new AsyncProcessor(action, executor, atomic);

            processor.Process();
            processor.Dispose();

            executor.LastAction();

            // verify
            repo.VerifyAll();
        }
    }
}
