﻿namespace Common.Threading
{
    using System;
    using System.Collections.Generic;
    using NUnit.Framework;
    using Rhino.Mocks;

    [TestFixture]
    public sealed class AsyncEnumeratorTests
    {
        [Test]
        public void ShouldHandleCallback()
        {
            var asyncProcessor = MockRepository.GenerateMock<IAsyncProcessor>();
            var callback = MockRepository.GenerateMock<Action<string>>();

            IAsyncEnumerator ae = new AsyncEnumerator(asyncProcessor);

        }

        private IEnumerable<IAsyncWait> Loop1(IAsyncWait wait, Action<Action<string>> asyncService, string expectedValue)
        {
            var reader = wait.Result(asyncService);
            yield return wait;
            
            var result = reader();
            Assert.That(result, Is.EqualTo(expectedValue));
        }
    }
}
