﻿using NUnit.Framework;
using System;

namespace LanguageTests
{
    [TestFixture]
    public sealed class GarbageCollectorTests
    {
        [Test]
        public void ShouldNotCollectWhenHandledByEventHandler()
        {
            var sender = new Sender();

            var listener = new Listener(sender);
            var weakHandle = new WeakReference(listener);

            listener = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();

            Assert.IsFalse(weakHandle.IsAlive);
        }

        class Sender
        {
            public event EventHandler SomeNotification;
        }

        class Listener
        {
            public Listener(Sender sender)
            {
                sender.SomeNotification += Sender_SomeNotification;
            }

            private void Sender_SomeNotification(object sender, EventArgs e)
            {
                throw new NotImplementedException();
            }
        }
    }
}
