﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LanguageTests
{
    [TestFixture]
    public sealed class SetTests
    {
        [Test]
        public void ShouldAddNonEquatableInstances()
        {
            var set = new HashSet<object>();
            set.Add(new Object());
            set.Add(new Object());

            Assert.That(set.Count, Is.EqualTo(2));
        }

        [Test]
        public void ShouldNotDuplicateEquatableInstances()
        {
            var set = new HashSet<EquotableClass>(EqualityComparer<EquotableClass>.Default);

            set.Add(new EquotableClass());
            set.Add(new EquotableClass());

            Assert.That(set.Count, Is.EqualTo(1));
        }
    }

    public sealed class EquotableClass : IEquatable<EquotableClass>
    {
        public override int GetHashCode()
        {
            return 0;
        }
        public bool Equals(EquotableClass other)
        {
            return true;
        }
    }
}
