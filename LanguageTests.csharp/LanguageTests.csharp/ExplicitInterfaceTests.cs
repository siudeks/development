﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LanguageTests.csharp
{
    [TestFixture]
    public sealed class ExplicitInterface
    {
        [Test]
        public void ShouldINvokeExplicitInterfaceInInheritanceChain()
        {
            var result = new List<string>();
            var obj = new ChildClass();
            obj.OnParentDispose = () => result.Add("Parent");
            obj.OnChildDispose = () => result.Add("Child");

            ((IDisposable)obj).Dispose();

            Assert.That(result, Is.EquivalentTo(new[] { "Child" }));
        }
    }

    public class ParentClass : IDisposable
    {
        public Action OnParentDispose { get; set; }
        void IDisposable.Dispose()
        {
            OnParentDispose();
        }
    }

    public class ChildClass : ParentClass, IDisposable
    {
        public Action OnChildDispose { get; set; }
        void IDisposable.Dispose()
        {
            OnChildDispose();
        }
    }
}
