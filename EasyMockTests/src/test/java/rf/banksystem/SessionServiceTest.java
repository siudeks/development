package rf.banksystem;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Assert;
import org.junit.Test;

public class SessionServiceTest {

	@Test
	public void ShouldLogOnWhenInvalidCredentials() {
		final String userName = "my username";
		final String password = "my password"; 
		
		IEmployeeDataService storage = EasyMock.createMock(IEmployeeDataService.class);
		
		EasyMock.expect(storage.Exists(userName, password))
			.andReturn(true);
		SessionService service = new SessionService(storage);
		
		EasyMock.replay(storage);
		
		Assert.assertTrue(service.logOn(userName, password));
		EasyMock.verify(storage);
	}

	@Test
	public void CantLogOnWhenInvalidCredendials() {
		Assert.fail();
	}
	
	@Test
	public void ShouldLockAccountAfterInvalidLogon() {
		Assert.fail();
	}
	
	@Test
	public void ShouldUnlockAccountAfterConfiguredTime() {
		Assert.fail();
	}
}
