package rf.banksystem;

public final class SessionService {

	private final IEmployeeDataService dataService;
	public SessionService (IEmployeeDataService dataService){
		this.dataService = dataService;
	}
	public boolean logOn(String userName, String password) {
		return dataService.Exists(userName, password);
	}
	
}
