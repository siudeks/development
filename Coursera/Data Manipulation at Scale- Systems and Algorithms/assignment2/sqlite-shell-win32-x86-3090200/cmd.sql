select b1, b2, sum(v1*v2) v from (
select b1, b2, v1, v2 from
(

select a.row b1, b.col b2, A.value v1, B.value v2 from

(
SELECT 'q' as row, 'washington' as col, 1 as value
UNION all
SELECT 'q' as row, 'taxes' as col, 1 as value
UNION all
SELECT 'q' as row, 'treasury' as col, 1 as value
union all
SELECT docid as row, term as col, count as value FROM frequency
) A,

(
SELECT 'washington' row, 'q' col, 1 as value 
UNION all
SELECT 'taxes' row, 'q' col, 1 as value 
UNION all
SELECT 'treasury' row, 'q' col, 1 as value 
union all
SELECT term as row, docid as col, count as value FROM frequency
) B

on A.col=B.row
)
where b1 < b2

)
where b1='q' or b2='q'
group by b1, b2
order by v desc
;
