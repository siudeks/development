import sys
import json

states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}

def asDictionary(afinnfile):
    scores = {} # initialize an empty dictionary
    for line in afinnfile:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    return scores


def toPoints(line,sent_dict):
    result = 0
    for w in line.split():
        result += sent_dict.get(w, 0)
    return result


def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])

    tweets = []
    for line in tweet_file:
        j = json.loads(line)
        text = j.get("text", None)
        if text is None: continue
        place = j.get("place", None)
        if place is None: continue
        country_code = place.get('country_code')
        if country_code is None:
            continue
        if country_code != "US":
            continue
        full_name = place.get('full_name')
        if full_name is None: continue
        state_name = full_name.split()[-1]
        tweets.append((text, state_name))

    states_happiness = {}
    sent_dict = asDictionary(sent_file)
    for text_state in tweets:
        text_sent = 0
        for word in text_state[0].split():
            text_sent += toPoints(word, sent_dict)
        state = text_state[1]
        happiness = states_happiness.get(state, 0) + text_sent
        states_happiness[state] = happiness

    state_name = "??"
    max_happiness = -100
    for state in states_happiness:
        if states_happiness[state] <= max_happiness: continue
        if not states.has_key(state): continue

        state_name = state
        max_happiness = states_happiness[state]
    print state_name


if __name__ == '__main__':
    main()
