import sys
import json


def asDictionary(afinnfile):
    scores = {} # initialize an empty dictionary
    for line in afinnfile:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    return scores


def toPoints(line,sent_dict):
    result = 0
    for w in line.split():
        result += sent_dict.get(w, 0)
    return result


def main():
    sent_file = open(sys.argv[1])
    tweetFile = open(sys.argv[2])

    tweets = []
    for line in tweetFile:
        j = json.loads(line)
        tweets.append(j)

    sentDict = asDictionary(sent_file)
    for t in tweets:
        s = t["text"]
        v = toPoints(s, sentDict)
        print str(v)





if __name__ == '__main__':
    main()
