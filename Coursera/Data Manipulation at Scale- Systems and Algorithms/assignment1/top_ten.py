import sys
import json

def asDictionary(afinnfile):
    scores = {} # initialize an empty dictionary
    for line in afinnfile:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    return scores


def toPoints(line,sent_dict):
    result = 0
    for w in line.split():
        result += sent_dict.get(w, 0)
    return result


def main():
    tweet_file = open(sys.argv[1])

    hashtag_count = {}
    for line in tweet_file:
        j = json.loads(line)
        entities = j.get("entities")
        if entities is None: continue;
        hashtags = entities["hashtags"]
        if len(hashtags) == 0: continue
        for hashtag in hashtags:
            hashtag_text = hashtag["text"]
            count = hashtag_count.get(hashtag_text, 0) + 1
            hashtag_count[hashtag_text] = count

    o = sorted(hashtag_count.items(), key=lambda x: x[1], reverse=True)
    for x in o[0:10]:
        print x[0] + " " + str(x[1])

if __name__ == '__main__':
    main()
