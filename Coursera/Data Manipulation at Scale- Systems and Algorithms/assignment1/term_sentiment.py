import sys
import json


def asDictionary(afinnfile):
    scores = {} # initialize an empty dictionary
    for line in afinnfile:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    return scores


# returns sentiment points for given termo or 0, if the ter is not-sentiment
def toPoints(word,sent_dict):
    return sent_dict.get(word, 0)

newTerms = {}
def store(word, points):
    value = newTerms.get(word,(0,0))
    newValue = (value[0]+1, value[1]+points)
    newTerms[word] = newValue

def main():
    data_File = open(sys.argv[1])
    tweetFile = open(sys.argv[2])

    tweets = []
    for line in tweetFile:
        j = json.loads(line)
        tweets.append(j)

    sentDict = asDictionary(data_File)
    for t in tweets:
        line = t.get("text", None)
        if line is None: continue

        linePoints = 0
        lineWords = 0
        for word in line.split():
            linePoints += toPoints(word, sentDict)
            lineWords += 1

        if linePoints == 0: continue

        for word in line.split():
            v = toPoints(word, sentDict)
            if v != 0: continue
            store(word, linePoints / lineWords)


if __name__ == '__main__':
    main()

    for it in newTerms:
        val = newTerms[it]
        print it + " " + str(float(val[1]) / float(val[0]))
