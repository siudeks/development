import sys
import json


def asDictionary(afinnfile):
    scores = {} # initialize an empty dictionary
    for line in afinnfile:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    return scores


# returns sentiment points for given termo or 0, if the ter is not-sentiment
def toPoints(word,sent_dict):
    return sent_dict.get(word, 0)

def main():
    tweet_file = open(sys.argv[1])

    tweets = []
    for fileRow in tweet_file:
        json_obj = json.loads(fileRow)
        text = json_obj.get("text", None)
        if text is None: continue
        tweets.append(text)

    words = {}
    terms_count = 0
    for t in tweets:
        for word in t.split():
            terms_count += 1
            wordCount = words.get(word, 0) + 1
            words[word] = wordCount

    for word in words:
        print word + " " + str(float(words[word]) / terms_count)


if __name__ == '__main__':
    main()
