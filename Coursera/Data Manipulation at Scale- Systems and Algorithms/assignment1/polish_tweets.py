import sys
import json


def asDictionary(afinnfile):
    scores = {} # initialize an empty dictionary
    for line in afinnfile:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    return scores


def toPoints(line,sent_dict):
    result = 0
    for w in line.split():
        result += sent_dict.get(w, 0)
    return result


def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])

    tweets = []
    for line in tweet_file:
        j = json.loads(line)
        text = j.get("text", None)
        if text is None: continue
        place = j.get("place", None)
        if place is None: continue
        country_code = place.get('country_code')
        if country_code is None: continue
        if country_code != "PL": continue
        tweets.append(text)

    sent_dict = asDictionary(sent_file)
    for text in tweets:
        text_sent = 0
        for word in text.split():
            text_sent += toPoints(word, sent_dict)

        print str(text_sent) + " " + text



if __name__ == '__main__':
    main()
