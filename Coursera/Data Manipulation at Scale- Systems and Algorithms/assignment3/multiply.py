import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    name = record[0]
    x = record[1]
    y = record[2]
    v = record[3]
    if name=="a":
        for i in range (0,5):
            mr.emit_intermediate((x,i),(y*2,v))
    if name=="b":
        for i in range (0,5):
            mr.emit_intermediate((i,y),(x*2+1,v))

def reducer(key, list_of_values):
    array = [0,0,0,0,0,0,0,0,0,0]
    for v in list_of_values:
        index = v[0]
        v = v[1]
        array[index]=v

    total = 0
    for i in range(0, 5):
        total += array[i*2] * array[i*2+1]

    mr.emit((key[0], key[1], total))


# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
