import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    words = record
    person = words[0]
    friend = words[1]
    if person < friend:
        key = person + ":" + friend
    else:
        key = friend + ":" + person
    mr.emit_intermediate(key, (person, friend))

def reducer(record_id, list_of_values):

    if len(list_of_values) > 1: return

    person = list_of_values[0][0]
    friend = list_of_values[0][1]
    mr.emit((friend, person))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
