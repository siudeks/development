import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    words = record
    mr.emit_intermediate(words[0], 1)

def reducer(record_id, list_of_values):
    count = 0
    for it in list_of_values:
        count += it

    mr.emit((record_id, count))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
