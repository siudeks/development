import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    words = record
    record_id = record[1]
    mr.emit_intermediate(record_id, words)

def reducer(record_id, list_of_values):
    order_items = []
    line_items = []
    for i in list_of_values:
        if i[0]=="order": order_items.append(i)
        if i[0]=="line_item": line_items.append(i)

    for o in order_items:
        for l in line_items:
            v = o + l
            mr.emit(v)

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
