package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5)
    val t2 = Fork(Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5), Leaf('d', 4), List('a', 'b', 'd'), 9)
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a', 'b', 'd'))
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 3)))
    assert(makeOrderedLeafList(List(('t', 2), ('e', 5), ('t', 4))) === List(Leaf('e', 5), Leaf('t', 6)))
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4)))
  }

  test("decode a very short text") {
    new TestTrees {
      assert(decode(t1, List(0, 1) ) === "ab".toList)
    }
  }

  test("t2 decode all combinations") {
    new TestTrees {
      assert(decode(t2, List(0, 0) ) === "a".toList)
      assert(decode(t2, List(0, 1) ) === "b".toList)
      assert(decode(t2, List(1) ) === "d".toList)
      assert(decode(t2, List(0, 0, 0, 1) ) === "ab".toList)
      assert(decode(t2, List(0, 0, 1) ) === "ad".toList)
      assert(decode(t2, List(0, 0, 0, 1, 1) ) === "abd".toList)
      assert(decode(t2, List(1, 0, 1, 0, 0) ) === "dba".toList)
    }
  }

  test("encode a very short text") {
    new TestTrees {
      assert(encode(t1)("ab".toList) === List(0, 1))
    }
  }

  test("t2 encode a very short text") {
    new TestTrees {
      assert(encode(t2)("a".toList) === List(0, 0))
      assert(encode(t2)("b".toList) === List(0, 1))
      assert(encode(t2)("d".toList) === List(1))
      assert(encode(t2)("da".toList) === List(1, 0, 0))
      assert(encode(t2)("dba".toList) === List(1, 0, 1, 0, 0))
      assert(encode(t2)("adb".toList) === List(0,0,1,0,1))
      assert(encode(t2)("bad".toList) === List(0,1,0,0,1))
    }
  }

  test("t1 decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("t2 decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t2, encode(t2)("ab".toList)) === "ab".toList)
    }
  }

  test("times implemented") {
    assert(times(List('a', 'b', 'c', 'c', 'c', 'b')).size === 3)
    assert(times(List('a', 'b', 'c', 'c', 'c', 'b'))(0) === ('a', 1))
    assert(times(List('a', 'b', 'c', 'c', 'c', 'b'))(1) === ('b', 2))
    assert(times(List('a', 'b', 'c', 'c', 'c', 'b'))(2) === ('c', 3))
  }
  
  test("Mystery solved") {
    assert(decodedSecret === "huffmanestcool".toList)
  }
  
  test("combine of a singleton or nil") {
    assert(combine(Nil) === Nil)
    assert(combine(List(new Leaf('a', 1))) === List(new Leaf('a', 1)))
  }
  
  test("convert implemented") {
    new TestTrees {
    	assert(convert(t1) === List(('a', List(0)),('b',List(1))))
    	assert(convert(t2) === List(('a', List(0,0)),('b',List(0,1)),('d', List(1))))
    }
  }

  test("quickEncode implemented") {
    new TestTrees {
    	assert(quickEncode(t1)(List('a','b')) === List(0,1))
    	assert(quickEncode(t2)(List('a','b','d')) === List(0,0,0,1,1))
    	assert(quickEncode(t2)(List('a','b','d','a','b','d')) === List(0,0,0,1,1,0,0,0,1,1))
    }
  }
  
  test("'createCodeTree(someText)' gives an optimal encoding, the number of bits when encoding 'someText' is minimal") {
    new TestTrees {
      assert(createCodeTree("caaabbccc".toList)=== Fork(Fork(Leaf('b',2),Leaf('a',3),List('b','a'),5),Leaf('c',4),List('b','a','c'),9) )
    }
  }
}
