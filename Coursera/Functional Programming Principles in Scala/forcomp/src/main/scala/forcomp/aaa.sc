package forcomp

import Anagrams._

object aaa {

  val b = sentenceOccurrences(List("aab"))        //> b  : forcomp.Anagrams.Occurrences = List((a,2), (b,1))
  combinations(b)                                 //> res0: List[forcomp.Anagrams.Occurrences] = List(List(), List((a,1)), List((b
                                                  //| ,1)), List((a,2)), List((a,1), (b,1)), List((a,2), (b,1)))

  // solution
  def isValidList(list: List[Occurrences]): Boolean =
    if (list.isEmpty) true
    else if (!dictionaryByOccurrences.contains(list.head)) false
    else isValidList(list.tail)                   //> isValidList: (list: List[forcomp.Anagrams.Occurrences])Boolean

  def findOccurrences(left: Occurrences, result: Occurrences): List[Occurrences] =
    if (left.isEmpty) List(result)
    else {
      combinations(left)
      .filter(!_.isEmpty)
      .foldLeft(List[Occurrences]())((acc: List[Occurrences], o: Occurrences) => acc ++ findOccurrences(subtract(left, o), result ++ o))
      }                                           //> findOccurrences: (left: forcomp.Anagrams.Occurrences, result: forcomp.Anagra
                                                  //| ms.Occurrences)List[forcomp.Anagrams.Occurrences]





  def visitCombinations(left: Occurrences, result: List[Occurrences])(visitor: (List[Occurrences]) => Unit): Int =
  	if (left.isEmpty) {
  	  visitor(result)
  		1
  	}
  	else combinations(left)
    .filter(!_.isEmpty)
    .filter(dictionaryByOccurrences.contains(_))
    .foldLeft(0)((acc, o) => acc + visitCombinations(subtract(left, o), result :+ o)(visitor))
                                                  //> visitCombinations: (left: forcomp.Anagrams.Occurrences, result: List[forcomp
                                                  //| .Anagrams.Occurrences])(visitor: List[forcomp.Anagrams.Occurrences] => Unit)
                                                  //| Int


  def finalTest1 = {
    val total = new scala.collection.mutable.Queue[List[Occurrences]]
    def visitor (current: List[Occurrences]) =
      total += current
  
  
    visitCombinations(sentenceOccurrences(List("Yes", "man")), Nil)(visitor)
    
    val sentences = new scala.collection.mutable.Queue[Sentence]
    
    total.foreach(sublists(_, Nil, sentences += _))
    
    sentences
  }                                               //> finalTest1: => scala.collection.mutable.Queue[forcomp.Anagrams.Sentence]
  
  def sublists(list: List[Occurrences], current: List[Word], visitor: (List[Word]) => Unit) : Int = {

    if (list.isEmpty) {
      visitor(current)
      1
    }
    else dictionaryByOccurrences.apply(list.head)
    	.foldLeft(0)((acc, o) => sublists(list.tail, current :+ o, visitor))
  }                                               //> sublists: (list: List[forcomp.Anagrams.Occurrences], current: List[forcomp.
                                                  //| Anagrams.Word], visitor: List[forcomp.Anagrams.Word] => Unit)Int
     

finalTest1 .toList                                //> res1: List[forcomp.Anagrams.Sentence] = List(List(as, en, my), List(as, my,
                                                  //|  en), List(en, as, my), List(en, my, as), List(my, as, en), List(my, en, as
                                                  //| ), List(my, sane), List(my, Sean), List(man, yes), List(say, men), List(men
                                                  //| , say), List(yes, man), List(sane, my), List(Sean, my))
}