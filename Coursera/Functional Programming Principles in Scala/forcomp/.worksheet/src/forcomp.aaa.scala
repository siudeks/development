package forcomp

import Anagrams._

object aaa {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(92); 

  val b = sentenceOccurrences(List("aab"));System.out.println("""b  : forcomp.Anagrams.Occurrences = """ + $show(b ));$skip(18); val res$0 = 
  combinations(b);System.out.println("""res0: List[forcomp.Anagrams.Occurrences] = """ + $show(res$0));$skip(193); 

  // solution
  def isValidList(list: List[Occurrences]): Boolean =
    if (list.isEmpty) true
    else if (!dictionaryByOccurrences.contains(list.head)) false
    else isValidList(list.tail);System.out.println("""isValidList: (list: List[forcomp.Anagrams.Occurrences])Boolean""");$skip(326); 

  def findOccurrences(left: Occurrences, result: Occurrences): List[Occurrences] =
    if (left.isEmpty) List(result)
    else {
      combinations(left)
      .filter(!_.isEmpty)
      .foldLeft(List[Occurrences]())((acc: List[Occurrences], o: Occurrences) => acc ++ findOccurrences(subtract(left, o), result ++ o))
      };System.out.println("""findOccurrences: (left: forcomp.Anagrams.Occurrences, result: forcomp.Anagrams.Occurrences)List[forcomp.Anagrams.Occurrences]""");$skip(370); 





  def visitCombinations(left: Occurrences, result: List[Occurrences])(visitor: (List[Occurrences]) => Unit): Int =
  	if (left.isEmpty) {
  	  visitor(result)
  		1
  	}
  	else combinations(left)
    .filter(!_.isEmpty)
    .filter(dictionaryByOccurrences.contains(_))
    .foldLeft(0)((acc, o) => acc + visitCombinations(subtract(left, o), result :+ o)(visitor));System.out.println("""visitCombinations: (left: forcomp.Anagrams.Occurrences, result: List[forcomp.Anagrams.Occurrences])(visitor: List[forcomp.Anagrams.Occurrences] => Unit)Int""");$skip(396); 


  def finalTest1 = {
    val total = new scala.collection.mutable.Queue[List[Occurrences]]
    def visitor (current: List[Occurrences]) =
      total += current
  
  
    visitCombinations(sentenceOccurrences(List("Yes", "man")), Nil)(visitor)
    
    val sentences = new scala.collection.mutable.Queue[Sentence]
    
    total.foreach(sublists(_, Nil, sentences += _))
    
    sentences
  };System.out.println("""finalTest1: => scala.collection.mutable.Queue[forcomp.Anagrams.Sentence]""");$skip(295); 
  
  def sublists(list: List[Occurrences], current: List[Word], visitor: (List[Word]) => Unit) : Int = {

    if (list.isEmpty) {
      visitor(current)
      1
    }
    else dictionaryByOccurrences.apply(list.head)
    	.foldLeft(0)((acc, o) => sublists(list.tail, current :+ o, visitor))
  };System.out.println("""sublists: (list: List[forcomp.Anagrams.Occurrences], current: List[forcomp.Anagrams.Word], visitor: List[forcomp.Anagrams.Word] => Unit)Int""");$skip(27); val res$1 = 
     

finalTest1 .toList;System.out.println("""res1: List[forcomp.Anagrams.Sentence] = """ + $show(res$1))}
}
