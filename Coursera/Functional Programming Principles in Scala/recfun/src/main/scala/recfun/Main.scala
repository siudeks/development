package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0) 1
    else if (c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {

    def count(item: Char) =
      if (item == '(') 1
      else if (item == ')') -1
      else 0

    def balancing(current: Int, chars: List[Char]): Boolean = {
      if (current < 0) false
      else if (chars.isEmpty) (current == 0)
      else balancing(current + count(chars.head), chars.tail)
    }

    balancing(0, chars)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {

    def changing(current: Int, count: Int, coins: List[Int]): Int = {
      if (coins.isEmpty) 0
      else if (current + count * coins.head == money) 1
      else if (current + count * coins.head > money) 0
      else changing(current, count + 1, coins) + changing(current + count * coins.head, 0, coins.tail)
    }

    changing(0, 0, coins)
  }
}
