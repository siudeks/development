package objsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TweetSetSuite extends FunSuite {
  trait TestSets {
    val set1 = new Empty
    val set2 = set1.incl(new Tweet("a", "a body", 20))
    val set3 = set2.incl(new Tweet("b", "b body", 20))
    val c = new Tweet("c", "c body", 7)
    val d = new Tweet("d", "d body", 9)
    val set4c = set3.incl(c)
    val set4d = set3.incl(d)
    val set5 = set4c.incl(d)
  }

  def asSet(tweets: TweetSet): Set[Tweet] = {
    var res = Set[Tweet]()
    tweets.foreach(res += _)
    res
  }

  def size(set: TweetSet): Int = asSet(set).size

  test("filter: on empty set") {
    new TestSets {
      assert(size(set1.filter(tw => tw.user == "a")) === 0)
    }
  }

  test("filter: a on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.user == "a")) === 1)
    }
  }

  test("filter: 20 on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.retweets == 20)) === 2)
    }
  }

  test("union: set4c and set4d") {
    new TestSets {
      assert(size(set4c.union(set4d)) === 4)
    }
  }

  test("union: set4c and set4c") {
    new TestSets {
      assert(size(set4c) === 3)
      assert(size(set4c.union(set4c)) === 3)
    }
  }

  test("union: with empty set (1)") {
    new TestSets {
      assert(size(set5.union(set1)) === 4)
    }
  }

  test("union: with empty set (2)") {
    new TestSets {
      assert(size(set1.union(set5)) === 4)
    }
  }

  test("descending: set5") {
    new TestSets {
      val trends = set5.descendingByRetweet
      assert(!trends.isEmpty)
      assert(trends.head.user == "a" || trends.head.user == "b")
    }
  }
  
  test("test1") {
  }
  
  test("test2") {
    //assert(size(TweetReader.allTweets.filter(o => o.retweets == 205))>0)
    //assert(!GoogleVsApple.appleTweets.filter(o => o.retweets == 205).isEmpty)
    
    val count = TweetReader.gizmodoTweets.length+
        TweetReader.techCrunchTweets.length+
        TweetReader.engadgetTweets.length+
        TweetReader.amazondealsTweets.length+
        TweetReader.cnetTweets.length+
        TweetReader.gadgetlabTweets.length+
        TweetReader.mashableTweets.length-5;
    
    assert(size(TweetReader.allTweets)===count)
        
    var counter: Integer = 0;
    TweetReader.allTweets.filter(o => 
      {
        counter = counter+1;
        true
      })

    assert(count === counter)
  }

  test("filter and union: tweets with 321 and 205 retweets") {
    val items = TweetReader.allTweets.filter(o => o.retweets == 321 || o.retweets == 205)
    assert(size(items) === 2)
  }

  test("filter and trending: tweets with 321 and 205 retweets") {
    //val items = GoogleVsApple.trending.s
    //assert(size(items) === 2)
  }
}
