﻿using Akka.Actor;
using Akka.Configuration;
using Akka.Persistence;
using Akka.TestKit.NUnit;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace TechnologyTests
{
    [TestFixture]
    public sealed class PersistentActorShould
    {
        [Test]
        public async Task RestoreStateAfterRestart()
        {
            var config = ConfigurationFactory.ParseString(@"akka {
persistence {
  journal {
    # Path to the journal plugin to be used
    plugin = 'akka.persistence.journal.inmem'
    # In-memory journal plugin.
    inmem {
      # Class name of the plugin.
      class = 'Akka.Persistence.Journal.MemoryJournal, Akka.Persistence'
      # Dispatcher for the plugin actor.
      plugin-dispatcher = 'akka.actor.default-dispatcher'
      }
    }
  }
}");

            using (var system = ActorSystem.Create("sys1", config))
            {
                var actor = system.ActorOf<MyPersistentActor>("abc");
                var result = await actor.Ask<int>(null);
                Assert.That(result, Is.EqualTo(1));
            }
        }

        public sealed class MyPersistentActor : PersistentActor
        {
            public override string PersistenceId { get; }
            private int i;

            public MyPersistentActor()
            {
                Receive(o => base.te ++i)
            }

            protected override bool ReceiveCommand(object message)
            {
                return false;
            }

            protected override bool ReceiveRecover(object message)
            {
                return false;
            }
        }
    }
}

