﻿using Akka.Actor;
using NUnit.Framework;
using System.Threading.Tasks;

namespace TechnologyTests
{
    [TestFixture]
    public class ActorShould
    {
        [Test]
        public async Task QueueNextInvocationWhenHandlingThePreviousOne()
        {
            using (var system = ActorSystem.Create("local"))
            {
                var actor = system.ActorOf<MyActor>();
                var response = await actor.Ask<string>("abc");

                Assert.That(response, Is.EqualTo("Echo abc"));
            }
        }

        public class MyActor : ReceiveActor
        {
            public MyActor()
            {
                Receive<string>(o => Sender.Tell("Echo " + o, Self));
                Receive<int>(o => Sender.Tell(o + 1, Self));
            }
        }

    }
}
