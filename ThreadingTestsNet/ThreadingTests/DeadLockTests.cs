﻿namespace Net.Siudek.Learning
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    [TestFixture]
    public sealed class DeadLockTests
    {
        [Test]
        public void ShouldLockThread()
        {
            var syncRoot1 = new object();
            var syncRoot2 = new object();

            var thread1 = new Thread(o => DoubleLock(syncRoot1, syncRoot2));
            var thread2 = new Thread(o => DoubleLock(syncRoot2, syncRoot1));

            thread1.Start();
            thread2.Start();

            Assert.That(thread1.Join(100), Is.False);
        }

        private static void DoubleLock(object obj1, object obj2)
        {
            lock (obj1)
            {
                Thread.Sleep(10);
                lock (obj2)
                {
                    // business logic
                }
            }
        }
    }
}
