﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.Siudek.Learning
{
    [TestFixture]
    public sealed class DispatcherTests
    {
        [Test]
        public void QueueTasksTest()
        {
            var i = 0l;
            var thread = new Thread(o =>
            {
                var current = Interlocked.Read(ref i);
                if (current < 10)
                {
                };
            });

            thread.Join();
            Thread.MemoryBarrier();
            Assert.That(i, Is.EqualTo(10));
        }
    }
}
