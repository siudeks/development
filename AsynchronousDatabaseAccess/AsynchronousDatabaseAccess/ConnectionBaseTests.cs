﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace AsynchronousDatabaseAccess
{
    [TestFixture]
    public sealed class ConnectionBaseTests
    {
        [SetUp]
        public void Init()
        {
        }

        [Test]
        public void MainTest()
        {
        }

        protected abstract void WormUp();

        protected abstract void MainTest(int count);
    }
}
