﻿using System;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using NUnit.Framework;

namespace AsynchronousDatabaseAccess
{
    [TestFixture]
    public class DatabaseAccess
    {
        const string databaseName = "TemporaryDatabaseForAsynchronousDatabaseAccess";

        string connectionString;
        string entityProviderString;
        const int count = 10000 - 1;

        public DatabaseAccess()
        {
            var sqlBuilder = new SqlConnectionStringBuilder();

            // Set the properties for the data source.
            sqlBuilder.DataSource = @"localhost\sqlexpress";
            sqlBuilder.InitialCatalog = databaseName;

            sqlBuilder.IntegratedSecurity = true;
            //sqlBuilder.UserID = "sa";
            //sqlBuilder.Password = "";

            sqlBuilder.AsynchronousProcessing = true;
            sqlBuilder.MaxPoolSize = 100;
            sqlBuilder.Pooling = true;
            sqlBuilder.MultipleActiveResultSets = true;

            // Build the SqlConnection connection string.
            connectionString = sqlBuilder.ToString();

            // Initialize the EntityConnectionStringBuilder.
            var entityBuilder = new EntityConnectionStringBuilder();

            //Set the provider name.
            entityBuilder.Provider = "System.Data.SqlClient";

            // Set the provider-specific connection string.
            entityBuilder.ProviderConnectionString = connectionString;
            entityBuilder.Metadata = @"res://*/DataModel.csdl|res://*/DataModel.ssdl|res://*/DataModel.msl";


            entityProviderString = entityBuilder.ToString();

        }

        [SetUp]
        public void Init()
        {
            using (var context = new DataModelContainer(entityProviderString))
            {
                context.CreateDatabase();
            }
        }

        [TearDown]
        public void Done()
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                conn.ChangeDatabase("tempdb");

                var sqlcmd = String.Format("alter database {0} set single_user with rollback immediate \n DROP DATABASE {0}", databaseName);
                using (var cmd = new SqlCommand(sqlcmd, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        [Test]
        public void InsertSyncEF()
        {
            int id = 0;
            AddCustomerEF(id++); // warm up

            Stopwatch sw = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                AddCustomerEF(id++);
            }
            sw.Stop();
            var time = sw.ElapsedMilliseconds;
            Debug.Print(time.ToString());
        }

        [Test]
        public void InsertSyncSqlCommand()
        {
            int id = 0;
            AddCustomerSC(id++); // warm up

            Stopwatch sw = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                AddCustomerSC(id++);
            }
            sw.Stop();
            var time = sw.ElapsedMilliseconds;
            Debug.Print(time.ToString());
        }

        [Test]
        public void InsertPooledAsync()
        {
            int id = 0;
            
            CountdownEvent ce = new CountdownEvent(1);

            AddCustomerPooledAsync(id++, ce);
            ce.Wait();

            ce = new CountdownEvent(count);

            Stopwatch sw = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                AddCustomerPooledAsync(id++, ce);
            }
            sw.Stop();
            var time1 = sw.ElapsedMilliseconds;
            ce.Wait();
            var time2 = sw.ElapsedMilliseconds;
            Debug.Print(time2.ToString());
        }

        [Test]
        public void InsertAsync()
        {

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                int id = 0;

                CountdownEvent ce = new CountdownEvent(1);
                AddCustomerAsync(id++, ce, conn); // warm up
                ce.Wait();

                ce = new CountdownEvent(count);

                var sw = Stopwatch.StartNew();

                for (int i = 0; i < count; i++)
                {
                    AddCustomerAsync(id++, ce, conn);
                }
                ce.Wait();

                var time2 = sw.ElapsedMilliseconds;
                Debug.Print(time2.ToString());
            }
        }

        private void AddCustomerEF(int id)
        {
            using (var context = new DataModelContainer(entityProviderString))
            {
                var c = context.CreateObject<Customer>();
                c.Id = id;
                c.Name = Guid.NewGuid().ToString();
                context.CustomerSet.AddObject(c);
                context.SaveChanges();
            }
        }

        private static SqlCommand CreateSqlCommand(int id)
        {
            const string insertSql = "insert into dbo.CustomerSet (id, name) values({0}, '{1}')";

            var customerName = Guid.NewGuid().ToString();
            var sql = String.Format(insertSql, id, customerName);
            var cmd = new SqlCommand(sql);

            return cmd;
        }

        private void AddCustomerSC(int id)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                var cmd = CreateSqlCommand(id);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
            }
        }

        private void AddCustomerPooledAsync(int id, CountdownEvent ce)
        {
            var customerName = Guid.NewGuid().ToString();

            var conn = new SqlConnection(connectionString);
            conn.Open();

            var cmd = CreateSqlCommand(id);
            cmd.Connection = conn;
            
            var state = new { Connection = conn, Command = cmd, Countdown = ce };

            cmd.BeginExecuteNonQuery(AddCustomerPooledCallback, state);
        }

        private void AddCustomerPooledCallback(IAsyncResult ar)
        {
            dynamic context = ar.AsyncState;
            var conn = (SqlConnection)context.Connection;
            var cmd = (SqlCommand)context.Command;
            var ce = (CountdownEvent)context.Countdown;

            try
            {
                cmd.EndExecuteNonQuery(ar);
            }
            finally
            {
                ce.Signal();
                cmd.Dispose();
                conn.Dispose();
            }
        }

        private void AddCustomerAsync(int id, CountdownEvent ce, SqlConnection conn)
        {
            var customerName = Guid.NewGuid().ToString();

            var cmd = CreateSqlCommand(id);
            cmd.Connection = conn;
            var state = new { Command = cmd, Countdown = ce };

            cmd.BeginExecuteNonQuery(AddCustomerCallback, state);
        }

        private void AddCustomerCallback(IAsyncResult ar)
        {
            dynamic context = ar.AsyncState;
            var cmd = (SqlCommand)context.Command;
            var ce = (CountdownEvent)context.Countdown;

            try
            {
                cmd.EndExecuteNonQuery(ar);
            }
            finally
            {
                ce.Signal();
                cmd.Dispose();
            }
        }
    }
}
