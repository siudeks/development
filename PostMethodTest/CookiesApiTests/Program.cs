﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;

namespace CookiesApi
{
    class Program
    {
        static void Main()
        {
            string baseAddress = "http://localhost:9000/";

            // Create HttpCient and make a request to api/values 
            var handler = new HttpClientHandler { CookieContainer = new CookieContainer(), UseCookies = true };
            handler.CookieContainer.Add(new Uri(baseAddress), new Cookie("cookieName", "cookieValue", "/"));

            var client = new HttpClient(handler);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));



            var response = client.GetAsync(baseAddress + "test/api/cookies").Result;

            Console.WriteLine(response);
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);

            Console.ReadLine();
        }


    }
}
