﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PostMethodClient
{
    public class Product
    {
        public string Name { get; set; }
        public DateTime Expiry { get; set; }
        public string[] Sizes { get; set; }
    }
}