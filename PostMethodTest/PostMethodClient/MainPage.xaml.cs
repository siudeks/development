﻿using Contracts;
using Newtonsoft.Json;
using System;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Reactive.Linq;
using System.Net.Browser;
using System.Diagnostics;

namespace PostMethodClient
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string baseAddress = "http://localhost:9000/";

            HttpWebRequest.RegisterPrefix("http://", WebRequestCreator.ClientHttp);
            HttpWebRequest.RegisterPrefix("https://", WebRequestCreator.ClientHttp);

            // Create HttpCient and make a request to api/values 
            var handler = new HttpClientHandler { CookieContainer = new CookieContainer(), UseCookies = true };
            handler.CookieContainer.Add(new Uri(baseAddress), new Cookie("cookieName", "cookieValue", "/"));

            var client = new HttpClient(handler);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            client.GetAsync(baseAddress + "test/api/cookies")
                .ContinueWith(o => { return o.Result.Content.ReadAsStringAsync(); })
                .Unwrap().ContinueWith(o =>
                {
                    Debug.WriteLine(o.Result);
                });
        }
    }
}
