﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contracts
{
    public sealed class AddResult
    {
        public int Result { get; set; }
    }
}
