﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contracts
{
    public sealed class AddRequest
    {
        public int Value1 { get; set; }
        public int Value2 { get; set; }
    }
}
