﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostMethodTest
{
    public class Product
    {
        public string Name { get; set; }
        public DateTime Expiry { get; set; }
        public string[] Sizes { get; set; }
    }
}