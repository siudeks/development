﻿using Newtonsoft.Json;
using PostMethodTest;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebApiClient.Desktop
{
    class Program
    {
        static void Main(string[] args)
        {
            var product = new Product();
            product.Name = "Apple";
            product.Expiry = new DateTime(2008, 12, 28);
            product.Sizes = new string[] { "Small" };

            string json = JsonConvert.SerializeObject(product);
            Console.WriteLine(json);
            var client = new WebClient();
            client.Headers["Content-Type"] = "application/json; charset=utf-8";
            // client.Headers["Content-Type"] = "application/json";
            client.UploadDataCompleted += client_UploadDataCompleted;
            client.UploadDataAsync(new Uri("http://localhost:63498/api/values"), "POST", Encoding.UTF8.GetBytes(json));
            Console.WriteLine("Press ENTER to exit");
            Console.ReadLine();
        }

        static void client_UploadDataCompleted(object sender, UploadDataCompletedEventArgs e)
        {
            Console.WriteLine(e.Error);
        }
    }
}
