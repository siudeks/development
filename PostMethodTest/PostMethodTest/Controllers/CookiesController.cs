﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace PostMethodTest.Controllers
{
    public class CookiesController : ApiController
    {
        public IEnumerable<string> Get()
        {
            var cookies = HttpContext.Current.Request.Cookies;
            for (int i = 0; i < HttpContext.Current.Request.Cookies.Count; i++)
			{
                yield return string.Format("{0}:{1}", cookies[i].Name, cookies[i].Value);
			}
            yield return "END";
        }
    }
}