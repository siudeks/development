package jsontest;

import java.util.concurrent.Callable;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import argo.allegro.ItemCategories.CategorySearchResult;

@Path("SessionService")
public final class WebApi {

    final static CategoryInfo[] nullResult = new CategoryInfo[0];

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(value = "")
    public CategorySearchData getCategoryList() throws Exception {

	CategorySearchData result = new CategorySearchData(); 
	
	if (name == "") return null;

	result = Service.AllegroService.itemSearch(categoryId, name);
	
	return result;
    }
}
