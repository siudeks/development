﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Reactive.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().Initialize();
            Console.WriteLine("Press enter to exit.");
            Console.ReadLine();
        }

        private void Initialize()
        {
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8070));
            socket.Listen(10);
            socket.BeginAccept(OnClient, socket);
        }

        private void OnClient(IAsyncResult ar)
        {
            var listener = (Socket)ar.AsyncState;
            var socket = listener.EndAccept(ar);

            var source = socket.GetSocketData<string>(100, Encoding.UTF8.GetString);
            source.Subscribe(OnData);
        }

        private void OnData(string text)
        {
            Console.WriteLine(text);
        }
    }
}
