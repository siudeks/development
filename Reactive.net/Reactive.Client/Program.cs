﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Reactive.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8070));

            while (true)
            {
                var line = Console.ReadLine();
                if (String.IsNullOrEmpty(line)) break;

                socket.Send(Encoding.UTF8.GetBytes(line));
            }

            Console.WriteLine("Press enter to exit CLIENT.");
            Console.ReadLine();
        }
    }
}
