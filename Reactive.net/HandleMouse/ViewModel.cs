﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Reactive.Linq;
//using System.Windows;

//namespace HandleMouse
//{
//    public class ViewModel
//    {
//        private string title;
//        private bool hasStuff;

//        public ViewModel()
//        {
//            IObservable<bool> initPredicate = this.ObserveProperty(x => x.Title)
//                     .StartWith(this.Title).Select(x => !string.IsNullOrEmpty(x)); ;
//            IObservable<bool> predicate = this.ObserveProperty(x => x.HasStuff)
//                     .StartWith(this.HasStuff);
//            SomeCommand = new ReactiveCommand(initPredicate, false);
//            SomeCommand.AddPredicate(predicate);
//            SomeCommand.CommandExecutedStream.Subscribe(x =>
//            {
//                MessageBox.Show("Command Running");
//            });
//        }

//        public ReactiveCommand SomeCommand { get; set; }

//        public string Title
//        {
//            get
//            {
//                return this.title;
//            }
//            set
//            {
//                RaiseAndSetIfChanged(ref this.title, value, () => Title);
//            }
//        }

//        public bool HasStuff
//        {
//            get
//            {
//                return this.hasStuff;
//            }
//            set
//            {
//                RaiseAndSetIfChanged(ref this.hasStuff, value, () => HasStuff);
//            }
//        }

//    }
//}
