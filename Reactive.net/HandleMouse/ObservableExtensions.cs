﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reactive.Linq;

namespace HandleMouse
{
    public static class ObservableExtensions
    {
        public static IObservable<TValue> ObserveProperty<T, TValue>(
            this T source,
             Expression<Func<T, TValue>> propertyExpression
        )
            where T : INotifyPropertyChanged
        {
            return source.ObserveProperty(propertyExpression, false);
        }

        public static IObservable<TValue> ObserveProperty<T, TValue>(
            this T source,
            Expression<Func<T, TValue>> propertyExpression,
            bool observeInitialValue
        )
            where T : INotifyPropertyChanged
        {
            var memberExpression = (MemberExpression)propertyExpression.Body;

            var getter = propertyExpression.Compile();

            var observable = from evt in Observable
                    .FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>
                    (h => source.PropertyChanged += h,
                    h => source.PropertyChanged -= h)
                             where evt.EventArgs.PropertyName == memberExpression.Member.Name
                             select getter(source);

            if (observeInitialValue)
                return observable.Merge(Observable.Return(getter(source)));

            return observable;
        }
    }
}
