﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HandleMouse
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public IReactiveCommand ReactiveCommand { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            var mouseIsUp = Observable.Merge(
                Observable.FromEventPattern<MouseButtonEventArgs>(this, "MouseUp").Select(o => true),
                Observable.FromEventPattern<MouseButtonEventArgs>(this, "MouseDown").Select(o => false)
                ).StartWith(true);

            ReactiveCommand = new ReactiveCommand(mouseIsUp, false);

            DataContext = this;
        }

        void MainWindow_MouseUp(object sender, MouseButtonEventArgs e)
        {
        }
    }
}
