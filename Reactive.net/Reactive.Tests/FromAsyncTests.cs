﻿namespace Reactive
{
    using NUnit.Framework;
    using System.Collections.Concurrent;
    using System.Reactive.Linq;
    using System.Threading.Tasks;

    [TestFixture]
    public sealed class FromAsyncTests
    {
        [Test]
        public async void ShouldHandleAsyncNotification()
        {
            var observable = Observable.FromAsync(() =>
                {
                    return Task<int>.Factory.StartNew(() =>
                        {
                            return 1;
                        });
                });

            var sum = await observable.Sum();

            Assert.That(sum, Is.EqualTo(1));
        }
    }
}
