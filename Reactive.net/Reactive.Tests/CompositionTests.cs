﻿using NUnit.Framework;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reactive
{
    [TestFixture]
    public sealed class CompositionTests
    {
        [Test]
        public void ShouldComposeMultipleValues()
        {
            var component1 = new Component();
            var component2 = new Component();
            var source1 = Observable.FromEventPattern<ComponentErrorEventArgs>(component1, "ComponentError");
            var source2 = Observable.FromEventPattern<ComponentErrorEventArgs>(component2, "ComponentError");
            var source = Observable
                .Merge(source1, source2)
                .Scan(new ConcurrentDictionary<object, string>(),
                (a, ep) =>
                {
                    string value;
                    string error = ep.EventArgs.Error;
                    var sender = ep.Sender;
                    if (error == null)
                        a.TryRemove(sender, out value);
                    else
                        a.AddOrUpdate(sender, error, (o, s) => error);
                    return a;
                });

            var actual = Enumerable.Empty<string>();

            source
                .ObserveOn(Scheduler.Immediate)
                .Subscribe(o => actual = o.Values.ToArray());

            CollectionAssert.AreEquivalent(actual, new string[] { });

            component1.RaiseError("error1");
            CollectionAssert.AreEquivalent(actual, new[] { "error1" });

            component2.RaiseError("error2");
            CollectionAssert.AreEquivalent(actual, new[] { "error1", "error2" });

            component1.RaiseError(null);
            CollectionAssert.AreEquivalent(actual, new[] { "error2" });

            component2.RaiseError("error3");
            CollectionAssert.AreEquivalent(actual, new[] { "error3" });

            component2.RaiseError(null);
            CollectionAssert.AreEquivalent(actual, new string[] { });
        }

        [Test]
        public void ShouldSubscribeUniqueValues()
        {
            var given = new[] { 1, 2, 2, 3, 1, 2, 2, 3 };
            var expected = new[] { 1, 2, 3 };
            var result = given.ToObservable()
                .Distinct()
                .ToArray().Wait();

            CollectionAssert.AreEqual(expected, result);
        }

        [Test]
        public void ShouldSubscribeUniqueSequencesValues()
        {
            var given = new[] { 1, 2, 2, 3, 1, 2, 2, 3 };
            var expected = new[] { 1, 2, 3, 1, 2, 3 };
            var result = given.ToObservable()
                .DistinctUntilChanged()
                .ToArray().Wait();

            CollectionAssert.AreEqual(expected, result);
        }
    }


    class ComponentErrorEventArgs : EventArgs
    {
        public string Error { get; set; }
    }

    class Component
    {
        public event EventHandler<ComponentErrorEventArgs> ComponentError;
        public void RaiseError(string error)
        {
            ComponentError(this, new ComponentErrorEventArgs { Error = error });
        }
    }
}
