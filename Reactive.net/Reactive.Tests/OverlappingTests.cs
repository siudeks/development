﻿using NUnit.Framework;
using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;

namespace Reactive
{
    [TestFixture]
    public sealed class OverlappingTests
    {
        [Test]
        public void ShouldNotOverlapStepsOnObservableWithThreadPool()
        {
            var counter = new CountdownEvent(2);

            var source = Observable.Range(1, 2, TaskPoolScheduler.Default);

            source.Subscribe(
                o =>
                {
                    counter.Signal();
                });
            counter.Wait();
        }
    }
}
