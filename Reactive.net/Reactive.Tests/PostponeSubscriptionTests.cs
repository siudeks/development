﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;

namespace Reactive
{
    [TestFixture]
    public sealed class PostponeSubscriptionTests
    {
        [Test]
        public void ShouldSubscribBePostponed()
        {
            var initializer = new BehaviorSubject<bool>(false);
            var source = Substitute.For<IObservable<int>>();
            var listener = Substitute.For<IObserver<int>>();

            source
                .PostponeSubscription(initializer)
                .Subscribe(listener);

            source.DidNotReceiveWithAnyArgs().Subscribe(null);
        }

        [Test]
        public void ShouldSubscribeBeStarted()
        {
            var initializer = new BehaviorSubject<bool>(true);
            var source = Substitute.For<IObservable<int>>();
            var listener = Substitute.For<IObserver<int>>();

            source
                .PostponeSubscription(initializer)
                .Subscribe(listener);

            source.ReceivedWithAnyArgs().Subscribe(null);
        }

        [Test]
        public void ShouldSubscribeBeTriggered()
        {
            var initializer = new BehaviorSubject<bool>(false);
            var source = Substitute.For<IObservable<int>>();
            var listener = Substitute.For<IObserver<int>>();

            source
                .PostponeSubscription(initializer)
                .Subscribe(listener);

            initializer.OnNext(true);

            source.ReceivedWithAnyArgs().Subscribe(null);
        }
    }

    public static class A
    {
        public static IObservable<TSource> PostponeSubscription<TSource>(this IObservable<TSource> source, IObservable<bool> initializer)
        {
            return Observable.Create<TSource>(o =>
            {
                var disposer = new CompositeDisposable();
                disposer.Add(
                    initializer.Where(_ => _).DistinctUntilChanged().Subscribe(b =>
                    {
                        disposer.Add(source.Subscribe(o));
                    }));
                return disposer;
            });
        }
    }
}
