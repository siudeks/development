﻿namespace Reactive
{
    using NUnit.Framework;
    using System.Reactive.Subjects;
    using System;
    using System.Reactive.Linq;
    using System.Threading.Tasks;
    using System.Reactive.Concurrency;
    using System.Threading;
    using System.Reactive;
    using System.Reactive.Disposables;
    using System.Diagnostics;

    [TestFixture]
    public sealed class AsyncTests
    {
        [Test]
        public void ShouldNotWaitWhenAsyncSubscriotionFinished()
        {
            // Test fails to show 'issue' relate dot async methods invoked in subscription.
            var waitHandle = new ManualResetEvent(false);
            var subject = new Subject<int>();
            subject
                .ObserveOn(Scheduler.CurrentThread)
                .Subscribe(async o =>
            {
                if (o == 1) await Task.Delay(1000);
                if (o == 2) waitHandle.Set();
            });

            subject.OnNext(1);
            subject.OnNext(2);
            // async operation should postpone next OnNext subscription, because the first is pending - but in async wau
            Assert.That(waitHandle.WaitOne(100), Is.True);
        }

        [Test]
        public void ShouldHandleAsyncSubscriptionWithoutOverlapping()
        {
            var waitHandle = new ManualResetEvent(false);
            Func<int, Task> asyncMethod = async o =>
                {
                    if (o == 1) await Task.Delay(1000);
                    if (o == 2) waitHandle.Set();
                };
            var subject = new Subject<int>();
            subject
                .ObserveOn(Scheduler.CurrentThread)
                .SubscribeAsync(async o =>
                {
                    if (o == 1) await Task.Delay(1000);
                    if (o == 2) waitHandle.Set();
                });

            subject.OnNext(1);
            subject.OnNext(2);
            Assert.That(waitHandle.WaitOne(100), Is.False);
            Assert.That(waitHandle.WaitOne(), Is.True);

            waitHandle.Reset();
            subject.OnNext(1);
            subject.OnNext(2);
            // at the end, it should be finished.
            Assert.That(waitHandle.WaitOne(), Is.True);
        }


        public void ShouldhandleAsyncSubscriptionFinished()
        {
            var waitHandle = new ManualResetEvent(false);
            Func<int, Task> asyncMethod = async o =>
            {
                if (o == 1) await Task.Delay(1000);
                if (o == 2) waitHandle.Set();
            };
            var subject = new Subject<int>();
            subject
                .ObserveOn(Scheduler.CurrentThread)
                .SubscribeAsync(async o =>
                {
                    if (o == 1) await Task.Delay(1000);
                    if (o == 2) waitHandle.Set();
                });

            subject.OnNext(1);
            subject.OnNext(2);
            Assert.That(waitHandle.WaitOne(100), Is.False);

            // at the end, it should be finished.
            Assert.That(waitHandle.WaitOne(), Is.True);

        }
    }

    public static class ObservableEx
    {
        public static IDisposable SubscribeAsync<T>(this IObservable<T> source, Func<T, Task> onNext)
        {
            var disposable = new CompositeDisposable();
            var asyncMethodFinished = new Subject<Unit>();
            disposable.Add(asyncMethodFinished);

            var subscriptionDisposer = source
                .Zip(asyncMethodFinished.StartWith(Unit.Default), (o1, o2) => o1)
                .Subscribe(o =>
                {
                    onNext(o).ContinueWith(t => asyncMethodFinished.OnNext(Unit.Default));
                });

            disposable.Add(subscriptionDisposer);

            return disposable;
        }
    }
}
