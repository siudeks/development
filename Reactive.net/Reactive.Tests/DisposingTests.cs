﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reactive
{
    [TestFixture]
    public sealed class DisposingTests
    {
        [Test]
        public void ShouldDisposeSource()
        {
            var d = new BooleanDisposable();
            var source = Observable.Using(
                () => d,
                (dd) => new ObservableSource(dd));

            source.Subscribe();

            Assert.That(d.IsDisposed, Is.True);
        }

        [Test]
        public void ShouldCompositeDisposableDisposeChildren()
        {
            var disposer = new CompositeDisposable();

            var child1 = new BooleanDisposable();
            disposer.Add(child1);
            disposer.Dispose();

            var child2 = new BooleanDisposable();
            disposer.Add(child2);

            Assert.That(child1.IsDisposed, Is.True);
            Assert.That(child2.IsDisposed, Is.True);
        }
    }

    class ObservableSource : IObservable<string>
    {
        public ObservableSource(IDisposable d)
        {

        }

        public IDisposable Subscribe(IObserver<string> observer)
        {
            observer.OnCompleted();
            return Disposable.Empty;
        }
    }

}
