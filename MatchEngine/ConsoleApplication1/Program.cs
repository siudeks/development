﻿using Matching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadLine();

            var expected = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 2, Count = 1, Price = 1, Side = SideCategory.Buy }};

            var engine = new Engine(new ResultHandler());

            engine.Add(1, 1, 1, SideCategory.Sell);
            engine.Add(2, 1, 1, SideCategory.Buy);
        }
    }
}
