﻿namespace Matching
{
    using Cudafy;
    using Cudafy.Host;
    using Cudafy.Translator;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public sealed class MatchResult
    {
        public long AskId;
        public long BidId;
    }

    public sealed class Engine : IEngine
    {
        private const int size = 100;

        private List<OrderEntryHolder> orders = new List<OrderEntryHolder>();
        //private readonly OrderEntryHolder[] bidEntries = new OrderEntryHolder[1];
        //private readonly OrderEntryHolder[] askEntries = new OrderEntryHolder[1];
        //private readonly MatchResult[] matchingResult = new MatchResult[1];

        private readonly GPGPU gpu;

        private readonly IResultHandler resultHandler;
        public Engine(IResultHandler resultHandler)
        {
            this.resultHandler = resultHandler;

            //var module = CudafyTranslator.Cudafy();
            //gpu = CudafyHost.GetDevice(CudafyModes.Target, CudafyModes.DeviceId);
            //gpu.LoadModule(module);
        }

        public void Add(OrderEntry entry)
        {
            orders.Add(new OrderEntryHolder(entry));

            var prepared = orders
                .Where(o => o. ???
            // we have at least two entries.
            Data().AsParallel().ForAll(o => 
                {
                    o.Item3.AskId = o.Item1.Id;
                    o.Item3.BidId = o.Item2.Id;
                });

            resultHandler.OnResult(
                new[] {
                new ProcessResult(1, 1, 1, SideCategory.Sell),
                new ProcessResult(2, 1, 1, SideCategory.Buy)});

        }

        private IEnumerable<Tuple<OrderEntryHolder, OrderEntryHolder, MatchResult>> Data()
        {
            yield return Tuple.Create(askEntries[0], bidEntries[0], matchingResult[0]);
        }

        public void Add(OrderEntry[] entries)
        {
            foreach (var item in entries)
            {
                Add(item);
            }
        }

        public void Dispose()
        {
        }
    }
}
