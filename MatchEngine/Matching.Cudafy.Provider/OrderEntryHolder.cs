﻿namespace Matching
{
    public class OrderEntryHolder
    {
        private static long id;

        public readonly long Id;
        public int CustomerId;
        public int Count;
        public int Price;
        public SideCategory Side;

        public OrderEntryHolder(OrderEntry entry)
        {
            Id = ++id;
            CustomerId = entry.CustomerId;
            Count = entry.Count;
            Price = entry.Price;
        }
    }
}
