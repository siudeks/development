﻿namespace Matching.Cudafy.Tests
{
    using NUnit.Framework;

    [TestFixture]
    public class EngineTests : EngineBaseTests
    {
        protected override IEngine CreateEngine(IResultHandler resultHandler)
        {
            return new Engine(resultHandler);
        }
    }
}
