﻿namespace Matching
{
    using NSubstitute;
    using NUnit.Framework;
    using System.Linq;

    [TestFixture]
    public class EngineTests
    {
        protected Engine CreateEngine(IResultHandler handler)
        {
            return new Engine(handler);
        }

        [Test]
        public void ShouldMatchSides()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);

            engine.Test();

        }
    }
}
