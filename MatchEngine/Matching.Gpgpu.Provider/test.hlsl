struct VS_OUTPUT
{
	int customerId; 
	float4 Diffuse    : COLOR0;
	float2 TextureUV  : TEXCOORD0;
};

VS_OUTPUT RenderSceneVS( float4 vPos : POSITION,
						float3 vNormal : NORMAL,
						float2 vTexCoord0 : TEXCOORD,
						uniform int nNumLights,
						uniform bool bTexture,
						uniform bool bAnimate )
{
	VS_OUTPUT Output;
	return Output;    
}

[numthreads(1,1,1)]
void main()
{
}