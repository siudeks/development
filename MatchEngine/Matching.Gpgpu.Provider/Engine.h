namespace Matching
{

	public ref class Engine : public IEngine
	{
	public:
		Engine(IResultHandler ^handler);
		~Engine();
		virtual void Add(array<Matching::OrderEntry ^> ^entries);
		virtual void Add(Matching::OrderEntry ^entry);
		void Test();
		void Test1();
	};
}