#pragma once
#include "Stdafx.h"
#include "Engine.h"
#include "PixelShader.h"
#include "atlbase.h"
#include "D3D11.h"
#pragma comment(lib, "d3d11.lib")


namespace Matching
{
	Engine::Engine(IResultHandler ^handler)
	{
	}

	Engine::~Engine()
	{
	}

	void Engine::Add(array<OrderEntry ^> ^entries)
	{
	}

	void Engine::Add(OrderEntry ^entry)
	{
	}

	void Engine::Test()
	{
		HRESULT hr = S_OK;
		ID3D11Device* device = nullptr;
		D3D_FEATURE_LEVEL level;
		ID3D11DeviceContext* context = nullptr;

		CComPtr<ID3D11PixelShader> m_pPixelShader;
		hr = D3D11CreateDevice(nullptr, 
			D3D_DRIVER_TYPE::D3D_DRIVER_TYPE_NULL, 
			nullptr,
			0, 
			nullptr, 
			0, 
			D3D11_SDK_VERSION, 
			&device, 
			&level, 
			&context);
		hr = device->CreatePixelShader(g_psshader, sizeof(g_psshader), nullptr, &m_pPixelShader);
	}

	void Engine::Test1()
	{
		ID3D11DeviceContext *g_pImmediateContext;
		ID3D11Device* g_pd3dDevice;

		D3D_FEATURE_LEVEL featureLevel[]
		{
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
			D3D_FEATURE_LEVEL_9_3,
			D3D_FEATURE_LEVEL_9_2,
			D3D_FEATURE_LEVEL_9_1,
		};

		auto swapChainDesc = DXGI_SWAP_CHAIN_DESC();
		ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
		swapChainDesc.BufferCount = 1;
		swapChainDesc.BufferDesc.Width = width;
		swapChainDesc.BufferDesc.Height = height;
		swapChainDesc.BufferDesc.Format = DXGI_FORMAT_UNKNOWN;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.OutputWindow = hWnd;
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;
		swapChainDesc.Windowed = TRUE;

		D3D11CreateDeviceAndSwapChain(NULL,
			D3D_DRIVER_TYPE::D3D_DRIVER_TYPE_HARDWARE,
			NULL,
			D3D11_CREATE_DEVICE_FLAG::D3D11_CREATE_DEVICE_SINGLETHREADED,
			featureLevel,
			5,
			D3D11_SDK_VERSION, 
			&swapChain, ..., &g_pd3dDevice, ..., &g_pImmediateContext);
	}

}