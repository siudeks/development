﻿namespace Matching
{
    using NUnit.Framework;

    public sealed class MatchingEnginePerformanceTests : EnginePerformanceBaseTests
    {
        protected override IEngine CreateEngine(IResultHandler listener)
        {
            return new Engine(listener);
        }
    }
}
