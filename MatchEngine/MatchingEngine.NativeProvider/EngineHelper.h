#pragma once;

namespace Matching {

	struct EngineHelper
	{
		static int SellMatches(int sellValue, int buyValue)
		{
			if (buyValue >= sellValue) 
				return sellValue;
			else 
				return 0;
		}

		static int BuyMatches(int buyValue, int sellValue)
		{
			return SellMatches(sellValue, buyValue);
		}

		static bool CanRemove(OrderEntryHolder o)
		{
			return o.Count == 0;
		}
	};
}