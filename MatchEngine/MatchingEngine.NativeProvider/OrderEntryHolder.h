#pragma once

namespace Matching 
{
	struct OrderEntryHolder
	{
		int CustomerId;
		int Count;
		int Price;
		int Timestamp;

		bool operator < (const OrderEntryHolder& rhs)
		{
			if (Price < rhs.Price) return true;
			if (Price == rhs.Price && Timestamp < rhs.Timestamp) return true;

			return false;
		}
	};
}