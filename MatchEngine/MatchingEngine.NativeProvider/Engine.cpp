#include "stdafx.h"
#include "Engine.h"
#include "EngineHelper.h"
#include "OrderEntryHolder.h"
#include <queue>
#include <functional>
#include <list>
#include "OrderProcessed.h"

namespace Matching {

	Engine::Engine(IResultHandler ^handler)
	{
		this->handler = handler;
		this->buyOrders = new std::list<OrderEntryHolder>();
		this->sellOrders = new std::list<OrderEntryHolder>();
	}

	Engine::~Engine()
	{
		int itemsCount = buyOrders->size() + sellOrders->size();
		auto results = gcnew array<ProcessResult>(itemsCount);
		int i = 0;
		for (auto iterator=buyOrders->begin(); iterator!=buyOrders->end(); ++iterator, i++)
			results[i] = * gcnew ProcessResult(iterator->CustomerId, iterator->Count, iterator->Price, SideCategory::Buy);
		for (auto iterator=sellOrders->begin(); iterator!=sellOrders->end(); ++iterator, i++)
			results[i] = * gcnew ProcessResult(iterator->CustomerId, iterator->Count, iterator->Price, SideCategory::Sell);

		handler->OnFinish(results);

		delete this->buyOrders;
		delete this->sellOrders;
	}

	typedef std::function<bool (int)> Filter;

	void Engine::Add(array<OrderEntry^>^ entries)
	{
		auto length = entries->Length;
		for (int i = 0; i < length; i++)
		{
			Add(entries[i]);
		}
	}

	void Engine::Add(OrderEntry ^e)
	{
		auto entry = new OrderEntryHolder();
		entry->CustomerId = e->CustomerId;
		entry->Count = e->Count;
		entry->Price = e->Price;
		entry->Timestamp = ++entryCounter;

		auto side = e->Side;

		// we need to prepare universal strategy for both side possible values.
		std::list<OrderEntryHolder> * orders;
		std::list<OrderEntryHolder> * ordersToMatch;
		OrderCategory orderCategory;
		OrderCategory matchedCategory;
		int (* ordersValueComparer) (int, int);
		if (side == SideCategory::Buy)
		{
			orders = buyOrders;
			ordersToMatch = sellOrders;
			orderCategory = OrderCategory::Buy;
			matchedCategory = OrderCategory::Sell;
			ordersValueComparer = &EngineHelper::BuyMatches;
		}
		else
		{
			orders = sellOrders;
			ordersToMatch = buyOrders;
			orderCategory = OrderCategory::Sell;
			matchedCategory = OrderCategory::Buy;
			ordersValueComparer = &EngineHelper::SellMatches;
		}

		auto results = new std::vector<OrderProcessed>();
		for (auto iterator=ordersToMatch->begin(); iterator!=ordersToMatch->end(); ++iterator)
		{
			if (entry->Count == 0) break;

			// avoid matching order for the same customer
			if (iterator->CustomerId == entry->CustomerId) continue;

			// avoid matching orders for non-compatible prices
			int matchedPrice = ordersValueComparer(entry->Price, iterator->Price);
			if (!matchedPrice) continue;

			int matchedCount = std::min(iterator->Count, entry->Count);

			auto matchedOrder = new OrderProcessed();
			matchedOrder->CustomerId = iterator->CustomerId;
			matchedOrder->Count = matchedCount;
			matchedOrder->Value = matchedPrice;
			matchedOrder->Category = matchedCategory;
			results->push_back(*matchedOrder);

			auto order = new OrderProcessed();
			order->CustomerId = entry->CustomerId;
			order->Count = matchedCount;
			order->Value = matchedPrice;
			order->Category = orderCategory;
			results->push_back(*order);
			
			iterator->Count -= matchedCount;
			entry->Count -= matchedCount;
		}

		ordersToMatch->remove_if(&EngineHelper::CanRemove);

		if (int resultsCount = results->size())
		{
			auto matchingResults = gcnew array<ProcessResult>(resultsCount);

			int i = 0;
			for (auto iterator=results->begin(); iterator!=results->end(); ++iterator, i++)
			{
				matchingResults[i] = * gcnew ProcessResult(iterator->CustomerId, iterator->Count, iterator->Value, (SideCategory) (iterator->Category));
			}

			handler->OnResult(matchingResults);

		}

		// if the current entry didn't fit completely with existing orders, we need to add it to waiting orders queue.
		if (entry->Count==0) return;

		orders->push_back(*entry);
		sellOrders->sort();
	};
}
