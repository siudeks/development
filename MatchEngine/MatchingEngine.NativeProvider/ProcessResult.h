#pragma once

namespace MatchEngineLib {
	public value struct ProcessResult
	{
		int CustomerId;
		int Count;
		int Value;
		int Side;
	};
}