#pragma once

namespace Matching 
{
	enum OrderCategory
	{
		Sell = SideCategory::Sell,
		Buy = SideCategory::Buy
	};

	struct OrderProcessed
	{
		int CustomerId;
		int Count;
		int Value;
		OrderCategory Category;
	};
}