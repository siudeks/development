// MatchEngine.Lib.h
#pragma once
#include <list>
#include "OrderEntryHolder.h"

using namespace System;
using namespace Matching;
using namespace System::Collections::Generic;

namespace Matching {

	public ref class Engine : public IEngine
	{
	public:
		Engine(IResultHandler ^handler);
		~Engine();
		virtual void Add(OrderEntry ^entry);
		virtual void Add(array<OrderEntry^>^ entries);
	private:
		IResultHandler ^handler;
		// Engine contains two containers for two sides of orders.
		std::list<OrderEntryHolder> *buyOrders;
		std::list<OrderEntryHolder> *sellOrders;
		int entryCounter;
	};
}
