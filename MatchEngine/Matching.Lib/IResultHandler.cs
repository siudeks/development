﻿namespace Matching
{
    public interface IResultHandler
    {
        void OnResult(ProcessResult[] result);
        void OnFinish(ProcessResult[] result);
    }
}
