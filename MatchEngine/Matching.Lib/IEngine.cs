﻿namespace Matching
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Allows to register client orders.
    /// </summary>
    public interface IEngine : IDisposable
    {
        /// <summary>
        /// Registers a customer offer.
        /// </summary>
        void Add(OrderEntry entry);

        /// <summary>
        /// Registers bulk customers offers.
        /// </summary>
        void Add(OrderEntry[] entries);
    }
}
