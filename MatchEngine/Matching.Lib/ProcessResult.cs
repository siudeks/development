﻿namespace Matching
{
    using System.Diagnostics;

    [DebuggerDisplay("Id:{CustomerId}, Count:{Count}, Side:{Side}, Value:{Value}")]
    public struct ProcessResult
    {
        public ProcessResult(int customerId, int count, int value, SideCategory side)
        {
            CustomerId = customerId;
            Count = count;
            Price = value;
            Side = side;
        }

        public int CustomerId;
        public int Count;
        public SideCategory Side ;
        public int Price;
    }
}
