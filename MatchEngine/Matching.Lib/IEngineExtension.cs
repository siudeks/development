﻿namespace Matching
{

    public static class IEngineExtension
    {
        public static void Add(this IEngine engine, int customerId, int count, int price, SideCategory side)
        {
            var entry = new OrderEntry
                {
                    CustomerId = customerId,
                    Count = count,
                    Price = price,
                    Side = side
                };
            engine.Add(entry);
        }
    }
}
