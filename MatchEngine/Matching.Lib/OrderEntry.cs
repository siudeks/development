﻿namespace Matching
{
    public class OrderEntry
    {
        public int CustomerId;
        public int Count;
        public int Price;
        public SideCategory Side;
    }
}
