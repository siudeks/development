﻿namespace Matching
{
    using NUnit.Framework;

    public sealed class MatchingEngineTests : EngineBaseTests
    {
        protected override IEngine CreateEngine(IResultHandler resultHandler)
        {
            return new Engine(resultHandler);
        }
    }
}
