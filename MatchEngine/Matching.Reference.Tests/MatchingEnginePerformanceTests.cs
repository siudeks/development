﻿namespace Matching
{
    using NUnit.Framework;

    public sealed class MatchingEnginePerformanceTests : EnginePerformanceBaseTests
    {
        protected override IEngine CreateEngine(IResultHandler resultHandler)
        {
            return new Engine(resultHandler);
        }
    }
}
