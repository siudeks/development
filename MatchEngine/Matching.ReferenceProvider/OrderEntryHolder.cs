﻿namespace Matching
{
    public sealed class OrderEntryHolder
    {
        public int CustomerId;
        public int Count;
        public int Price;
        public SideCategory Side;
        public int Timestamp;
    }
}
