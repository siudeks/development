﻿namespace Matching
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    sealed class OrderEntryHolderComparer : IComparer<OrderEntryHolder>
    {
        public int Compare(OrderEntryHolder x, OrderEntryHolder y)
        {
            if (x.Price < y.Price) return -1;
            if (x.Price > y.Price) return 1;
            return x.Timestamp - y.Timestamp;
        }
    }
}
