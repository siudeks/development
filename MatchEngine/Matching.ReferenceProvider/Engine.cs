﻿namespace Matching
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public sealed class Engine : IEngine, IDisposable
    {
        static Func<int, int, int> SellMatcher = (int sellValue, int buyValue) =>
        {
            if (buyValue >= sellValue)
                return sellValue;
            else
                return 0;
        };

        static Func<int, int, int> BuyMatcher = (int buyValue, int sellValue) =>
        {
            return SellMatcher(sellValue, buyValue);
        };

        static bool CanRemove(OrderEntryHolder o)
        {
            return o.Count == 0;
        }

        private readonly IComparer<OrderEntryHolder> orderComparer = new OrderEntryHolderComparer();
        private readonly List<OrderEntryHolder> sellOrders = new List<OrderEntryHolder>();
        private readonly List<OrderEntryHolder> buyOrders = new List<OrderEntryHolder>();
        private int currentTimestamp = 0;
        private IResultHandler handler;
        public Engine(IResultHandler handler)
        {
            this.handler = handler;
        }

        public void Add(OrderEntry[] entries)
        {
            foreach (var entry in entries)
            {
                Add(entry);
            }
        }

        public void Add(OrderEntry e)
        {
            var entry = new OrderEntryHolder
            {
                CustomerId = e.CustomerId,
                Count = e.Count,
                Price = e.Price,
                Side = e.Side,
                Timestamp = ++currentTimestamp
            };

            List<OrderEntryHolder> orders;
            List<OrderEntryHolder> matchedOrders;
            SideCategory dealCategory;
            SideCategory matchedDealCategory;
            Func<int, int, int> PriceMatcher;
            switch (entry.Side)
            {
                case SideCategory.Sell:
                    orders = sellOrders;
                    matchedOrders = buyOrders;
                    dealCategory = SideCategory.Sell;
                    matchedDealCategory = SideCategory.Buy;
                    PriceMatcher = BuyMatcher;
                    break;
                case SideCategory.Buy:
                    orders = buyOrders;
                    matchedOrders = sellOrders;
                    dealCategory = SideCategory.Buy;
                    matchedDealCategory = SideCategory.Sell;
                    PriceMatcher = SellMatcher;
                    break;
                default:
                    throw new InvalidOperationException();
            }

            var deals = new Queue<ProcessResult>();
            foreach (var item in matchedOrders)
            {
                if (entry.Count == 0) break;
                if (entry.CustomerId == item.CustomerId) continue;

                var matchedPrice = PriceMatcher(item.Price, entry.Price);
                if (matchedPrice == 0) continue;
                var matchedCount = Math.Min(item.Count, entry.Count);

                var matchedDeal = new ProcessResult
                {
                    CustomerId = item.CustomerId,
                    Count = matchedCount,
                    Price = matchedPrice,
                    Side = matchedDealCategory
                };

                var deal = new ProcessResult
                {
                    CustomerId = entry.CustomerId,
                    Count = matchedCount,
                    Price = matchedPrice,
                    Side = dealCategory
                };

                item.Count -= matchedCount;
                entry.Count -= matchedCount;

                deals.Enqueue(matchedDeal);
                deals.Enqueue(deal);
            }

            if (deals.Any())
            {
                matchedOrders.RemoveAll(o => o.Count == 0);
                handler.OnResult(deals.ToArray());
            }

            if (entry.Count == 0) return;
            orders.Add(entry);

            sellOrders.Sort(orderComparer);
        }

        public void Dispose()
        {
            var orders = buyOrders.Concat(sellOrders)
                .Select(o => new ProcessResult(o.CustomerId, o.Count, o.Price, o.Side));

            handler.OnFinish(orders.ToArray());
        }
    }
}
