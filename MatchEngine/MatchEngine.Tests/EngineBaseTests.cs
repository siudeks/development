﻿namespace Matching
{
    using NUnit.Framework;
    using Matching;
    using NSubstitute;
    using System.Linq;
    using System.Diagnostics;

    public abstract class EngineBaseTests
    {
        protected abstract IEngine CreateEngine(IResultHandler resultHandler);

        [Test]
        public void T01ShouldMatchSides()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);

            var expected = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 2, Count = 1, Price = 1, Side = SideCategory.Buy }};

            engine.Add(1, 1, 1, SideCategory.Sell);
            engine.Add(2, 1, 1, SideCategory.Buy);

            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected)));
        }

        [Test]
        public void T02ShouldNotMatchCustomerWithHimself()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);

            engine.Add(1, 1, 1, SideCategory.Sell);
            engine.Add(1, 1, 1, SideCategory.Buy);

            listener.DidNotReceive().OnResult(Arg.Any<ProcessResult[]>());
        }

        [Test]
        public void T03ShouldMatchPartials()
        {
            var listener = Substitute.For<IResultHandler>();

            var expected = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 2, Count = 1, Price = 1, Side = SideCategory.Buy }};

            var engine = CreateEngine(listener);

            engine.Add(1, 2, 1, SideCategory.Sell);
            engine.Add(2, 1, 1, SideCategory.Buy);

            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected)));
        }

        [Test]
        public void T04ShouldMatchSidesInTwoStepsVariant1()
        {
            var listener = Substitute.For<IResultHandler>();

            var expected1 = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 2, Count = 1, Price = 1, Side = SideCategory.Buy }};

            var expected2 = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 3, Count = 1, Price = 1, Side = SideCategory.Buy }};

            var engine = CreateEngine(listener);

            engine.Add(1, 2, 1, SideCategory.Sell);
            engine.Add(2, 1, 1, SideCategory.Buy);
            engine.Add(3, 1, 1, SideCategory.Buy);

            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected1)));
            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected2)));
        }

        [Test]
        public void T05ShouldMatchSidesInTwoStepsVariant2()
        {
            var listener = Substitute.For<IResultHandler>();

            var expected1 = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 2, Count = 1, Price = 1, Side = SideCategory.Buy }};

            var expected2 = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 2, Count = 1, Price = 1, Side = SideCategory.Buy }};

            var engine = CreateEngine(listener);

            engine.Add(1, 1, 1, SideCategory.Sell);
            engine.Add(2, 2, 1, SideCategory.Buy);
            engine.Add(1, 1, 1, SideCategory.Sell);

            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected1)));
            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected2)));
        }

        [Test]
        public void T06ShouldMatchSidesInThirdStep()
        {
            var listener = Substitute.For<IResultHandler>();

            var expected = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Buy },
                new ProcessResult { CustomerId = 3, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 2, Count = 2, Price = 1, Side = SideCategory.Buy },
                new ProcessResult { CustomerId = 3, Count = 2, Price = 1, Side = SideCategory.Sell }};

            var engine = CreateEngine(listener);

            engine.Add(1, 1, 1, SideCategory.Buy);
            engine.Add(2, 2, 1, SideCategory.Buy);
            engine.Add(3, 3, 1, SideCategory.Sell);

            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected)));
        }


        [Test]
        public void T07ShouldMatchSidesWhenSellCountIsBiggerThanBuyCount()
        {
            var listener = Substitute.For<IResultHandler>();

            var expected = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 2, Count = 1, Price = 1, Side = SideCategory.Buy }};

            var engine = CreateEngine(listener);

            engine.Add(1, 2, 1, SideCategory.Sell);
            engine.Add(2, 1, 1, SideCategory.Buy);

            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected)));
        }

        [Test]
        public void T08ShouldMatchSidesWhenBuyCountIsBiggerThanSellCount()
        {
            var listener = Substitute.For<IResultHandler>();

            var expected = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 2, Count = 1, Price = 1, Side = SideCategory.Buy }};

            var engine = CreateEngine(listener);

            engine.Add(1, 1, 1, SideCategory.Sell);
            engine.Add(2, 2, 1, SideCategory.Buy);

            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected)));
        }

        [Test]
        public void T09ShouldNotMatchSidesWhenSellHigherThenBuy()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);

            engine.Add(1, 1, 10, SideCategory.Sell);
            engine.Add(2, 2, 9, SideCategory.Buy);

            listener.DidNotReceive().OnResult(Arg.Any<ProcessResult[]>());
        }

        [Test]
        public void T10ShouldNotMatchSidesWhenBuyLowerThenSell()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);

            engine.Add(1, 1, 9, SideCategory.Buy);
            engine.Add(2, 2, 10, SideCategory.Sell);

            listener.DidNotReceive().OnResult(Arg.Any<ProcessResult[]>());
        }

        [Test]
        public void T11ShouldReportNonMatchedOrdersWhenDisposed()
        {
            var listener = Substitute.For<IResultHandler>();

            var expected = new[] { 
                new ProcessResult { CustomerId = 2, Count = 1, Price = 1, Side = SideCategory.Buy },
                new ProcessResult { CustomerId = 1, Count = 1, Price = 10, Side = SideCategory.Sell},};

            var engine = CreateEngine(listener);
            engine.Add(1, 1, 10, SideCategory.Sell);
            engine.Add(2, 1, 1, SideCategory.Buy);
            engine.Dispose();

            listener.Received().OnFinish(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected)));
        }

        [Test]
        public void T12ShouldNotReportMatchedOrdersWhenDisposed()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);
            engine.Add(1, 1, 1, SideCategory.Sell);
            engine.Add(2, 1, 1, SideCategory.Buy);
            engine.Dispose();

            listener.Received().OnFinish(Arg.Is<ProcessResult[]>(o => o.Count() == 0));
        }

        [Test]
        public void T13ShouldMatchWithTheBestSellOrder()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);

            var expected = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 3, Count = 1, Price = 1, Side = SideCategory.Buy }};

            engine.Add(1, 1, 1, SideCategory.Sell);
            engine.Add(2, 1, 2, SideCategory.Sell);
            engine.Add(3, 1, 3, SideCategory.Buy);

            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected)));
        }

        [Test]
        public void T14ShouldMatchWithTheBestSellOrderWhenTheBestIsNotTheFirst()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);

            var expected = new[] { 
                new ProcessResult { CustomerId = 2, Count = 1, Price = 1, Side = SideCategory.Sell },
                new ProcessResult { CustomerId = 3, Count = 1, Price = 1, Side = SideCategory.Buy }};

            engine.Add(1, 1, 2, SideCategory.Sell);
            engine.Add(2, 1, 1, SideCategory.Sell);
            engine.Add(3, 1, 3, SideCategory.Buy);

            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected)));
        }

        [Test]
        public void T15ShouldMatchWithTheFirstAcceptableBuyOrder()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);

            var expected = new[] { 
                new ProcessResult { CustomerId = 1, Count = 1, Price = 1, Side = SideCategory.Buy },
                new ProcessResult { CustomerId = 3, Count = 1, Price = 1, Side = SideCategory.Sell }};

            engine.Add(1, 1, 1, SideCategory.Buy);
            engine.Add(2, 1, 2, SideCategory.Buy);
            engine.Add(3, 1, 1, SideCategory.Sell);

            listener.Received().OnResult(Arg.Is<ProcessResult[]>(o => o.SequenceEqual(expected)));
        }
    }
}
