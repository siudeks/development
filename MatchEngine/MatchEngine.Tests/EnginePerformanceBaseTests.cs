﻿namespace Matching
{
    using NUnit.Framework;
    using Matching;
    using NSubstitute;
    using System.Linq;
    using System.Diagnostics;

    [TestFixture]
    [Ignore]
    public abstract class EnginePerformanceBaseTests
    {
        protected abstract IEngine CreateEngine(IResultHandler resultHandler);

        [TestFixtureSetUp]
        public void WormUp()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);
            engine.Add(0, 0, 0, SideCategory.Sell);
            engine.Add(0, 0, 0, SideCategory.Buy);
        }

        [Test]
        public void ShouldMatch10000DealsVariant1()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);

            const int count = 10000;

            var timer = Stopwatch.StartNew();
            engine.Add(0, count - 1, 1, SideCategory.Sell);

            for (int i = 1; i < count; i++)
            {
                engine.Add(i, 1, 1, SideCategory.Buy);
            }
            timer.Stop();
            Trace.WriteLine("ShouldMatch1000DealsVariant1: " + timer.ElapsedMilliseconds);

        }

        [Test]
        public void ShouldMatch10000DealsBulkVariant1()
        {
            var listener = Substitute.For<IResultHandler>();

            var engine = CreateEngine(listener);

            const int count = 10000;
            var orders = new OrderEntry[count];
            orders[0] = new OrderEntry { CustomerId = 0, Count = count - 1, Price = 1, Side = SideCategory.Sell };

            for (int i = 1; i < count; i++)
            {
                orders[i] = new OrderEntry { CustomerId = i, Count = 1, Price = 1, Side = SideCategory.Buy };
            }

            var timer = Stopwatch.StartNew();
            engine.Add(orders);
            timer.Stop();
            Trace.WriteLine("ShouldMatch1000DealsBulkVariant1: " + timer.ElapsedMilliseconds);
        }

        [Test]
        public void ShouldMatch10000DealsVariant2()
        {
            var listener = Substitute.For<IResultHandler>();

            using (var engine = CreateEngine(listener))
            {

                const int count = 10000;

                var timer = Stopwatch.StartNew();

                for (int i = 1; i < count; i++)
                {
                    engine.Add(i, count, 1, SideCategory.Buy);
                }
                engine.Add(0, count - 1, 1, SideCategory.Sell);

                timer.Stop();
                Trace.WriteLine("ShouldMatch1000DealsVariant2: " + timer.ElapsedMilliseconds);
            }
        }
    }
}
