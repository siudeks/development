﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thrift;
using Thrift.Server;
using Thrift.Transport;

namespace ChatService
{
    class Program
    {
        static void Main(string[] args)
        {
            TFramedTransport
            MyServiceHandler handler = new MyServiceHandler();
            MyService.Processor processor = new MyService.Processor(handler);
            var serverTransport = new TServerSocket(9090);
            var server = new TSimpleServer(processor, serverTransport);
            server.Serve();
        }
    }
}
