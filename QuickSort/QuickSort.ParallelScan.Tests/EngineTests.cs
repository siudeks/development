﻿using NUnit.Framework;

namespace QuickSort.ParallelScan.Tests
{
    [TestFixture]
    public sealed class EngineTests : EngineBaseTests
    {
        protected override IEngine CreateEngine()
        {
            return new Engine();
        }

        protected override string GetLogPrefix()
        {
            return "ParallelScan";
        }
    }
}
