﻿namespace QuickSort
{
    using NUnit.Framework;

    [TestFixture]
    public sealed class EnginePartitionedTests : EngineBaseTests
    {
        protected override IEngine CreateEngine()
        {
            return new EnginePartitioned();
        }

        protected override string GetLogPrefix()
        {
            return "Parallel";
        }
    }
}
