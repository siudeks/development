﻿namespace QuickSort
{
    using System;
    using System.Collections.Generic;

    // http://www.algolist.net/Algorithms/Sorting/Quicksort
    // http://snipd.net/quicksort-in-c
    public sealed class Engine : IEngine
    {
        public void Sort(int[] unsorted)
        {
            Quicksort(unsorted, 0, unsorted.Length - 1);
        }

        private static void Quicksort(int[] elements, int left, int right)
        {
            int i = left, j = right;
            var pivot = elements[(left + right) / 2];

            while (i <= j)
            {
                while (elements[i] < pivot) i++;
                while (elements[j] > pivot) j--;

                if (i <= j)
                {
                    // Swap
                    var tmp = elements[i];
                    elements[i] = elements[j];
                    elements[j] = tmp;

                    i++;
                    j--;
                }
            }

            // Recursive calls
            if (left < j)
            {
                Quicksort(elements, left, j);
            }

            if (i < right)
            {
                Quicksort(elements, i, right);
            }
        }
    }
}
