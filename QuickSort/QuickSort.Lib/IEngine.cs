﻿namespace QuickSort
{
    public interface IEngine
    {
        void Sort(int[] unsorted);
    }
}
