﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSort
{
    public static class ArrayExtensions
    {
        public static void QuicksortSequential(this int[] elements, int left, int right)
        {
            int i = left, j = right;
            var pivot = elements[(left + right) / 2];

            while (i <= j)
            {
                while (elements[i] < pivot) i++;
                while (elements[j] > pivot) j--;

                if (i <= j)
                {
                    // Swap
                    var tmp = elements[i];
                    elements[i] = elements[j];
                    elements[j] = tmp;

                    i++;
                    j--;
                }
            }

            // Recursive calls
            if (left < j)
            {
                QuicksortSequential(elements, left, j);
            }

            if (i < right)
            {
                QuicksortSequential(elements, i, right);
            }
        }
    }
}
