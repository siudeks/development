﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuickSort
{
    // http://www.uio.no/studier/emner/matnat/ifi/INF3380/v10/undervisningsmateriale/inf3380-week12.pdf
    // algorithm: parallel sorting by regular sampling
    public sealed class EnginePartitioned : IEngine
    {
        public void Sort(int[] unsorted)
        {
            // a) original list is partitioned
            var partitioner = Partitioner.Create(0, unsorted.Length);
            var partitions = partitioner
                .GetDynamicPartitions()
                .ToArray();
            var samplesCount = partitions.Length * partitions.Length;

            var pivots = new int[partitions.Length];
            Parallel.ForEach(partitions, (part, s, index) =>
                {
                    // b) each task sorts with quicksort
                    unsorted.QuicksortSequential(part.Item1, part.Item2 - 1);
                    // c) Each task selects regular samples from its sorted sublist.
                    for (int i = 0; i < partitions.Length; i++)
                    {

                    }
                });
        }
    }
}
