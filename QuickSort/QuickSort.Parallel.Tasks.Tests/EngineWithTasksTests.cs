﻿namespace QuickSort
{
    using NUnit.Framework;

    [TestFixture]
    public sealed class EngineWithTasksTests : EngineBaseTests
    {
        protected override IEngine CreateEngine()
        {
            return new EngineWithTasks();
        }

        protected override string GetLogPrefix()
        {
            return "Parallel";
        }
    }
}
