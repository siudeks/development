﻿namespace QuickSort
{
    using NSubstitute;
    using NUnit.Framework;
    using System;
    using System.Diagnostics;
    using System.Linq;

    [TestFixture]
    public abstract class EngineBaseTests
    {
        const int maxItems = 16777216;
        private static int[] data = new int[maxItems];

        protected abstract IEngine CreateEngine();
        protected abstract string GetLogPrefix();

        [TestCase(0, 1, 2, 3, 4)]
        [TestCase(4, 3, 2, 1, 0)]
        [TestCase(0, 2, 4, 3, 1)]
        [TestCase(0, 3, 1, 4, 2)]
        public void ShouldSortFiveItems(int i0, int i1, int i2, int i3, int i4)
        {
            var unsorted = new[] { i0, i1, i2, i3, i4 };
            var expected = Enumerable.Range(0, 5).ToArray();

            var engine = CreateEngine();

            engine.Sort(unsorted);

            CollectionAssert.AreEqual(unsorted, expected);
        }

        [TestCase(1, false)]
        [TestCase(maxItems, true)]
        public void ShouldSortBigTableSortedAZ(int items, bool measurePerformance)
        {
            for (int i = 0; i < items - 1; i++)
            {
                data[i] = i;
            }

            var engine = CreateEngine();

            var st = Stopwatch.StartNew();

            engine.Sort(data);

            st.Stop();
            if (!measurePerformance) return;

            Trace.WriteLine(string.Format(GetLogPrefix() + " ShouldSortBigTableSortedAZ, Items: {0}, Time[ms]:{1}", items, st.ElapsedMilliseconds));
        }

        [TestCase(1, false)]
        [TestCase(maxItems, true)]
        public void ShouldSortBigTableSortedZA(int items, bool measurePerformance)
        {
            for (int i = items - 1; i >= 0; i--)
            {
                data[i] = i;
            }

            var engine = CreateEngine();

            var st = Stopwatch.StartNew();

            engine.Sort(data);

            st.Stop();
            if (!measurePerformance) return;

            Trace.WriteLine(string.Format(GetLogPrefix() + " ShouldSortBigTableSortedZA, Items: {0}, Time[ms]:{1}", items, st.ElapsedMilliseconds));
        }

        [TestCase(1, false)]
        [TestCase(maxItems, true)]
        public void ShouldSortBigTableSorted09(int items, bool measurePerformance)
        {
            for (int i = 0; i < items - 1; i++)
            {
                data[i] = i % 10;
            }

            var engine = CreateEngine();

            var st = Stopwatch.StartNew();

            engine.Sort(data);

            st.Stop();
            if (!measurePerformance) return;

            Trace.WriteLine(string.Format(GetLogPrefix() + " ShouldSortBigTableSorted09, Items: {0}, Time[ms]:{1}", items, st.ElapsedMilliseconds));
        }

        [TestCase(1, false)]
        [TestCase(maxItems, true)]
        public void ShouldSortBigTableSorted90(int items, bool measurePerformance)
        {
            for (int i = 0; i < items - 1; i++)
            {
                data[i] = 9 - (i % 10);
            }

            var engine = CreateEngine();

            var st = Stopwatch.StartNew();

            engine.Sort(data);

            st.Stop();
            if (!measurePerformance) return;

            Trace.WriteLine(string.Format(GetLogPrefix() + " ShouldSortBigTableSorted90, Items: {0}, Time[ms]:{1}", items, st.ElapsedMilliseconds));
        }
    }
}
