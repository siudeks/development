﻿namespace QuickSort
{
    using NUnit.Framework;

    [TestFixture]
    public sealed class EngineTests : EngineBaseTests
    {
        protected override IEngine CreateEngine()
        {
            return new Engine();
        }

        protected override string GetLogPrefix()
        {
            return "Reference";
        }
    }
}
