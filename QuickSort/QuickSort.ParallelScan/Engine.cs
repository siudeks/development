﻿namespace QuickSort
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    // http://www.drdobbs.com/parallel/quicksort-partition-via-prefix-scan/240003109
    // C http://www.winlab.rutgers.edu/~pkataria/pdc.pdf
    public sealed class Engine : IEngine
    {
        public void Sort(int[] unsorted)
        {
            var count = unsorted.Length;
            var binarySelector = new int[count];
            var pivot = unsorted[unsorted.Length/2];

            var partitioner = Partitioner.Create(unsorted,true);
            var query = partitioner.AsParallel();

            query.Select((o, i) => Tuple.Create(o, i))
                .ForAll(o => binarySelector[o.Item2] = (o.Item1 < pivot) ? 1 : 0);
        }
    }
}
