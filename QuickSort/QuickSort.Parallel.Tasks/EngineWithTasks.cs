﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSort
{
    public sealed class EngineWithTasks : IEngine
    {
        // http://www.darkside.co.za/archive/2008/03/14/microsoft-parallel-extensions-.net-framework.aspx
        public void Sort(int[] unsorted)
        {
            var task = Task.Factory.StartNew(() => QuicksortParallelOptimised(unsorted, 0, unsorted.Length - 1));
            task.Wait();
        }

        private static void QuicksortSequential(int[] elements, int left, int right)
        {
            int i = left, j = right;
            var pivot = elements[(left + right) / 2];

            while (i <= j)
            {
                while (elements[i] < pivot) i++;
                while (elements[j] > pivot) j--;

                if (i <= j)
                {
                    // Swap
                    var tmp = elements[i];
                    elements[i] = elements[j];
                    elements[j] = tmp;

                    i++;
                    j--;
                }
            }

            // Recursive calls
            if (left < j)
            {
                QuicksortSequential(elements, left, j);
            }

            if (i < right)
            {
                QuicksortSequential(elements, i, right);
            }
        }

        //private void QuicksortSequential(int[] arr, int left, int right)
        //{
        //    if (right > left)
        //    {
        //        int pivot = Partition(arr, left, right);
        //        QuicksortSequential(arr, left, pivot - 1);
        //        QuicksortSequential(arr, pivot + 1, right);
        //    }
        //}

        private void QuicksortParallelOptimised(int[] unsorted, int left, int right)
        {
            int i = left, j = right;
            var pivot = unsorted[(left + right) / 2];

            while (i <= j)
            {
                while (unsorted[i] < pivot) i++;
                while (unsorted[j] > pivot) j--;

                if (i <= j)
                {
                    // Swap
                    var tmp = unsorted[i];
                    unsorted[i] = unsorted[j];
                    unsorted[j] = tmp;

                    i++;
                    j--;
                }
            }

            const int SEQUENTIAL_THRESHOLD = 512;
            if (right > left)
            {
                // Recursive calls
                if (right - left < SEQUENTIAL_THRESHOLD)
                {
                    if (left < j)
                    {
                        QuicksortSequential(unsorted, left, j);
                    }

                    if (i < right)
                    {
                        QuicksortSequential(unsorted, i, right);
                    }
                }

                else
                {
                    if (left < j)
                    {
                        Task.Factory.StartNew(() => QuicksortSequential(unsorted, left, j), TaskCreationOptions.AttachedToParent);
                    }

                    if (i < right)
                    {
                        Task.Factory.StartNew(() => QuicksortSequential(unsorted, i, right), TaskCreationOptions.AttachedToParent);
                    }

                }
            }
        }

        private void Swap<T>(T[] arr, int i, int j)
        {
            T tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }

        private int Partition(int[] arr, int low, int high)
        {
            // Simply partitioning implementation

            int pivotPos = (high + low) / 2;
            var pivot = arr[pivotPos];
            Swap(arr, low, pivotPos);

            int left = low;
            for (int i = low + 1; i <= high; i++)
            {
                if (arr[i] < pivot)
                {
                    left++;
                    Swap(arr, i, left);
                }
            }

            Swap(arr, low, left);
            return left;
        }
    }
}
