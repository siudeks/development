﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net.siudek.learning
{
    [TestFixture]
    public sealed class StorageTests
    {
        [Test]
        public void ShouldReadDocument()
        {
            var connectionString = "mongodb://192.168.0.107";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("test");
            var collection = database.GetCollection<Document>("entities");
            var entry = new Document { Name = "Document name" };
            
            collection.Insert(entry);
            Assert.That(entry.Id, Is.Not.Null);

            var query = Query<Document>.EQ(e => e.Id, entry.Id);
            var actual = collection.FindOne(query);
            Assert.That(entry, Is.Not.SameAs(actual));
            Assert.That(entry.Id, Is.EqualTo(actual.Id));
            Assert.That(entry.Name, Is.EqualTo(actual.Name));

        }

        public class Document
        {
            public ObjectId Id { get; set; }
            public string Name { get; set; }
        }
    }
}
