﻿using Microsoft.Xna.Framework.Graphics;
using NUnit.Framework;

namespace Lines
{
    [TestFixture]
    public sealed class LineSetTests
    {
        [Test]
        public void ShouldReturnLines()
        {
            var lineSet = new LineSet(10);
            var line1 = lineSet.Create();
            var line2 = lineSet.Create();

            Assert.That(lineSet.GetLines(), Is.EquivalentTo(new [] {line1, line2}));
        }

        [Test]
        public void ShouldRemoveLineWhenOverlimitMaxPoints()
        {
            var lineSet = new LineSet(1);
            var line1 = lineSet.Create();
            line1.AddVertex(new VertexPositionColor());

            var line2 = lineSet.Create();
            line2.AddVertex(new VertexPositionColor());

            Assert.That(lineSet.GetLines(), Is.EquivalentTo(new[] { line2 }));
        }
    }
}
