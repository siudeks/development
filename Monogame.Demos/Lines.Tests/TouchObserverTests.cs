﻿using Microsoft.Xna.Framework.Input.Touch;
using NSubstitute;
using NUnit.Framework;

namespace Lines
{
    [TestFixture]
    public sealed class TouchObserverTests
    {
        [Test]
        public void ShouldCreateLinesWhenTouched()
        {
            ILineSet lineSet = Substitute.For<ILineSet>();
            lineSet.Create()
                .Returns(o => new Line(0));

            var touches = new TouchCollection(new[] { new TouchLocation() });
            var lines = TouchObserver.Adjust(null, lineSet, touches);

            Assert.That(lines, Is.Not.Empty);
            lineSet.Received().Create();
        }
    }
}
