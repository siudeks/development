﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lines.Tests
{
    [TestFixture]
    public sealed class LineTests
    {
        [Test]
        public void ShouldHoldPoints()
        {
            var line = new Line(10, (o1, o2) => false);
            var vertex1 = new VertexPositionColor();
            var vertex2 = new VertexPositionColor();
            var vertex3 = new VertexPositionColor();
            line.AddVertex(vertex1);
            line.AddVertex(vertex2);
            line.AddVertex(vertex3);
            var points = line.ToPoints();
            Assert.That(points, Has.Length.EqualTo(3));
            Assert.That(points, Has.Member(vertex1));
            Assert.That(points, Has.Member(vertex2));
            Assert.That(points, Has.Member(vertex3));
        }

        [Test]
        public void ShouldReturnLinePoints()
        {
            var line = new Line(10, (o1, o2) => false);

            line.AddVertex(new VertexPositionColor());
            Assert.That(line.ToLine(), Is.Empty);

            line.AddVertex(new VertexPositionColor());
            Assert.That(line.ToLine(), Is.EquivalentTo(new[] { 0, 1 }));

            line.AddVertex(new VertexPositionColor());
            Assert.That(line.ToLine(), Is.EquivalentTo(new[] { 0, 1, 1, 2 }));
        }

        [Test]
        public void ShouldIgnoreDuplicatedPoints()
        {
            var line = new Line(10);

            line.AddVertex(new VertexPositionColor());
            Assert.That(line.ToPoints(), Is.Not.Empty);
            line.AddVertex(new VertexPositionColor());
            Assert.That(line.ToPoints(), Has.Length.EqualTo(1));
        }

        [Test]
        public void ShouldEvictPrevItem()
        {
            var line = new Line(1);

            var first = new VertexPositionColor(new Vector3(1,2,3), Color.White);
            var next = new VertexPositionColor(new Vector3(2,3,4), Color.Black);

            line.AddVertex(first);
            line.AddVertex(next);

            Assert.That(line.ToPoints(), Has.Length.EqualTo(1));
            Assert.That(line.ToPoints()[0], Is.EqualTo(next));
        }

        [Test]
        public void ShouldReturnLinePointsWhenRemoved()
        {
            var line = new Line(10, (o1, o2) => false);

            var vertex1 = new VertexPositionColor();
            var vertex2 = new VertexPositionColor();
            line.AddVertex(vertex1);
            line.AddVertex(vertex2);
            int removedCount;
            
            line.RemoveFirst(out removedCount);
            Assert.That(line.ToPoints(), Is.Not.Empty);
            Assert.That(line.ToLine(), Is.Empty);

            line.RemoveFirst(out removedCount);

            Assert.That(line.ToPoints(), Is.Empty);
            Assert.That(line.ToLine(), Is.Empty);
        }
    }
}
