using Microsoft.Phone.Reactive;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Linq;
using Lines;
using System;

// http://stackoverflow.com/questions/10962666/xna-mouse-coordinates-to-word-space-transformation
namespace PhoneApp1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class BoardGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        CompositeDisposable disposer = new CompositeDisposable();

        Matrix worldMatrix;
        Matrix viewMatrix;
        Matrix projectionMatrix;

        static readonly int maxPoints = 1000;
        BasicEffect basicEffect;
        VertexDeclaration vertexDeclaration;
        LineSet lineSet = new LineSet(maxPoints);

        VertexBuffer vertexBuffer;

        public bool CleanOrdered;

        public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
        (
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(12, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0)

        );

        public BoardGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        //VertexPositionColor[] primitiveList;
        //RasterizerState rasterizerState;
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Initializes the triangle list.
        /// </summary>
        private void InitializeTriangleList()
        {
        }

        /// <summary>
        /// Initializes the transforms used by the game.
        /// </summary>
        private void InitializeTransform()
        {

            viewMatrix = Matrix.CreateLookAt(
                new Vector3(0.0f, 0.0f, 1.0f),
                Vector3.Zero,
                Vector3.Up
                );

            projectionMatrix = Matrix.CreateOrthographicOffCenter(
                0,
                (float)GraphicsDevice.Viewport.Width,
                (float)GraphicsDevice.Viewport.Height,
                0,
                1.0f, 1000.0f);
        }


        /// <summary>
        /// Initializes the effect (loading, parameter setting, and technique selection)
        /// used by the game.
        /// </summary>
        private void InitializeEffect()
        {

            vertexDeclaration = new VertexDeclaration(new VertexElement[]
                {
                    new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
                    new VertexElement(12, VertexElementFormat.Color, VertexElementUsage.Color, 0)
                }
            );

            basicEffect = new BasicEffect(GraphicsDevice);
            basicEffect.VertexColorEnabled = true;

            worldMatrix = Matrix.CreateTranslation(GraphicsDevice.Viewport.Width / 2f - 150,
                GraphicsDevice.Viewport.Height / 2f - 50, 0);
            basicEffect.World = worldMatrix;
            basicEffect.View = viewMatrix;
            basicEffect.Projection = projectionMatrix;
        }

        /// <summary>
        /// Initializes the point list.
        /// </summary>
        private void InitializePoints()
        {
            // Initialize the vertex buffer, allocating memory for each vertex.
            vertexBuffer = new VertexBuffer(graphics.GraphicsDevice, vertexDeclaration, maxPoints, BufferUsage.None);
        }

        protected override void LoadContent()
        {
            InitializeTransform();
            InitializeEffect();
            InitializePoints();

            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
        }

        // Contains currently painted lines.
        IShape[] lines;
        protected override void Update(GameTime gameTime)
        {
            if (CleanOrdered)
            {
                CleanOrdered = false;
                lineSet = new LineSet(maxPoints);
            }

            var touches = TouchPanel.GetState();

            if (touches.Count > 0)
            {
                lines = TouchObserver.Adjust(lines, lineSet, touches);

                bool removeReleasedLines = false;
                for (int touchIndex = 0; touchIndex < touches.Count; touchIndex++)
                {
                    var touch = touches[touchIndex];
                    if (touch.State == TouchLocationState.Released)
                    {
                        lines[touchIndex] = null;
                        removeReleasedLines = true;
                        continue;
                    }
                    var x = (int)touch.Position.X;
                    var y = (int)touch.Position.Y;

                    Debug.WriteLine("X Y : " + x + " " + y);

                    var point = GraphicsDevice.Viewport.Unproject(new Vector3(x, y, 0), projectionMatrix, viewMatrix, worldMatrix);
                    lines[touchIndex].AddVertex(new VertexPositionColor(point, Color.White));
                }

                if (removeReleasedLines)
                {
                    lines = lines.Where(o => o != null).ToArray();
                }
            }
            else
            {
                // no trackng lines because  user is not pressing.
                lines = null;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            RasterizerState rasterizerState1 = new RasterizerState();
            rasterizerState1.CullMode = CullMode.None;
            graphics.GraphicsDevice.RasterizerState = rasterizerState1;
            foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                foreach (var currentLine in lineSet.GetLines())
                {
                    var pointList = currentLine.ToPoints();
                    var lineListIndices = currentLine.ToLine();
                    vertexBuffer.SetData<VertexPositionColor>(pointList);
                    
                    var pointsCount = pointList.Length;
                    var linesCount = lineListIndices.Length / 2;
                    
                    if (linesCount > 0)
                    {
                        GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(
                            PrimitiveType.LineList,
                            pointList,
                            0,  // vertex buffer offset to add to each element of the index buffer
                            pointsCount,  // number of vertices in pointList
                            lineListIndices,  // the index buffer
                            0,  // first index element to read
                            linesCount);   // number of primitives to draw
                    }
                }

            }

            base.Draw(gameTime);

            spriteBatch.End();
        }
    }
}
