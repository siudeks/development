﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example
{
    public sealed class Map
    {
        const int dx = 4;
        const int dy = 4;
        public readonly int PointsCount = dx * dy;
        const int midx = dx / 2;
        const int x_min = 0 - midx;
        const int x_max = dx - midx;
        const int midy = dy / 2;
        const int y_min = 0 - midy;
        const int y_max = dy - midy;

        public readonly VertexPositionColor[] Points = new VertexPositionColor[dx * dy];
        public readonly short[] Lines;

        public Map()
        {
            Lines = new short[PointsCount * 4];

            for (int x = x_min; x < x_max; x++)
                for (int y = y_min; y < y_max; y++)
                {
                    var offset = ToOffset(x, y);
                    Points[offset].Position = new Vector3(x, y, 0);
                    Points[offset].Color = Color.White;
                }

            var loffset = 0;
            for (int x = x_min; x < (x_max - 1); x++)
                for (int y = y_min; y < (y_max - 1); y++)
                {
                    Lines[loffset++] = ToOffset(x, y);
                    Lines[loffset++] = ToOffset(x + 1, y);
                    Lines[loffset++] = ToOffset(x, y);
                    Lines[loffset++] = ToOffset(x, y+1);
                }
        }

        private static short ToOffset(int x, int y)
        {
            return (short)((y - y_min) * dx + (x - x_min));
        }
    }
}
