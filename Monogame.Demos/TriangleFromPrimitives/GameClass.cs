﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example
{
    // http://www.toymaker.info/Games/XNA/html/xna_simple_triangle.html
    class GameClass : Game
    {
        GraphicsDeviceManager graphics; 
        VertexPositionColor[] verts;
        BasicEffect effect;

        public GameClass()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void LoadContent()
        {
            verts = new VertexPositionColor[3];

            verts[0].Position = new Vector3(0, 1, 0);
            verts[0].Color = Color.Blue;
            verts[1].Position = new Vector3(1, -1, 0);
            verts[1].Color = Color.Blue;
            verts[2].Position = new Vector3(-1, -1, 0);
            verts[2].Color = Color.Blue;

            effect = new BasicEffect(GraphicsDevice);
            effect.World = Matrix.Identity;
            effect.View = Matrix.CreateLookAt(new Vector3(0, 0, 5), Vector3.Zero, Vector3.Up);

            float aspect = (float)Window.ClientBounds.Width / (float)Window.ClientBounds.Height;
            effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, aspect, 1, 100);

            effect.VertexColorEnabled = true;

            base.LoadContent();
        }

        protected override void Draw(GameTime gameTime)
        {
            //RasterizerState rasterizerState1 = new RasterizerState();
            //rasterizerState1.CullMode = CullMode.None;
            //graphics.GraphicsDevice.RasterizerState = rasterizerState1;

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.TriangleList, verts, 0, 1);
            }
        }
    }
}
