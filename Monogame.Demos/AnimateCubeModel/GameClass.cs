﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example
{
    // http://www.toymaker.info/Games/XNA/html/xna_simple_triangle.html
    class GameClass : Game
    {
        GraphicsDeviceManager graphics;
        BasicEffect effect;

        public GameClass()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void LoadContent()
        {
            var model = Content.Load<Model>("Cube");
            effect = new BasicEffect(GraphicsDevice);
            effect.World = Matrix.Identity;
            effect.View = Matrix.CreateLookAt(new Vector3(0, 0, 5), Vector3.Zero, Vector3.Up);

            float aspect = (float)Window.ClientBounds.Width / (float)Window.ClientBounds.Height;
            effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, aspect, 1, 100);

            effect.VertexColorEnabled = true;

            base.LoadContent();
        }

        protected override void Draw(GameTime gameTime)
        {
            RasterizerState rasterizerState1 = new RasterizerState();
            rasterizerState1.CullMode = CullMode.None;
            graphics.GraphicsDevice.RasterizerState = rasterizerState1;

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(
                    PrimitiveType.LineList, 
                    map.Points, 
                    0,
                    map.Points.Length, map.Lines, 0, map.Points.Length*2);
            }
        }
    }
}
