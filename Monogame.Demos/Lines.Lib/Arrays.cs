﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lines
{
    public static class Arrays
    {
        public static void RemoveFirst<T>(ref T[] array)
        {
            for (int i = 1; i < array.Length; i++)
            {
                array[i - 1] = array[i];
            }
            Array.Resize(ref array, array.Length - 1);
        }

        public static void AddLast<T>(ref T[] array, T item)
        {
            var length = array.Length;
            Array.Resize(ref array, length + 1);
            array[length] = item;
        }
    }
}
