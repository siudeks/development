﻿using Microsoft.Xna.Framework.Input.Touch;
using System;
namespace Lines
{
    public sealed class TouchObserver
    {
        private static readonly IShape[] Empty = new Line[0];

        public static IShape[] Adjust(IShape[] current, ILineSet lineSet, TouchCollection touches)
        {
            current = current ?? Empty;

            var delta = touches.Count - current.Length;
            if (delta > 0)
            {
                for (int i = 0; i < delta; i++)
                {
                    var line = lineSet.Create();
                    Arrays.AddLast(ref current, line);
                }
            }

            if (delta == 0) return current;

            if (delta < 0)
            {
               Array.Resize(ref current, touches.Count);
            }

            return current;
        }
    }
}
