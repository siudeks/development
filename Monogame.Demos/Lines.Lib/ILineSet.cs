﻿using System;
namespace Lines
{
    public interface ILineSet
    {
        IShape Create();
        IShape[] GetLines();
    }
}
