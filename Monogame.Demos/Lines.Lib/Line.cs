﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Lines
{
    // TODO optimize: change List to array.
    public sealed class Line : IShape
    {
        public static readonly Func<VertexPositionColor, VertexPositionColor, bool> DefaultEqualityComparer = (o1, o2) => o1.Position == o2.Position;
        private static Action EmptyAddListener = () => { };
        private VertexPositionColor[] points = new VertexPositionColor[0];
        private Func<VertexPositionColor, VertexPositionColor, bool> equalityComparer;
        private readonly int bufferSize;
        private readonly Action addListener;

        public Line(int bufferSize)
            : this(bufferSize, DefaultEqualityComparer)
        {
        }

        public Line(
            int bufferSize,
            Func<VertexPositionColor, VertexPositionColor, bool> equalityComparer)
            : this(bufferSize, equalityComparer, EmptyAddListener)
        {
        }

        public Line(
            int bufferSize,
            Func<VertexPositionColor, VertexPositionColor, bool> equalityComparer,
            Action addListener)
        {
            this.equalityComparer = equalityComparer;
            this.bufferSize = bufferSize;
            this.addListener = addListener;
        }

        public void AddVertex(VertexPositionColor vertex)
        {
            if (points.Length > 0 && equalityComparer(points[points.Length - 1], vertex)) return;
            if (points.Length == bufferSize) Arrays.RemoveFirst(ref points);

            Arrays.AddLast(ref points, vertex);

            // empty instances meaning is - it needs to recalculate results;
            toPoints = emptyToPoints;
            toLine = emptyToLine;

            addListener();
        }

        private static readonly VertexPositionColor[] emptyToPoints = new VertexPositionColor[0];
        private VertexPositionColor[] toPoints = emptyToPoints;
        public VertexPositionColor[] ToPoints()
        {
            if (toPoints == emptyToPoints)
            {
                toPoints = points;
            }
            return toPoints;
        }

        private static readonly short[] emptyToLine = new short[0];
        private short[] toLine = emptyToLine;
        public short[] ToLine()
        {
            if (toLine != emptyToLine) return toLine;
            var pointsLength = points.Length;

            // can't create lines from less than 2 points.
            if (pointsLength < 2) return toLine;

            var result = new short[(pointsLength - 1) * 2];
            if (pointsLength == 0) return result;

            for (short i = 0; i < pointsLength - 1; i++)
            {
                result[i * 2] = i;
                result[i * 2 + 1] = (short)(i + 1);
            }
            toLine = result;

            return toLine;
        }

        /// <summary>
        /// Removes the older point point from the list
        /// </summary>
        /// <param name="removed">Count of removed points</param>
        /// <returns>True when contains more points, otherwise false.</returns>
        public bool RemoveFirst(out int removed)
        {
            if (points.Length > 0)
            {
                Arrays.RemoveFirst(ref points);
                toLine = emptyToLine;
                toPoints = emptyToPoints;
                removed = 1;
            }
            else
            {
                removed = 0;
            }
            return points.Length > 0;
        }
    }
}