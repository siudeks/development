﻿using Microsoft.Xna.Framework.Graphics;

namespace Lines
{
    public interface IShape
    {
        void AddVertex(VertexPositionColor vertexPositionColor);

        VertexPositionColor[] ToPoints();

        short[] ToLine();
    }
}
