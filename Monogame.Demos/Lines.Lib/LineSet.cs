﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lines
{
    public sealed class LineSet : ILineSet
    {
        private Line[] lines = new Line[0];

        private readonly int bufferSize;
        private int addedTotal;
        public LineSet(int bufferSize)
        {
            this.bufferSize = bufferSize;
        }

        public IShape Create()
        {
            var line = new Line(bufferSize, Line.DefaultEqualityComparer, onAddVertex);
            Array.Resize(ref lines, lines.Length + 1);
            lines[lines.Length - 1] = line;
            return line;
        }

        private void onAddVertex()
        {
            addedTotal++;
            if (lines.Length == 1) return;

            while (addedTotal > bufferSize)
            {
                int removedCount;
                var hasItems = lines[0].RemoveFirst(out removedCount);
                addedTotal -= removedCount;
                // first line doesn't contain points - could be safely removed.
                if (!hasItems)
                {
                    for (int i = 1; i < lines.Length; i++)
                    {
                        lines[i - 1] = lines[i];
                    }
                    Array.Resize(ref lines, lines.Length - 1);
                }
            }

        }

        public IShape[] GetLines()
        {
            return lines;
        }
    }
}
