/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.siudek.reactive;

import java.util.concurrent.CountDownLatch;
import org.junit.Test;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 *
 * @author ssiudek
 */
public class OverlappingTests {

    public OverlappingTests() {
    }

    @Test(timeout = 1000)
    public void shouldNotOverlapStepsOnObservableWithThreadPool() throws InterruptedException {
        CountDownLatch counter = new CountDownLatch(2);

        Observable<Integer> source = Observable.range(1, 2, Schedulers.newThread());

        source.subscribe(o -> {
            counter.countDown();
        });
        counter.await();
    }
}
