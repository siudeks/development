package aaa
/**
 * Created by slawomir.siudek on 2015-03-16.
 */
public class ArrayExtensions {
    public static <T> Collection cast(final Collection self, Class<T> clazz) {
        self.findResult {clazz.isInstance(it) ? it : null}.asType(clazz)
    }
}
