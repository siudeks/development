import spock.lang.Specification

import aaa.*

class CollectionTests extends Specification {
    def "Should filter and cast items in collection" () {
        given: "Collection of different types"
        Object[] arr = [ new Object(), "ala", new Long(1), []]

        when:
        def longs = arr.cast(Long.class)

        then:
        longs*.intValue() == [1]
    }
}

public class FuncProgUtilExtension {
    public static Collection filter(Collection self, Closure clozure) {
        return self.grep(clozure)
    }
    public static Collection map(Collection self, Closure clozure) {
        return self.collect(clozure)
    }
    public static Object reduce(Collection self, Closure clozure) {
        return self.inject(clozure)
    }
    public static Object reduce(Collection self, String operator) {
        switch(operator){
            case '+' :
                self.inject({acc, val -> acc + val})
                break
            case '-' :
                self.inject({acc, val -> acc - val})
                break
            case'*' :
                self.inject({acc, val -> acc * val})
                break
            case '/':
                self.inject({acc, val -> acc / val})
                break
            case'**':
                self.inject({acc, val -> Math.pow(acc, val)})
                break
            default:
                throw new IllegalArgumentException()
                break
        }
    }
}

class PirateStaticExtension {
    static String talkLikeAPirate(final String type) {
        "Arr, me hearty,"
    }
}

