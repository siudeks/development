/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.siudek.cache;

import com.google.common.collect.Iterators;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.SearchAttribute;
import net.sf.ehcache.config.Searchable;
import net.sf.ehcache.search.Attribute;
import net.sf.ehcache.search.Result;
import net.sf.ehcache.search.Results;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;
import static org.hamcrest.CoreMatchers.*;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ssiudek
 */
public class QueryTests {
    
    public QueryTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void shouldFindItemInCacheByProperty(){
        CacheManager cacheManager = CacheManager.create();
        CacheConfiguration cacheConfig = new CacheConfiguration("myCache", 0);
        
        Searchable searchable = new Searchable();
        cacheConfig.addSearchable(searchable);
        searchable.addSearchAttribute(new SearchAttribute().name("age"));
        
        cacheManager.addCache(new Cache(cacheConfig));
        Cache cache = cacheManager.getCache("myCache");

        Attribute<Integer> age = cache.getSearchAttribute("age");
        Element item = new Element(1, new PersonModel("Anna", 20));
        cache.put(item);
        
        Results results = cache
                .createQuery()
                .includeValues()
                .addCriteria(age.eq(20)).execute();

        Result[] cachedItems = Iterators
                .toArray(results.all().iterator(), Result.class);
        assertThat(cachedItems.length, is(1));
        assertEquals(((PersonModel)cachedItems[0].getValue()).getAge(), 20);
    }
}
