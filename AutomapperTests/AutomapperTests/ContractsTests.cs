﻿using AutoMapper;
using NUnit.Framework;

namespace AutomapperTests
{
    [TestFixture]
    public sealed class ContractsTests
    {

        [Test]
        public void ShouldDiscoverMissingMappingsInIncludedType()
        {
            Mapper.CreateMap<object, BaseType>()
                .Include<object, ChildType>();
            Assert.Throws<AutoMapperConfigurationException>(Mapper.AssertConfigurationIsValid);
        }

        [Test]
        public void ShouldCopyProperties()
        {
            var obj1 = new ChildType();
            var obj2 = new ChildType();

            obj1.Property1 = "1";

            Mapper.CreateMap<ChildType, ChildType>();

            Mapper.Map<ChildType, ChildType>(obj1, obj2);

            Assert.That(obj2.Property1, Is.EqualTo("1"));
        }

        public class BaseType { }

        public class ChildType : BaseType
        {
            public string Property1 { get; set; }
        }

        [Test]
        public void ShouldCopyPropertiesAndNotOverrideNonDefinedMapping()
        {
            var itemA = new ClassA { Property1 = "1" };
            var itemB = new ClassB { Property1 = "11", Property2 = "22" };
            Mapper.CreateMap<ClassA, ClassB>();

            Mapper.Map<ClassA, ClassB>(itemA, itemB);
            Assert.That(itemB.Property1, Is.EqualTo("1"));
            Assert.That(itemB.Property2, Is.EqualTo("22"));
        }

        public class ClassA
        {
            public string Property1 { get; set; }
        }

        public class ClassB
        {
            public string Property1 { get; set; }
            public string Property2 { get; set; }
        }
    }
}
