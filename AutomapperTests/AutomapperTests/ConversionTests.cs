﻿using AutoMapper;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomapperTests
{
    [TestFixture]
    public sealed class ConversionTests
    {
        [Test]
        public void ShouldConvertFromIntToEnum()
        {
            Assert.That(Mapper.Map<Numbers, string>(Numbers.First), Is.EqualTo("First"));
            Assert.That(Mapper.Map<Numbers, int>(Numbers.First), Is.EqualTo(1));
        }

        [Test]
        public void ShouldConvertFromStringToEnum()
        {
            Assert.That(Mapper.Map<Numbers, string>(Numbers.First), Is.EqualTo("First"));
        }
    }

    public enum Numbers
    {
        First = 1,

        Second = 2
    }
}
