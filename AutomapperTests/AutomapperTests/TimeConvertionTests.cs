﻿using AutoMapper;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomapperTests
{
    [TestFixture]
    public sealed class TimeConvertionTests
    {
        [Test]
        public void ShouldIncreaseTime()
        {
            Mapper.CreateMap<DateTime, DateTime>().ConvertUsing((DateTime o) => new DateTime(o.Year, o.Month, o.Day, o.Hour+1, o.Minute, o.Second));
            Mapper.CreateMap<TypeWithTime, TypeWithTime>();

            var source = new TypeWithTime { Property1 = "aaa", Property2 = new DateTime(2001, 2, 3, 4, 5, 6) };
            var target = Mapper.Map<TypeWithTime, TypeWithTime>(source);

            Assert.That(target.Property1, Is.EqualTo("aaa"));
            Assert.That(target.Property2, Is.EqualTo(new DateTime(2001,2,3,5,5,6)));
        }
    }
}
