﻿using AutoMapper;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomapperTests
{
    [TestFixture]
    public sealed class NullabilityTests
    {
        [Test]
        public void ShouldConvertToNullableStructure()
        {
            double source = 1;
            var target = Mapper.Map<double, double?>(source);

            Assert.That(target, Is.EqualTo(source));
        }

        [Test]
        public void ShouldConvertFromNullableStructure()
        {
            double? source = null;
            var target = Mapper.Map<double?, double>(source);

            Assert.That(target, Is.EqualTo(default(double)));
        }

        [Test]
        public void ShouldMapWhenConditionMet()
        {
            Mapper.CreateMap<Double, Double>()
                .ForMember(t => t.Property1, o => o.MapFrom(s => s.Property1 > 5 ? s.Property1 : default(double)));

            Assert.That(Mapper.Map<Double, Double>(new Double(1)).Property1, Is.EqualTo(default(decimal)));
            Assert.That(Mapper.Map<Double, Double>(new Double(10)).Property1, Is.EqualTo(10));
        }

        public class Double
        {
            public double Property1 { get; set; }
            public Double(double property1)
            {
                Property1 = property1;
            }
        }

    }
}
