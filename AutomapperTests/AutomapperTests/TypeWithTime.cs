﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomapperTests
{
    public class TypeWithTime
    {
        public string Property1 { get; set; }
        public DateTime? Property2 { get; set; }
    }
}
