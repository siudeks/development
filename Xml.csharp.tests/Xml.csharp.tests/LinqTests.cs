﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace XmlTests
{
    [TestFixture]
    public sealed class LinqTests
    {
        const string data = 
@"<Level1>
  <Level2>
    <Key>Item1Key</Key>
    <Value>Item1Value</Value>
  </Level2>
  <Level2>
    <Key>Item2Key</Key>
    <Value>Item2Value</Value>
  </Level2>
</Level1>";
        [Test]
        public void ShouldFindChildren()
        {
            var doc = XDocument.Load(new StringReader(data));
            var items = doc.Descendants("Level2")
                .Select(o => new
                {
                    Key = o.Element("Key").Value,
                    Value = o.Element("Value").Value
                })
                .Select(o => Tuple.Create(o.Key, o.Value));

            Assert.That(items, Is.EquivalentTo(new[] 
            { 
                Tuple.Create("Item1Key", "Item1Value"), 
                Tuple.Create("Item2Key", "Item2Value")
            }));
        }
    }
}
