﻿namespace WpfApplication
{
    using Caliburn.Micro;
    using System;
    using System.Collections.Generic;
    using System.Composition.Hosting;
    using System.Windows;

    public sealed class Bootstrapper : BootstrapperBase
    {
        private CompositionHost compositionHost;

        public Bootstrapper()
        {
            compositionHost = CompositionHost.CreateCompositionHost()
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            base.DisplayRootViewFor<ShellViewModel>();
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return compositionHost.GetExport()
        }
    }
}
