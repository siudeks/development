﻿namespace WpfApplication
{
    using System;
    using System.Collections.Generic;
    using System.Composition;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public sealed class ShellViewModel
    {
        public IEnumerable<IViewModel> ViewModelDemos { private get; set; }

        [OnImportsSatisfied]
        public void Initialize()
        {
        }
    }
}
