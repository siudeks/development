﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication
{
    [Export(typeof(IViewModel))]
    public class AsyncBindingDemoViewModel : IViewModel
    {
        public string Caption { get; set; }

        public AsyncBindingDemoViewModel()
        {
            Caption = "AsyncBinding";
        }
    }
}
