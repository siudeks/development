﻿namespace WindsorCastleTests
{

    public sealed class Subscriber
    {
        public PublishedEventArgs LastArgs { get; private set; }

        public void OnEvent(object sender, PublishedEventArgs args)
        {
            LastArgs = args;
        }
    }
}
