﻿namespace WindsorCastleTests
{
    public interface IGenericSend<TRequest>
    {
        void Send(TRequest request);
    }

    public interface IGenericSend<TRequest, TResponse>
    {
        TResponse Send(TRequest request);
    }
}
