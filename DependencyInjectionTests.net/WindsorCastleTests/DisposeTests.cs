﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindsorCastleTests
{
    [TestFixture]
    public sealed class DisposeTests
    {
        [Test]
        public void ShouldDisposeTransientComponent()
        {
            var container = new WindsorContainer();
            container.Register(Component.For<MyDisposableComponent>().LifeStyle.Transient);

            var instance = container.Resolve<MyDisposableComponent>();
            container.Release(instance);

            Assert.That(instance.Disposed, Is.True);
        }

        [Test]
        public void ShouldCollectTrasientComponent()
        {

            var released = false;
            Action onReleased = () => released = true;

            var container = new WindsorContainer();
            container.Register(
                Component.For<Action>().Instance(onReleased),
                Component.For<TransientComponent>().LifeStyle.Transient);

            var instance = container.Resolve<TransientComponent>();
            instance = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            Assert.That(released, Is.True);
        }

        [Test]
        public void ShouldDisposeTransientComponentWhenCollected()
        {
            bool disposed = false;
            Action onDisposed = () => { disposed = true; };

            var container = new WindsorContainer();
            container.Register(
                Component.For<Action>().Instance(onDisposed),
                Component.For<MyDelegateDisposableComponent>().LifeStyle.Transient);

            var instance = container.Resolve<MyDelegateDisposableComponent>();
            instance = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            Assert.That(disposed, Is.True);
        }
    }

    class MyDisposableComponent : IDisposable
    {
        public bool Disposed { get; private set; }

        public void Dispose()
        {
            Disposed = true;
        }
    }

    class TransientComponent 
    {
        private readonly Action onDisposed;
        public TransientComponent(Action onDisposed)
        {
            this.onDisposed = onDisposed;
        }

        ~TransientComponent()
        {
            onDisposed();
        }
    }

    class MyDelegateDisposableComponent : IDisposable
    {
        private readonly Action onDisposed;
        public MyDelegateDisposableComponent(Action onDisposed)
        {
            this.onDisposed = onDisposed;
        }

        public void Dispose()
        {
            onDisposed();
        }
    }
}
