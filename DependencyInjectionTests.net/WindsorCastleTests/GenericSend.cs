﻿namespace WindsorCastleTests
{
    public class GenericSend<TRequest> : IGenericSend<TRequest>
    {
        public void Send(TRequest request)
        {
        }
    }

    public class GenericSend<TRequest, TResponse> : IGenericSend<TRequest, TResponse>
    {
        public TResponse Send(TRequest request)
        {
            return default(TResponse);
        }
    }
}
