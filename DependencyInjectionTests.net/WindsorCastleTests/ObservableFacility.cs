﻿using Castle.Core;
using Castle.Core.Configuration;
using Castle.MicroKernel;
using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.ModelBuilder;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WindsorCastleTests
{
    public sealed class ObservableFacility : AbstractFacility, IContributeComponentModelConstruction, ICommissionConcern
    {
        // to create subscription on generic objects where we know type at runtime execution we need to 
        // use generic method
        private static readonly MethodInfo createSubscription = typeof(ObservableFacility).GetMethod("CreateSubscription", BindingFlags.Static | BindingFlags.NonPublic);
        private ConcurrentDictionary<Type, object> subjects = new ConcurrentDictionary<Type, object>();

        protected override void Init()
        {
            Kernel.ComponentModelBuilder.AddContributor(this);
            Kernel.ComponentCreated += kernel_ComponentCreated;
        }

        // TODO measure performance and add type reflection cache if needed.
        // NOTE: We assume all IObservable<> properties and IObserver<> properties are created, not null and will not be changed.
        void kernel_ComponentCreated(ComponentModel model, object instance)
        {
            // visit senders
            VisitProperties(instance,
                o => o.IsDefined(typeof(NotificationSenderAttribute)) && IsAssignableToGenericType(o.PropertyType, typeof(IObservable<>)),
                (t, o, s) => createSubscription.MakeGenericMethod(t).Invoke(null, new[] { o.GetGetMethod().Invoke(instance, (object[])null), s }));

        }

        internal void VisitProperties(object instance, Func<PropertyInfo, bool> propertySelector, Action<Type, PropertyInfo, object> propertyWithSubjectVisitor)
        {
            var instanceType = instance.GetType();
            var propertiesWithSubjects = instanceType.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(propertySelector)
                .Select(o => new
                {
                    PropertyInfo = o,
                    NotificationType = o.PropertyType.GetGenericArguments()[0]
                })
                .Select(o => new
                {
                    o.NotificationType,
                    o.PropertyInfo,
                    Subject = subjects.GetOrAdd(o.NotificationType, t => Activator.CreateInstance(typeof(Subject<>).MakeGenericType(t)))
                });

            foreach (var item in propertiesWithSubjects)
            {
                propertyWithSubjectVisitor(item.NotificationType, item.PropertyInfo, item.Subject);
            }
        }

        internal static bool IsAssignableToGenericType(Type givenType, Type genericType)
        {
            if (givenType.IsGenericType && givenType.GetGenericTypeDefinition() == genericType)
                return true;

            var interfaceTypes = givenType.GetInterfaces();
            foreach (var it in interfaceTypes)
            {
                if (it.IsGenericType && it.GetGenericTypeDefinition() == genericType)
                    return true;
            }

            Type baseType = givenType.BaseType;
            if (baseType == null) return false;

            return IsAssignableToGenericType(baseType, genericType);
        }

        private static void CreateSubscription<T>(IObservable<T> source, IObserver<T> listener)
        {
            source.Subscribe(listener);
        }

        public void Apply(ComponentModel model, object component)
        {
            // visit listenersKe
            VisitProperties(component,
                o => o.IsDefined(typeof(NotificationListenerAttribute)) && IsAssignableToGenericType(o.PropertyType, typeof(IObservable<>)),
                (t, o, s) => o.GetSetMethod().Invoke(component, new [] { s }));
        }

        public void ProcessModel(IKernel kernel, ComponentModel model)
        {
            model.Lifecycle.Add(this);
        }
    }
}
