﻿namespace WindsorCastleTests
{
    using System;
    using System.Reactive.Subjects;

    public sealed class Observable<T>
    {
        public IObservable<T> Sender { get; set; }

        public Observable(ISubject<T> sender)
        {
            Sender = sender;
        }
    }

    public sealed class ObservableWithAttribute<T>
    {
        [NotificationSender]
        public IObservable<T> Sender { get; set; }

        public ObservableWithAttribute(IObservable<T> sender)
        {
            Sender = sender;
        }
    }

}
