﻿namespace WindsorCastleTests
{
    using Castle.Core;
    using System;
    using System.Reactive.Subjects;

    public sealed class Observer<T>
    {
        public IObserver<T> Handler { get; private set; }
        public Observer(IObserver<T> handler)
        {
            this.Handler = handler;
        }
    }

    public sealed class ObserverWithAttribute<T> : IInitializable
    {
        [NotificationListener]
        public IObservable<T> Handler { get; set; }

        public void Initialize()
        {
            throw new NotImplementedException();
        }
    }
}
