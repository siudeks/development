﻿using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;
using Castle.MicroKernel.ModelBuilder;
using Castle.MicroKernel.ModelBuilder.Inspectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindsorCastleTests
{
    public sealed class ObservableInspector : IContributeComponentModelConstruction
    {
        private readonly ObservableFacility facility;
        public ObservableInspector(ObservableFacility facility)
        {
            this.facility = facility;
        }


        public void ProcessModel(IKernel kernel, ComponentModel model)
        {
            model.Lifecycle.Add(facility);
        }
    }
}
