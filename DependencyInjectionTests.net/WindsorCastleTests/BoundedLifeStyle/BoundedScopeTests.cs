﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Facilities.TypedFactory;
using Castle.Core;
using Castle.Facilities.Startable;

namespace WindsorCastleTests.BoundedLifeStyle
{
    [TestFixture]
    public sealed class BoundedScopeTests
    {
        [Test]
        public void ShouldBoundClasses()
        {
            var container = new WindsorContainer();
            container.Register(
                Component.For<Parent1Class>().LifeStyle.Transient,
                Component.For<Child1Class>().LifeStyle.Transient,
                Component.For<Service1Class>().LifeStyle.BoundTo<Parent1Class>());

            var parent = container.Resolve<Parent1Class>();
            Assert.AreSame(parent.Service, parent.Child.Service);
        }

        public class Service1Class
        {
        }
        public class Parent1Class
        {
            public Service1Class Service { get; set; }
            public Child1Class Child { get; set; }
        }
        public class Child1Class
        {
            public Service1Class Service { get; set; }
        }

        [Test]
        public void ShouldBoundClassesCreatedWithFactory()
        {
            var container = new WindsorContainer();
            container.AddFacility<TypedFactoryFacility>();
            container.Register(
                Component.For<Parent2Class>().LifeStyle.Transient,
                Component.For<Child2Class>().LifeStyle.Transient,
                Component.For(typeof(IFactory<>)).AsFactory().LifeStyle.BoundTo<Parent2Class>(),
                Component.For<Service2Class>().LifeStyle.BoundTo<Parent2Class>());

            var parent = container.Resolve<Parent2Class>();
            Assert.That(parent.Service, Is.Not.Null);
            parent.CreateChildren();

            Assert.That(parent.CreatedFromFactory, Is.All.Matches((Child2Class o) => o.Service == parent.Service));
        }
        public class Service2Class
        {
        }
        public class Parent2Class
        {
            public IFactory<Child2Class> ChildFactory { get; set; }
            public Service2Class Service { get; set; }
            public IEnumerable<Child2Class> CreatedFromFactory { get; private set; }

            public void CreateChildren()
            {
                var result = new List<Child2Class>();
                result.Add(ChildFactory.Create());
                result.Add(ChildFactory.Create());
                CreatedFromFactory = result;
            }
        }
        public class Child2Class
        {
            public Service2Class Service { get; set; }
        }
        public interface IFactory<T>
        {
            T Create();
        }

        [Test]
        public void ShouldCreateBoundedServices()
        {
            var container = new WindsorContainer();
            container.AddFacility<StartableFacility>();
            container.Register(Component.For<ServiceContainer>().LifeStyle.Transient);
            container.Register(Component.For<ServiceImplementation,IStartable>().ImplementedBy<ServiceImplementation>().LifeStyle.BoundTo<ServiceContainer>());

            var a = container.Resolve<ServiceContainer>();
            container.Release(a);
        }

        public class ServiceContainer
        {
            public ServiceImplementation a { get; set; }
        }

        public class ServiceImplementation : IStartable
        {
            public void Start()
            {
            }

            public void Stop()
            {
            }
        }

    }
}
