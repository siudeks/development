﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Facilities.TypedFactory;

namespace WindsorCastleTests.BoundedLifeStyle
{
    [TestFixture]
    public sealed class FactoryWithBoundedLifeStyleTests
    {
        [Test]
        public void ShouldCreateBoundedServiceWithFactory()
        {
            var container = new WindsorContainer();
            container.Register(
                Component.For<ScopeOwner>().LifeStyle.Transient,
                Component.For<OwnedElement>().LifeStyle.Transient,
                Component.For<ScopedService>().LifestyleBoundTo<ScopeOwner>(),
                Component.For(typeof(IFactory<>)).AsFactory());

            var scope = container.Resolve<ScopeOwner>();
            var otherScope = container.Resolve<ScopeOwner>();

            Assume.That(scope, Is.Not.SameAs(otherScope));
            Assume.That(scope.Service, Is.Not.SameAs(otherScope.Service));

            Assert.That(scope.Service, Is.SameAs(scope.Element.Service));
            var scopedService = scope.Factory.Create();
            Assert.That(scope.Service, Is.SameAs(scopedService));
        }

        public interface IFactory<T>
        {
            T Create();
        }

        public class ScopeOwner
        {
            public ScopedService Service { get; set; }
            public OwnedElement Element { get; set; }
            public IFactory<ScopedService> Factory { get; set; }
        }

        public class OwnedElement
        {
            public ScopedService Service { get; set; }
        }

        public class ScopedService
        {
        }
    }
}
