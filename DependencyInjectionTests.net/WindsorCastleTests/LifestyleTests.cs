﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindsorCastleTests
{
    [TestFixture]
    public sealed class LifestyleTests
    {
        [Test]
        public void ShouldDisposeReleasedInstance()
        {
            var container = new WindsorContainer();
            container.Register(Component.For<Disposable>().LifeStyle.Transient);

            var d = container.Resolve<Disposable>();
            Assert.That(d.IsDisposed, Is.False);

            container.Release(d);
            Assert.That(d.IsDisposed, Is.True);
        }

        [Test]
        public void ShouldDisposeDependency()
        {
            var container = new WindsorContainer();
            container.Register(Component.For<DisposableOwner>().LifeStyle.Transient);
            container.Register(Component.For<Disposable>().LifeStyle.Transient);

            var o = container.Resolve<DisposableOwner>();
            Assert.That(o.Disposable.IsDisposed, Is.False);

            container.Release(o);
            Assert.That(o.Disposable.IsDisposed, Is.True);
        }

        [Test]
        public void ShouldNotTraceTransparentINstanceWithoutDecomissioning()
        {
            var container = new WindsorContainer();
            container.Register(Component.For<PureData>().LifeStyle.Transient);

            var obj = container.Resolve<PureData>();
            var objref = new WeakReference(obj);
            obj = null;

            GC.Collect();

            Assert.That(objref.IsAlive, Is.False);
        }

    }

    public class Disposable : IDisposable
    {
        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            IsDisposed = true;
        }
    }

    public class DisposableOwner
    {
        public Disposable Disposable { get; set; }
    }

    public class PureData
    {
    }
}
