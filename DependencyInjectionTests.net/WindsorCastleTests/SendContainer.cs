﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindsorCastleTests
{
    public class SendContainer
    {
        public IGenericSend<int> Property1 { get; set; }
        public IGenericSend<int, string> Property2 { get; set; }
    }
}
