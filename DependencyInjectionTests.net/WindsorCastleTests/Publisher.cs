﻿namespace WindsorCastleTests
{
    using System;

    public class Publisher
    {
        public event EventHandler<PublishedEventArgs> OnEvent;

        public void RaiseEvent(PublishedEventArgs args)
        {
            OnEvent(this, args);
        }
    }
}
