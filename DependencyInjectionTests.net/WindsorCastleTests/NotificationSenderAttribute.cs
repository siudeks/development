﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindsorCastleTests
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NotificationSenderAttribute : Attribute
    {
    }
}
