﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace WindsorCastleTests
{
    [TestFixture]
    public sealed class ObservableFacilityTests
    {
        [Test]
        public void ShouldNotDeliverNonDistributedMessage()
        {
            var container = new WindsorContainer();
            container.AddFacility<ObservableFacility>();

            container.Register(Component.For(typeof(Observable<>)));
            container.Register(Component.For(typeof(Observer<>)));

            var subject = new Subject<int>();
            var handler = Substitute.For<ISubject<int>>();

            var sender = container.Resolve<Observable<int>>(new { sender = subject });
            var listener = container.Resolve<Observer<int>>(new { handler = handler });

            subject.OnNext(1);
            handler.DidNotReceive().OnNext(1);
        }

        [Test]
        public void ShouldDeliverDistributedMessage()
        {
            var container = new WindsorContainer();
            container.AddFacility<ObservableFacility>();

            container.Register(Component.For(typeof(ObservableWithAttribute<>)));
            container.Register(Component.For(typeof(ObserverWithAttribute<>)));

            var subject = new Subject<int>();
            var handler = Substitute.For<ISubject<int>>();

            var sender = container.Resolve<ObservableWithAttribute<int>>(new { sender = subject });
            var listener = container.Resolve<ObserverWithAttribute<int>>(new { handler = handler });

            subject.OnNext(1);
            handler.Received().OnNext(1);
        }

        [Test]
        public void ShouldNotTraceListener()
        {
            var container = new WindsorContainer();
            var facility = new ObservableFacility();
            container.AddFacility(facility);

            container.Register(Component.For(typeof(ObservableWithAttribute<>)).LifeStyle.Transient);
            container.Register(Component.For(typeof(ObserverWithAttribute<>)).LifeStyle.Transient);

            var subject = new Subject<int>();
            var sender = container.Resolve<ObservableWithAttribute<int>>(new { sender = subject });
            var listener = container.Resolve<ObserverWithAttribute<int>>();

            var list = new List<int>();
            var disposable = listener.Handler.Subscribe(list.Add);

            subject.OnNext(1);
            Assert.That(list, Is.EquivalentTo(new[] { 1 }));

            var listenerref = new WeakReference(listener);
            listener = null; 
            GC.Collect();
            Assert.That(listenerref.IsAlive, Is.False);

            disposable.Dispose();
            subject.OnNext(2);
            Assert.That(list, Is.EquivalentTo(new[] { 1 }));

        }
    }
}
