﻿namespace WindsorCastleTests
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.Resolvers.SpecializedResolvers;
    using Castle.Windsor;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class CastleWindsorTests
    {
        [Test]
        public void ShouldUseOpenGeneric()
        {
            var container = new WindsorContainer();

            container.Register
                (
                Component.For(typeof(IGenericSend<>)).ImplementedBy(typeof(GenericSend<>)),
                Component.For(typeof(IGenericSend<,>)).ImplementedBy(typeof(GenericSend<,>)),
                Component.For(typeof(SendContainer)).ImplementedBy(typeof(SendContainer))
                );

            var propertyContainer = container.Resolve<SendContainer>();
            Assert.IsNotNull(propertyContainer.Property1);
            Assert.IsNotNull(propertyContainer.Property2);
        }

        [Test]
        public void ShouldInjectCollectionIntoProperty()
        {
            var container = new WindsorContainer();
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel, true));

            container.Register
            (
                Component.For<object>().ImplementedBy<object>().Named("o1"),
                Component.For<object>().ImplementedBy<object>().Named("o2"),
                Component.For<ShouldInjectCollectionIntoPropertyClass1>().ImplementedBy<ShouldInjectCollectionIntoPropertyClass1>()
            );

            var instance = container.Resolve<ShouldInjectCollectionIntoPropertyClass1>();
            Assert.AreEqual(instance.Objects.Count(), 2);
        }

        public class ShouldInjectCollectionIntoPropertyClass1
        {
            public IEnumerable<object> Objects { get; set; }
        }
    }
}
