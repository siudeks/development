﻿using NUnit.Framework;
using System;
using System.Composition;
using System.Composition.Hosting;

namespace DependencyInjection
{
    [TestFixture]
    public sealed class FactoryMethodShould
    {
        [Test]
        public void InjectExportedToken()
        {
            var configuration = new ContainerConfiguration();
            var container = configuration
                .WithAssembly(GetType().Assembly)
                .CreateContainer();

            var token = container.GetExport<MyToken>();
        }
    }

    public class MyToken
    {
    }

    public class MyTokenProvider : ExportFactory<MyToken>
    {
        public MyTokenProvider(Func<Tuple<MyToken, Action>> exportCreator) : 
            base(exportCreator)
        {
        }
    }
}
