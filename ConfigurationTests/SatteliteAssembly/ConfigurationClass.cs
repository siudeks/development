﻿using System.Configuration;

namespace SatteliteAssembly
{
    public class ConfigurationClass
    {
        public string ValueFromConfiguration
        {
            get
            {
                return ConfigurationManager.AppSettings["key1"];
            }
        }

        public string ConnectionString1
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["connectionString1"].ConnectionString;
            }
        }
    }
}
