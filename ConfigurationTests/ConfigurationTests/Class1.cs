﻿using NUnit.Framework;
using SatteliteAssembly;

namespace ConfigurationTests
{
    [TestFixture]
    public sealed class ConfigurationTest
    {
        [Test]
        public void ShouldReadFromLocalConfiguration()
        {
            Assert.That(new ConfigurationClass().ValueFromConfiguration, Is.EqualTo("value2"));
            Assert.That(new ConfigurationClass().ConnectionString1, Is.EqualTo("connectionString2"));
        }
    }
}
