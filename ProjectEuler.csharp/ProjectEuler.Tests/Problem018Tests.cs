﻿using NUnit.Framework;
using System;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using static ProjectEuler.Problem018;

namespace ProjectEuler
{
    // https://projecteuler.net/problem=18
    [TestFixture]
    public class Problem018Tests
    {

        [Test]
        public void TotalsTest()
        {
            var empty = new Node[0];

            var root = new Node(1, new[]
                {
                    new Node(2, new[]
                    {
                        new Node(3, empty)
                    }),
                    new Node(4, new[]
                    {
                        new Node(5, new []
                        {
                            new Node(8, empty),
                        }),
                        new Node(6, empty)
                    })
                });

            var actual = withParentValue(1, root);
            Assert.That(actual.Value, Is.EqualTo(2));
            Assert.That(actual.Children[0].Value, Is.EqualTo(4));
            Assert.That(actual.Children[0].Children[0].Value, Is.EqualTo(7));
            Assert.That(actual.Children[1].Value, Is.EqualTo(6));
            Assert.That(actual.Children[1].Children[0].Value, Is.EqualTo(11));
            Assert.That(actual.Children[1].Children[0].Children[0].Value, Is.EqualTo(19));
            Assert.That(actual.Children[1].Children[1].Value, Is.EqualTo(12));

            Assert.That(findMax(0, actual), Is.EqualTo(19));

        }

        const string exampleTree = @"
3
7 4
2 4 6
8 5 9 3";

        const string problemTree = @"
75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23";

        [Test]
        public void SolutionTest()
        {
            Assert.That(solve(exampleTree), Is.EqualTo(23));

            var answer = solve(problemTree);
            Debug.WriteLine($"Answer is: {answer}");
        }

        private static int solve(string tree)
        {
            var empty = new Node[0];

            var treeLines = tree
                .Split('\n')
                .Where(it => !String.IsNullOrWhiteSpace(it))
                .Select(o => o.Split(' ').Select(it => new Node(Int32.Parse(it), empty)).ToArray())
                .ToArray();

            for (int row = treeLines.Length - 1; row >= 0; row--)
            {
                var rowLength = treeLines[row].Length;
                for (int index = 0; index < rowLength; index++)
                {
                    // every element need to be added to two parents - on left and right side

                    // right parent
                    if (index < rowLength - 1)
                    {
                        var parent = treeLines[row - 1][index];
                        parent = new Node(parent.Value, parent.Children.Concat(Enumerable.Repeat(treeLines[row][index], 1)).ToArray());
                        treeLines[row - 1][index] = parent;
                    }

                    // left parent
                    if (index > 0)
                    {
                        var parent = treeLines[row - 1][index - 1];
                        parent = new Node(parent.Value, parent.Children.Concat(Enumerable.Repeat(treeLines[row][index], 1)).ToArray());
                        treeLines[row - 1][index - 1] = parent;
                    }
                }
            }

            var root = treeLines[0][0];

            var newRoot = withParentValue(0, root);
            return findMax(0, newRoot);
        }
    }
}
