﻿using NUnit.Framework;
using System;
using System.Linq;

namespace ProjectEuler.Tests
{
    public partial class ProblemTests
    {
        [Test]
        public void Solution009Test()
        {
            Assume.That(Problem009.Solution(3 + 4 + 5).First(), Is.EqualTo(Tuple.Create(3, 4, 5)));

            var a = Problem009.Solution(1000).First();
        }
    }
}
