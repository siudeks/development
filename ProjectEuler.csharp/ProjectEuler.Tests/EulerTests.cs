﻿using NUnit.Framework;
using System.Collections.Generic;

namespace ProjectEuler.Tests
{
    [TestFixture]
    public sealed class EulerTests
    {
        [TestCase(2, new[] { 2 })]
        [TestCase(3, new[] { 3 })]
        [TestCase(4, new[] { 2, 2 })]
        [TestCase(28, new[] { 2, 2, 7 })]
        [TestCase(10, new[] { 2, 5 })]
        public void FactorizeTest(int value, IEnumerable<int> divisors)
        {
            Assert.That(Euler.Factorize(value), Is.EquivalentTo(divisors));
        }
    }
}
