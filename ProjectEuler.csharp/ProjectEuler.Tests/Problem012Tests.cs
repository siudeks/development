﻿using NUnit.Framework;
using System.Linq;

namespace ProjectEuler.Tests
{
    // https://projecteuler.net/problem=12

    [TestFixture]
    public class Problem012Tests
    {
        [Test]
        public void SolutionTest()
        {
            var result = Problem012.solution(501);
            Assert.That(result, Is.GreaterThan(0));
        }

        [TestCase(3, 2)]
        [TestCase(6, 4)]
        [TestCase(10, 4)]
        [TestCase(15, 4)]
        [TestCase(21, 4)]
        [TestCase(28, 6)]
        //[TestCase(2031120,240)]
        [TestCase(48, 10)]
        public void DivisorsCountTest(int value, int expected)
        {
            var factors = Euler.Factorize(value);

            Assume.That(Problem012.divisorsCount(factors), Is.EqualTo(expected));
        }

        [Test]
        public void TriangleNumbersTest()
        {
            Assert.That(Problem012.triangleNumbers().Take(7), Is.EquivalentTo(new[] { 1, 3, 6, 10, 15, 21, 28 }));
        }

        // first item is the number of divisors, the rest - list of factors)
        [TestCase(new long[] { 2, 2 })]
        [TestCase(new long[] { 4, 2, 3 })]
        [TestCase(new long[] { 12, 2, 2, 3, 3, 3 })]
        public void DivisorsCountTest(long[] items)
        {
            var expected = items.First();
            var factors = items.Skip(1);
            var actual = Problem012.divisorsCount(factors);

            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
