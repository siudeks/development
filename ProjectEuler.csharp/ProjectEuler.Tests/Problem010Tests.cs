﻿using NUnit.Framework;

namespace ProjectEuler.Tests
{
    [TestFixture]
    public class Problem010Tests
    {
        // https://projecteuler.net/problem=10
        [Test]
        public void Solution010Test()
        {
            var a = new[] { 1, 2 };
            Assume.That(Problem010.Solution(10), Is.EqualTo(17));
            Assert.That(Problem010.Solution(2000000), Is.EqualTo(142913828922));
        }
    }
}
