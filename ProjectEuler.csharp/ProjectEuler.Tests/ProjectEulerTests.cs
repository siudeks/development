﻿using NUnit.Framework;

namespace ProjectEuler.Tests
{
    [TestFixture]
    public sealed class ProjectEulerTests
    {

        [Test, Ignore("Doesn't work and I don't remember (and care) why")]
        public void SolveProblem6()
        {
            Assert.That(Problem6.SumOfSquares(10), Is.EqualTo(385));
            Assert.That(Problem6.SquareOfSum(10), Is.EqualTo(3025));
            Assert.That(Problem6.Solution(10), Is.EqualTo(2640));

            Assert.That(Problem6.Solution(100), Is.EqualTo(2640));
        }
    }
}
