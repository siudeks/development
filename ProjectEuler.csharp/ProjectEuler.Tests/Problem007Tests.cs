﻿using NUnit.Framework;

namespace ProjectEuler.Tests
{
    // https://projecteuler.net/problem=7

    [TestFixture]
    public class Problem007Tests
    {
        [Test]
        public void SolveProblem7()
        {
            Assert.That(Problem7.FindPrime(6), Is.EqualTo(13));

            var result = Problem7.FindPrime(10001);
        }

    }
}
