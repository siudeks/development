﻿using NUnit.Framework;

namespace ProjectEuler
{
    // https://projecteuler.net/problem=14

    [TestFixture]
    public class Problem014Tests
    {
        [Test]
        public void CollatzSequenceTest()
        {
            var value = Problem014.CollatzSequence(13, 0);
            Assert.That(value, Is.EqualTo(10));
        }

        [Test]
        public void SolutionTest()
        {
            var value = Problem014.solution();
            Assert.That(value, Is.GreaterThan(0));
        }

    }
}
