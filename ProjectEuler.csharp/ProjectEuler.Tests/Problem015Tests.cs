﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace ProjectEuler
{
    // https://projecteuler.net/problem=15
    [TestFixture]
    public class Problem015Tests
    {
        [Test]
        public void SolutionTestForProvidedValues()
        {
            var value = Problem015.solution(2);
            Assert.That(value, Is.EqualTo(6));

            // line below can take around 10 mins
            Assert.That(Problem015.solution(20), Is.EqualTo(137846528820));
        }

        [Test]
        public void SolutionTestWithCaching()
        {
            var value = reduce(20, 20, new Dictionary<Tuple<int, int>, long>());
            Assert.That(value, Is.EqualTo(137846528820));
        }

        private static long reduce(int x, int y, Dictionary<Tuple<int, int>, long> cache)
        {
            if (x == 0 || y == 0) return 1;

            var key = Tuple.Create(x, y);
            long value;
            if (!cache.TryGetValue(key, out value))
            {
                value = reduce(x - 1, y, cache) + reduce(x, y - 1, cache);
                cache[key] = value;
            }
            return value;
        }
    }
}
