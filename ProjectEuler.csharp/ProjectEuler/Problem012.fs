﻿namespace ProjectEuler

open System.Numerics
open System
open FSharp.Collections.ParallelSeq

module Problem012 = 
    let triangleNumbers() = Seq.unfold (fun (acc, index) -> Some(acc + index, (acc + index, index + 1L))) (0L, 1L)
    
    // http://stackoverflow.com/questions/110344/algorithm-to-calculate-the-number-of-divisors-of-a-given-number
    // Essentially it boils down to if your number n is:
    // n = a^x * b^y * c^z
    // (where a, b, and c are n's prime divisors and x, y, and z are the number of times that divisor is repeated) 
    // then the total count for all of the divisors is: 
    // (x + 1) * (y + 1) * (z + 1).
    let divisorsCount (factors : seq<int64>) = 
        factors
        |> Seq.groupBy (fun it -> it)
        |> Seq.map (fun it -> (snd (it) |> Seq.length) + 1)
        |> Seq.reduce (fun acc it -> acc * it)
    
    let solution limit = 
        triangleNumbers()
        |> Seq.map (fun it -> (it, Euler.Factorize it))
        |> Seq.map (fun it -> (fst (it), divisorsCount (snd it)))
        |> Seq.where (fun it -> snd (it) >= limit)
        |> Seq.map fst
        |> Seq.head
