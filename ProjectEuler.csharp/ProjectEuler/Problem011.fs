﻿namespace ProjectEuler

open System.Numerics
open System
open FSharp.Collections.ParallelSeq

module Problem011 =
    let Solution (data:byte[]) xsize ysize size =
        let xlimit = xsize - 1
        let ylimit = ysize - 1
        let hline (x:int,y:int) = seq { for i in 0..size-1 do yield (x+i,y)} |> Array.ofSeq
        let vline (x:int,y:int) = seq { for i in 0..size-1 do yield (x,y+i)} |> Array.ofSeq
        let dline1 (x:int,y:int) = seq { for i in 0..size-1 do yield (x+i,y+i)} |> Array.ofSeq
        let dline2 (x:int,y:int) = seq { for i in 0..size-1 do yield (x-i,y+i)} |> Array.ofSeq
        let isValid (arr:(int*int)array) = Array.exists (fun (x,y) -> x > xlimit || y > ylimit || x < 0 || y < 0) arr |> not
        let value (arr:(int*int)array) = Array.fold (fun acc (x,y) -> acc * int data.[y*xsize+x]) 1 arr

        let points = seq { for y in 0..ylimit do
                             for x in 0..xlimit do
                                 let point = (x,y)
                                 yield hline point
                                 yield vline point
                                 yield dline1 point
                                 yield dline2 point
                         }

        points |> Seq.where isValid |> Seq.map value |> Seq.max
        
        
