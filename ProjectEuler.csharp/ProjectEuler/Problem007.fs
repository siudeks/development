﻿namespace ProjectEuler

open System.Numerics

module Problem7 =
    let FindPrime orderNo = 
        let rec iter (optionalPrime:int64) found = 
          if Euler.IsPrime optionalPrime then
            if found+1 = orderNo then optionalPrime
            else iter (optionalPrime+1L) (found+1)
          else iter (optionalPrime+1L) found
        iter 2L 0