﻿namespace ProjectEuler

module Problem018 =
  
  type Node (value: int, children: Node[]) = 
    member this.Value = value
    member this.Children = children
    
  let rec withParentValue (parentValue: int, node: Node) =
    match node.Children with 
    | null | [| |] -> Node(node.Value + parentValue, Array.empty) 
    | _ -> Node(node.Value + parentValue, node.Children |> Array.map (fun it -> withParentValue (node.Value + parentValue, it) )) 
    
  let rec findMax (currentMax: int)(node: Node) =
    match node.Children with
    | [| |] -> if currentMax > node.Value then currentMax else node.Value
    | _ -> node.Children |> Array.fold findMax currentMax

  let solution (size) =
      let rec reduce (x, y) = 
          if (x = 0 || y = 0) then 1
          else reduce (x-1, y) + reduce (x, y-1)

      reduce (size, size)

  