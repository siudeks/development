﻿namespace ProjectEuler

open System.Numerics
open System

module Problem009 =
    let isValidTriangle (a, b, c) = 
        if a + b <= c then false
        else if b + c <= a then false
             else if c + a <= b then false
                  else true
    let isPythagoreanTriangle (a, b, c) = a*a + b*b = c*c


    let Solution expectedSum =
        seq { for a in 1..1000 do
              for b in 1..1000 do
              for c in 1..1000 do yield (a, b, c) 
            }
        |> Seq.filter isValidTriangle
        |> Seq.filter isPythagoreanTriangle
        |> Seq.filter (fun (a,b,c) -> a+b+c=expectedSum)
        

