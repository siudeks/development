﻿namespace ProjectEuler

open System.Numerics

module Problem6 =
    let SumOfSquares max = [1L..max] |> List.map (fun x -> x *x ) |> List.sum
    let SquareOfSum max = 
        let aggr = [1L..max] |> List.sum 
        aggr * aggr
    let Solution max = SquareOfSum max - SumOfSquares max
