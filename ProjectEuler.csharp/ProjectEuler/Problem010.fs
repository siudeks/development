﻿namespace ProjectEuler

open System.Numerics
open System
open FSharp.Collections.ParallelSeq

module Problem010 =
    let Solution topLimit = 
        let sumPrimes (x:int64[]) = x |> Array.filter Euler.IsPrime |> Array.fold (fun acc o -> acc + o) 0L
        seq {for i in 2L..topLimit do yield i }
        |> Seq.chunkBySize 1000
        |> PSeq.sumBy sumPrimes
        
        
