﻿namespace ProjectEuler

open System.Numerics

module Euler =
    let IsPrime (value:int64) = 
        if value = 1L then false
        else seq { 2L..int64 (sqrt (float value)) } |> Seq.exists (fun o -> (value % o) = 0L) |> not
    let internal firstDivisor (value:int64) =
        seq { for i in 2L..int64 (sqrt (float value)) do if value % i = 0L then yield i } |> Seq.tryHead

    /// <summary>
    /// Factorizes given number.
    /// </summary>
    /// <param name="value"></param>
    let Factorize (value:int64) = 
        let unfold state = 
            match state with
            | None -> None
            | Some(s) -> match firstDivisor s with
                         | None -> Some(s, None)
                         | Some(x) -> Some(x, Some(s / x))

        let divisorSeq = Seq.unfold unfold (Some(value))
                                    
        if value = 1L then seq { 1L..1L }
        else divisorSeq 
