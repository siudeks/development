﻿namespace ProjectEuler

open System.Numerics
open System

module Problem008 =

    let Solution (data:seq<byte>) size =
        let products = 
            Seq.windowed size data
            |> Seq.map (fun arr -> Array.fold (fun acc a1 -> acc * (int64)a1) 1L arr)
        Seq.max products

