﻿namespace ProjectEuler

module Problem014 = 
    let rec CollatzSequence (i:int64) current = 
        if i <= 1L then current+1
        else if i % 2L = 0L then CollatzSequence (i / 2L) (current+1)
        else CollatzSequence (i * 3L + 1L) (current+1)

    let solution() = 
        let rec solutionIter (currentValue, resultValue, resultLength) = 
            if currentValue > 1000000L then resultValue
            else 
                let seqLength = CollatzSequence currentValue 0
                let nextIterParam = 
                    if seqLength > resultLength 
                    then (currentValue + 1L, currentValue, seqLength) 
                    else (currentValue + 1L, resultValue, resultLength)
                solutionIter nextIterParam
        solutionIter (0L, 0L, 0)
         
    
