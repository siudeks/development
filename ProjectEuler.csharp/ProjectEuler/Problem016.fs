﻿namespace ProjectEuler

module Problem016 =
  let solution (size) =
      let rec reduce (x, y) = 
          if (x = 0 || y = 0) then 1
          else reduce (x-1, y) + reduce (x, y-1)

      reduce (size, size)

  