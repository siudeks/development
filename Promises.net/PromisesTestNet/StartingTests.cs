﻿namespace Promises
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    [TestFixture]
    public sealed class StartingTests
    {
        [Test]
        public void ShouldBeStartedWhenCreatedInFactory()
        {
            var waitHandle = new ManualResetEvent(false);
            var task = Task.Factory.StartNew(() => 
            {
                waitHandle.WaitOne();
            });

            Assert.Inconclusive("Need to be finished");
        }
    }
}
