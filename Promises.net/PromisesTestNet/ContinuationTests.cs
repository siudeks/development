﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Promises
{
    [TestFixture]
    public sealed class ContinuationTests
    {
        [Test]
        public void ShouldContinueOnCompleted()
        {
            var isCanceled = false;
            var isFaulted = false;
            var isCompleted = false;

            var task = Task<int>.FromResult(0);
            task.ContinueWith(o =>
            {
                isCanceled = o.IsCanceled;
                isFaulted = o.IsFaulted;
                isCompleted = o.IsCompleted;
            }).Wait();

            Assert.IsTrue(isCompleted);

            Assert.IsFalse(isCanceled);
            Assert.IsFalse(isFaulted);
        }

        [Test]
        public void ShouldContinueOnFailed()
        {
            var isCanceled = false;
            var isFaulted = false;
            var isCompleted = false;

            var task = Task.Run(() => { throw new Exception(); });
            task.ContinueWith(o =>
            {
                isFaulted = o.IsFaulted;
                isCompleted = o.IsCompleted;
                isCanceled = o.IsCanceled;
            })
            .Wait();

            Assert.IsFalse(isCanceled);
            Assert.IsTrue(isFaulted);
            Assert.IsTrue(isCompleted);
        }

        [Test]
        public void ShouldContinueOnCancelled()
        {
            var isCanceled = false;
            var isFaulted = false;
            var isCompleted = false;

            var token = new CancellationToken(true);
            var task = Task.Run(() => { throw new Exception(); }, token);
            task.ContinueWith(o =>
            {
                isFaulted = o.IsFaulted;
                isCompleted = o.IsCompleted;
                isCanceled = o.IsCanceled;
            })
            .Wait();

            Assert.IsTrue(isCanceled);
            Assert.IsFalse(isFaulted);
            Assert.IsTrue(isCompleted);
        }

        [Test]
        public void ShouldBeCompletedWhenEmptyCollection()
        {
            var tasks = Enumerable.Empty<Task>();
            Assert.That(tasks.AsSequence().IsCompleted, Is.True);
        }

        [Test]
        public void ShouldBeResultCompletedWhenSubtaskSuccessful()
        {
            var subtaskTcs = new TaskCompletionSource<object>();
            var tasks = new[] { subtaskTcs.Task };
            var task = tasks.AsSequence();
            Assert.That(task.IsCompleted, Is.False);

            subtaskTcs.SetResult(null);
            task.Wait();
            Assert.That(task.IsCompleted, Is.True);
            Assert.That(task.IsFaulted, Is.False);
        }

        [Test]
        public void ShouldBeResultCompletedWhenSubtaskFailed()
        {
            var subtaskTcs = new TaskCompletionSource<object>();
            var tasks = new[] { subtaskTcs.Task };
            var task = tasks.AsSequence();
            Assert.That(task.IsCompleted, Is.False);

            subtaskTcs.SetException(new Exception());
            try
            {
                task.Wait();
            }
            catch (AggregateException)
            {
            }
            Assert.That(task.IsCompleted, Is.True);
            Assert.That(task.IsFaulted, Is.True);
        }

        [Test]
        public void ShouldNotContinueWhenInitialTaskPending()
        {
            var subtaskTcs = new TaskCompletionSource<object>();
            var enumerator = new TaskEnumerator(subtaskTcs.Task);
            var task = enumerator.Enumerate().AsSequence();
            Assert.That(task.IsCompleted, Is.False);
            Assert.That(enumerator.Finished, Is.False);
        }

        [Test]
        public void ShouldContinueEnumerationWhenSubtaskFailed()
        {
            var subtaskTcs = new TaskCompletionSource<object>();
            subtaskTcs.SetException(new Exception());
            var enumerator = new TaskEnumerator(subtaskTcs.Task);
            var task = enumerator.Enumerate().AsSequence();
            try
            {
                task.Wait();
            }
            catch (AggregateException)
            {
            }
            Assert.That(enumerator.Finished, Is.True);
            Assert.That(task.IsCompleted, Is.True);
            Assert.That(task.IsFaulted, Is.True);
        }

        [Test]
        public void ShouldContinueCancelled()
        {
            var tcs = new TaskCompletionSource<object>();
            int i = 0;
            tcs.SetCanceled();
            tcs.Task.ContinueWith(o =>
                {
                    Interlocked.Increment(ref i);
                });

            try
            {
                tcs.Task.Wait();
            }
            catch (Exception)
            {
            }

            Assert.That(i == 1);
        }

        class TaskEnumerator
        {
            public bool Finished { get; private set; }

            private Task[] subtasks;
            public TaskEnumerator(params Task[] subtasks)
            {
                this.subtasks = subtasks;
            }

            public IEnumerable<Task> Enumerate()
            {
                foreach (var task in subtasks)
                {
                    yield return task;
                }
                Finished = true;
            }
        }

        [Test]
        public void ShouldNotContinueEnumerationWhenEnumerationFailed()
        {
            var enumerator = new TaskEnumeratorWithException();
            var task = enumerator.Enumerate().AsSequence();
            try
            {
                task.Wait();
            }
            catch (AggregateException)
            {
            }
            Assert.That(enumerator.Finished, Is.False);
            Assert.That(task.IsCompleted, Is.True);
            Assert.That(task.IsFaulted, Is.True);
        }

        class TaskEnumeratorWithException
        {
            public bool Finished { get; private set; }

            public IEnumerable<Task> Enumerate()
            {
                yield return Task.FromResult(1);

                throw new Exception();
            }
        }

        [Test]
        public void ShouldRunMultipleContinuations()
        {
            int counter = 0;
            var task = Task.Factory.StartNew(() => { });
            var subtask1 = task.ContinueWith(t => { Interlocked.Increment(ref counter); });
            var subtask2 = task.ContinueWith(t => { Interlocked.Increment(ref counter); });
            Task.WaitAll(subtask1, subtask2);
            Thread.MemoryBarrier();
            Assert.That(counter, Is.EqualTo(2));
        }
    }
}
