﻿using NUnit.Framework;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Promises
{
    [TestFixture]
    public sealed class CancellationTests
    {
        [Test]
        public void ShouldNotStartTaskWhenTokenIsAlreadyCancelled()
        {
            var token = new CancellationToken(true);
            var task = new Task(() => { }, token);

            Assert.Throws<InvalidOperationException>(() => { task.Start(); });
        }

        [Test]
        public void ShouldStateBeCancelledWhenTokenIsAlreadyCancelled()
        {
            var token = new CancellationToken(true);
            var task = new Task(() => { }, token);

            Assert.That(task.Status, Is.EqualTo(TaskStatus.Canceled));
        }

        [Test]
        public void ShouldComposedSourceHandleCancellationOnFirstToken()
        {
            var source1 = new CancellationTokenSource();
            var source2 = new CancellationTokenSource();

            var composedTokenSource = CancellationTokenSource.CreateLinkedTokenSource(source1.Token, source2.Token);

            source1.Cancel();
            Assert.That(composedTokenSource.IsCancellationRequested, Is.True);
        }

        [Test]
        public void ShouldComposedSourceHandleCancellationOnSecondToken()
        {
            var source1 = new CancellationTokenSource();
            var source2 = new CancellationTokenSource();

            var composedTokenSource = CancellationTokenSource.CreateLinkedTokenSource(source1.Token, source2.Token);

            source2.Cancel();
            Assert.That(composedTokenSource.IsCancellationRequested, Is.True);
        }

        [Test]
        public void ShouldTaskBeCancelledWhenThrowOperationCanceledException()
        {
            var task = Task.Factory.StartNew(() =>
                {
                    throw new OperationCanceledException();
                });

            task.Wait();
            Assert.That(task.Status, Is.EqualTo(TaskStatus.Canceled));
        }
    }
}
