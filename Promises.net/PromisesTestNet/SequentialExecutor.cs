﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promises
{
    public static class SequentialExecutor
    {
        public static Task AsSequence(this IEnumerable<Task> tasks)
        {
            var enumerator = tasks.GetEnumerator();
            var tcs = new TaskCompletionSource<object>();

            Continuation(tcs, enumerator);
            return tcs.Task;
        }

        private static void Continuation(TaskCompletionSource<object> tcs, IEnumerator<Task> enumerator)
        {
            var canContinue = default(bool);
            var exception = default(Exception);
            try
            {
                canContinue = enumerator.MoveNext();
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            if (!canContinue)
            {
                enumerator.Dispose();
                if (exception != null)
                    tcs.TrySetException(exception);
                else
                    tcs.TrySetResult(null);
                return;
            };

            enumerator.Current.ContinueWith(o => {
                if (o.IsFaulted) tcs.TrySetException(o.Exception.InnerException);
                if (o.IsCanceled) tcs.TrySetCanceled();
                Continuation(tcs, enumerator);
            });
        }
    }
}
