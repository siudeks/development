﻿/// <reference path="./typings/angularjs/angular.d.ts"/>
/// <reference path="./typings/angularjs/angular-route.d.ts"/>

var app = angular.module('App', []);

module MyModule {
    export class HomeController {
    }
}

app.controller('HomeController', MyModule.HomeController);

