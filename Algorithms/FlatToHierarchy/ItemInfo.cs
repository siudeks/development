﻿namespace Algorithms
{
    public sealed class ItemInfo
    {
        public int ItemId { get; set; }
        public int ParentId { get; set; }
    }
}
