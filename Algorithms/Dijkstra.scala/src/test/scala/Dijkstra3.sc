object Dijkstra3 {
  def paths[T](map: Map[T, List[T]], visited: Set[(T,T)], result: List[List[T]]) : List[List[T]] = result match {
    case headPaths :: rest_paths => headPaths match {
      case head :: rest =>
        val visiting = map(head).filter(!visited.contains(_))
        val revisited
        if (newItems.isEmpty) result
        else

        val newPath = map(head).flatMap(o => if (visited.contains(o)) Nil else List(o :: headPaths))
        Nil
    }
  }


  val items = Map[String, List[String]] (
    "a" -> List("b","e"),
    "b" -> List("c"),
    "c" -> List("d"),
    "d" -> List("e")
  )
  paths(items, Set.empty, List(List("a")))
}