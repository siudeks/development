import org.scalatest.FlatSpec

import scala.collection.immutable.{HashSet, HashMap}

/**
 * http://rosettacode.org/wiki/Dijkstra%27s_algorithm
 * http://codereview.stackexchange.com/questions/44195/dijkstra-like-routing-algorithm
 * http://rosettacode.org/wiki/Dijkstra%27s_algorithm#Scala
 * http://edu.i-lo.tarnow.pl/inf/alg/001_search/0138.php
 */
class DijkstraSpec extends FlatSpec {

//  it should "find the shortest path 2x1" in {
//    val a = "a"
//    val b = "b"
//
//    val edges = Set(
//      new Edge(a, b, 1)
//    )
//
//    assert(Dijkstra.shortestPath(edges, a, b) === Array(a, b))
//  }

  it should "find the shortest path 2x2" in {
    val a = "a"
    val b = "b"
    val c = "c"
    val d = "d"

    val edges = Set(
      new Edge(a, d, 10),
      new Edge(a, b, 1),
      new Edge(b, c, 1),
      new Edge(c, d, 1)
    )

    assert(Dijkstra.shortestPath(edges, a, b) === Array(a, b, c, d))
  }

//  it should "find the shortest path" in {
//    val a = "a"
//    val b = "b"
//    val c = "c"
//    val d = "d"
//    val e = "e"
//    val f = "f"
//
//    val edges = Set(
//      new Edge(a, b, 7),
//      new Edge(a, c, 9),
//      new Edge(a, f, 14),
//      new Edge(b, c, 10),
//      new Edge(b, d, 15),
//      new Edge(c, d, 11),
//      new Edge(c, f, 2),
//      new Edge(d, e, 6),
//      new Edge(e, f, 9)
//    )
//
//    assert(Dijkstra.shortestPath(edges, a, e) === Array(a, c, d, e))
//  }
}

class Edge[T](val start: T, val end: T, val cost: Int) {
}

object Dijkstra {
  def shortestPath[Key](graph: Set[Edge[Key]], start: Key, end: Key): List[Key] = {

    def shortestPathIter(lookup: Map[Key, List[(Key, Int)]], sortedPaths: List[(List[Key], Double)], visited: Set[Key]): List[Key] = sortedPaths match {
      case shortestPath :: restOfPaths => shortestPath match {
        case (pathHead :: path_rest, weight) =>
          if (pathHead == end) shortestPath._1.reverse
          else {
            val newPaths = lookup(pathHead)
              .flatMap { case (k, w) => if (visited.contains(k)) List.empty else List((k :: shortestPath._1, weight + w))}
              //              .sortBy(_._2)
            val allPaths = newPaths ++ sortedPaths
            val sortedNewPaths = allPaths.sortWith( { case ((_, d1), (_, d2)) => d1 < d2})
            println(sortedNewPaths)
            shortestPathIter(lookup, sortedNewPaths, visited + pathHead)
          }
      }
    }

    val lookup: Map[Key, List[(Key, Int)]] = graph.groupBy(_.start).mapValues(_.map(o => (o.end, o.cost)).toList)
    shortestPathIter(lookup, List((List(start), 0d)), Set.empty )
  }
}


