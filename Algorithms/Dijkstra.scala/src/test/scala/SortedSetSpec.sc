import scala.collection.immutable.TreeSet

object Spec {
  //  def run(): (String, Int) = {
  //    val asia = ("Asia", 40)
  //    val zosia = ("Zosia", 20)
  //
  //    object PersonOrdering extends Ordering[(String, Int)] {
  //      override def compare(x: (String, Int), y: (String, Int)): Int = x._2 - y._2
  //    }
  //    TreeSet[(String, Int)](asia, zosia).firstKey
  //  }
  //
  ////  run()
  //  // val a: Boolean = Value(2) > Value(1)

  case class Value(i: Int) extends Ordered[Value] {
    def compare(that: Value) = this.i - that.i
  }

  TreeSet(Value(2),Value(1), Value(3))}

