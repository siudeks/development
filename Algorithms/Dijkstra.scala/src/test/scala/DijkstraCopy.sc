
object Temp {
  Dijkstra2.main
}

object Dijkstra2 {

  type Path[Key] = (Double, List[Key])

  def Dijkstra[Key](lookup: Map[Key, List[(Key, Double)]], proposals: List[Path[Key]], dest: Key, visited: Set[Key]): Path[Key] = proposals match {
    case (dist, path) :: fringe_rest => path match {
      case key :: path_rest =>
        if (key == dest) (dist, path.reverse)
        else {
          val paths = lookup(key).flatMap { case (key, d) => if (!visited.contains(key)) List((dist + d, key :: path)) else List.empty}
          val sorted_proposals = (paths ++ fringe_rest).sortWith { case ((d1, _), (d2, _)) => d1 < d2}
          Dijkstra(lookup, sorted_proposals, dest, visited + key)
        }
    }
    case Nil => (0, List())
  }

  def main(): Unit = {
    val lookup1 = Map(
      "a" -> List(("b", 7.0), ("c", 9.0), ("f", 14.0)),
      "b" -> List(("c", 10.0), ("d", 15.0)),
      "c" -> List(("d", 11.0), ("f", 2.0)),
      "d" -> List(("e", 6.0)),
      "e" -> List(("f", 9.0)),
      "f" -> Nil
    )

    val lookup2 = Map(
      "a" -> List(("b", 1.0), ("d", 3.0)),
      "b" -> List(("c", 1.0)),
      "c" -> List(("d", 1.0)),
      "d" -> Nil
    )

    val lookup3 = Map(
      "a" -> List(("b", 1.0), ("c", 1.0)),
      "b" -> List(("c", 1.0)),
      "c" -> Nil
    )
    val res = Dijkstra[String](lookup3, List((0d, List("a"))), "c", Set())
    println(res)
  }
}