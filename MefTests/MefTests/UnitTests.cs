﻿namespace MefTests
{
    using System.ComponentModel.Composition.Hosting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void ShouldInjectGenericDependency()
        {
            var container = new CompositionContainer(new AssemblyCatalog(this.GetType().Assembly));

            var service = container.GetExportedValue<Client>();

            Assert.IsNotNull(service.RequestSender);

        }
    }
}
