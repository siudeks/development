﻿namespace MefTests
{
    using System.ComponentModel.Composition;

    public interface IService
    {
        TResult Service1<TValue, TResult>(TValue value);
    }

    [Export(typeof(IService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public sealed class Service : IService
    {
        public Service()
        {

        }

        public TResult Service1<TValue, TResult>(TValue value)
        {
            return default(TResult);
        }
    }

    public interface IGenericService<TValue, TResult>
    {
        TResult Service1(TValue value);
    }

    [Export(typeof(IGenericService<,>))]
    [PartCreationPolicy(CreationPolicy.NonShared)] // Can be CreationPolicy.Shared, but we are testing creation of new instances.
    public sealed class GenericServiceFactory<TValue, TResult> : IGenericService<TValue, TResult>
    {
        private readonly IService service;

        [ImportingConstructor]
        public GenericServiceFactory(IService service)
        {
            this.service = service;
        }

        public TResult Service1(TValue value)
        {
            return service.Service1<TValue, TResult>(value);
        }
    }
}
