﻿namespace MefTests
{
    using System.ComponentModel.Composition;

    [Export]
    public class Client
    {
        [Import]
        public IGenericService<string, int> RequestSender { get; set; }
    }
}
