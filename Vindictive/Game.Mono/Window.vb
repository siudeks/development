﻿public Class Window

    public bounds As Rectangle
    private borderColor As Color
    private backgrounborderColor As Color
    private drawBG As Boolean

    public Sub New(x As Integer, y As Integer, width As Integer, height As Integer, bgColor As Color, bColor As Color, bg As Boolean)
        bounds = New Rectangle(x, y, width, height)
        backgrounborderColor = bgColor
        borderColor = bColor
        drawBG = bg
    End Sub

    public Sub Draw(x, y, alpha)
        bounds.X = x
        bounds.Y = y
        Dim sourceRect As Rectangle
        Dim destRect As Rectangle
        ' Draw Transparent background for menu leaving 3 pixels less on all sides for the slighty rounded corners.
        If drawBG Then
            sourceRect = New Rectangle(0, 0, 64, 32)
            destRect = New Rectangle(bounds.X + 3, bounds.Y + 3, bounds.Width - 6, bounds.Height - 6)
            Globals.SpriteBatch.Draw(Textures.Menu, destRect, sourceRect, backgrounborderColor * 0.85 * alpha)
        End If

        ' Draw Top Left Corner
        sourceRect = New Rectangle(64, 0, 16, 16)
        destRect = New Rectangle(bounds.X, bounds.Y, 16, 16)
        Globals.SpriteBatch.Draw(Textures.Menu, destRect, sourceRect, borderColor * alpha)

        ' Draw Top Middle
        sourceRect = New Rectangle(80, 0, 16, 16)
        destRect = New Rectangle(bounds.X + 16, bounds.Y, bounds.Width - 32, 16)
        Globals.SpriteBatch.Draw(Textures.Menu, destRect, sourceRect, borderColor * alpha)

        'Draw Top Right Corner
        sourceRect = New Rectangle(112, 0, 16, 16)
        destRect = New Rectangle(bounds.X + bounds.Width - 16, bounds.Y, 16, 16)
        Globals.SpriteBatch.Draw(Textures.Menu, destRect, sourceRect, borderColor * alpha)

        ' Draw Left Side
        sourceRect = New Rectangle(64, 16, 16, 16)
        destRect = New Rectangle(bounds.X, bounds.Y + 16, 16, bounds.Height - 32)
        Globals.SpriteBatch.Draw(Textures.Menu, destRect, sourceRect, borderColor * alpha)

        ' Draw Right Side
        sourceRect = New Rectangle(112, 16, 16, 16)
        destRect = New Rectangle(bounds.X + bounds.Width - 16, bounds.Y + 16, 16, bounds.Height - 32)
        Globals.SpriteBatch.Draw(Textures.Menu, destRect, sourceRect, borderColor * alpha)

        ' Draw Bottom Left Corner
        sourceRect = New Rectangle(64, 48, 16, 16)
        destRect = New Rectangle(bounds.X, bounds.Y + bounds.Height - 16, 16, 16)
        Globals.SpriteBatch.Draw(Textures.Menu, destRect, sourceRect, borderColor * alpha)

        ' Draw Bottom Middle
        sourceRect = New Rectangle(80, 48, 16, 16)
        destRect = New Rectangle(bounds.X + 16, bounds.Y + bounds.Height - 16, bounds.Width - 32, 16)
        Globals.SpriteBatch.Draw(Textures.Menu, destRect, sourceRect, borderColor * alpha)

        'Draw Bottom Right Corner
        sourceRect = New Rectangle(112, 48, 16, 16)
        destRect = New Rectangle(bounds.X + bounds.Width - 16, bounds.Y + bounds.Height - 16, 16, 16)
        Globals.SpriteBatch.Draw(Textures.Menu, destRect, sourceRect, borderColor * alpha)
    End Sub
End Class
