﻿namespace GameEngine
{
    using System;

    public class PacketHandler
    {
        public Action<string> Disconnect;
        public Action<string> Connect;
        public Action<string> ConnectedUsers;
        public Action<string> Broadcast;
        public Action<string> Location;
        public Action<string> MapData;
        public Action<string> TileData;
        public Action<string> Ping;

        public PacketHandler(UserConnection user)
        {
            user.LineReceived += IncomingPacket;
        }

        public void IncomingPacket(byte id, string data)
    {
        switch (id)
        {
            case PacketID.Disconnect:
                Disconnect(data);
            case PacketID.Connect:
                Connect(data);
            case PacketID.ConnectedUsers:
                ConnectedUsers(data);
            case PacketID.Broadcast:
                Broadcast(data);
            case PacketID.Location:
                Location(data);
            case PacketID.MapData:
                MapData(data);
            case PacketID.TileData:
                TileData(data);
            case PacketID.Ping;
                Ping(data);
        }

    }
}