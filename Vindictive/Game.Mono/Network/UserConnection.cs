﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace GameEngine
{
    public class UserConnection
    {
        public Action<string> ConnectionStatus { get; set; }
        public Action Connected { get; set; }
        public Action Disconnected { get; set; }
        public Action<byte, string> LineReceived { get; set; }

        public PacketHandler PacketHandler;
        public string UserName = string.Empty;

        private TcpClient client;
        private byte[] readBuffer = new byte[1024];
        private StringBuilder packetBuffer = new StringBuilder();

        public UserConnection()
        {
            PacketHandler = new PacketHandler(this);
        }

        public void Connect(string user)
        {
            UserName = user;
            ConnectionStatus("Connecting...");
            client = new TcpClient();
            client.BeginConnect("127.0.0.1", 8191, StartConnection, null);
            //'client.BeginConnect("127.0.0.1", 8191, AddressOf Connected, Nothing)
        }

        public bool isConnected
        {
            get
            {
                if (client == null) return false;
                return client.Connected;
            }
        }

        private void StartConnection(IAsyncResult ar)
        {
            try
            {
                client.EndConnect(ar);
                client.GetStream().BeginRead(readBuffer, 0, readBuffer.Length, ReadStream, null);
                ConnectionStatus("Connected");
            }
            catch (Exception ex)
            {
                ConnectionStatus("Unable to connect");
            }
        }

        private void ReadStream(IAsyncResult ar)
        {
            int numBytes;
            try
            {
                //SyncLock 
                client.GetStream();
                numBytes = client.GetStream.EndRead(ar);
                // End SyncLock
            }
            catch (Exception ex)
            {
                // 'RaiseEvent Disconnected(Me)
                return;
            }

            if (numBytes > 0)
            {
                var uEncode = new UTF8Encoding();
                var data = uEncode.GetString(readBuffer, 0, numBytes);
                packetBuffer.Append(data);
            }
            else
            {
                // 'RaiseEvent Disconnected(Me)
                return;
            }

            while (packetBuffer.ToString().Contains("-"))
            {
                var endOfPacket = packetBuffer.ToString().IndexOf("-");
                var packetData = packetBuffer.ToString().Substring(0, endOfPacket);
                packetBuffer.Remove(0, endOfPacket + 1);
                ParsePacket(packetData);
            }

            try
            {
                //SyncLock client.GetStream()
                client.GetStream().BeginRead(readBuffer, 0, readBuffer.Length, ReadStream, null);
                //End SyncLock
            }
            catch (Exception ex)
            {
                //'RaiseEvent Disconnected(Me)
            }
        }

        public void Send(byte packetID, string packetData)
        {
            //SyncLock client.GetStream
            var dataArray = System.Text.Encoding.UTF8.GetBytes(packetData);
            var byteArray = new byte[dataArray.Length];
            byteArray[0] = packetID;
            dataArray.CopyTo(byteArray, 1);
            var loadedPacket = Convert.ToBase64String(byteArray);

            var writer = new StreamWriter(client.GetStream());
            writer.Write(loadedPacket + "-");
            writer.Flush();
            // End SyncLock
        }

        public void CloseConnection()
        {
            client.Close();
        }

        public void ParsePacket(string packet)
        {
            byte[] dataBytes = Convert.FromBase64String(packet);
            NetworkPacket newPacket;
            newPacket.PacketID = dataBytes[0];

            if (newPacket.PacketID == PacketID.MapData)
            {
                Dim dArray(dataBytes.Length - 2) As Byte
            Array.ConstrainedCopy(dataBytes, 1, dArray, 0, dataBytes.Length - 1)

            Dim pArray() As Byte = Globals.Decompress(dArray)
            newPacket.PacketData = System.Text.Encoding.UTF8.GetString(pArray, 0, pArray.Length - 1)
            }
            else
            {
                newPacket.PacketData = System.Text.Encoding.UTF8.GetString(dataBytes, 1, dataBytes.Length - 1)
            }
            LineReceived(newPacket.PacketID, newPacket.PacketData);
        }
    }


    public enum PacketID : byte
    {
        Disconnect = 0,
        Connect = 1,
        ConnectedUsers = 2,
        Broadcast = 3,
        Location = 50,
        MapData = 100,
        TileData = 101,
        Ping = 255,
    }

    public struct NetworkPacket
    {
        public PacketID PacketID;
        public string PacketData;
    }
}