﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
namespace GameEngine
{
    public class Input
    {
        private static KeyboardState currentKeyState;
        private static KeyboardState lastKeyState;
        private static MouseState currentMouseState;
        private static MouseState lastMouseState;
        public static Point MousePosition;

        public static void Update()
        {
            lastKeyState = currentKeyState;
            currentKeyState = Keyboard.GetState();
            lastMouseState = currentMouseState;
            currentMouseState = Mouse.GetState();
            MousePosition.X = currentMouseState.X;
            MousePosition.Y = currentMouseState.Y;
            // 'MousePosition.X = Math.Floor(Globals.GameSize.X / Globals.Graphics.GraphicsDevice.Viewport.Width * currentMouseState.X)
            // 'MousePosition.Y = Math.Floor(Globals.GameSize.Y / Globals.Graphics.GraphicsDevice.Viewport.Height * currentMouseState.Y)
        }

        public static bool KeyDown(Keys key)
        {
            return currentKeyState.IsKeyDown(key);
        }

        public static bool KeyPressed(Keys key)
        {
            if (currentKeyState.IsKeyDown(key) && lastKeyState.IsKeyUp(key))
            {
                return true; ;
            }
            return false; ;
        }

        public static bool LeftDown()
        {
            if (currentMouseState.LeftButton == ButtonState.Pressed)
            {
                return true; ;
            }
            return false; ;
        }

        public static bool RightDown()
        {
            if (currentMouseState.RightButton == ButtonState.Pressed)
            {
                return true; ;
            }
            return false;
        }

        public static bool MiddleDown()
        {
            if (currentMouseState.MiddleButton == ButtonState.Pressed)
            {
                return true;
            }
            return false;
        }

        public static bool LeftPressed()
        {
            if (currentMouseState.LeftButton == ButtonState.Pressed && lastMouseState.LeftButton == ButtonState.Released)
            {
                return true;
            }
            return false;
        }

        public static bool RightPressed()
        {
            if (currentMouseState.RightButton == ButtonState.Pressed && lastMouseState.RightButton == ButtonState.Released)
            {
                return true;
            }
            return false;
        }

        public static bool MiddlePressed()
        {
            if (currentMouseState.MiddleButton == ButtonState.Pressed && lastMouseState.MiddleButton == ButtonState.Released)
            {
                return true;
            }

            return false;
        }

        public static bool ScrollUp()
        {
            if (currentMouseState.ScrollWheelValue > lastMouseState.ScrollWheelValue)
                return true;

            return false;
        }

        public static bool ScrollDown()
        {
            if (currentMouseState.ScrollWheelValue < lastMouseState.ScrollWheelValue)
                return true;

            return false;
        }
    }
}