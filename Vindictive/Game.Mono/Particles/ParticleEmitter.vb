﻿public Class ParticleEmitter
    public Particles As New List(Of Particle)
    public Texture As Texture2D = Textures.Particle
    public StartColor As New Color(0, 0, 0)
    public EndColor As New Color(0, 0, 0)
    public Emission As Integer
    public MaxEmission As Integer
    public MinStartLocation As Vector2
    public MaxStartLocation As Vector2
    public MinSize As Vector2
    public MaxSize As Vector2
    public MinEnergy As Single
    public MaxEnergy As Single
    public MinDirection As Single
    public MaxDirection As Single
    public MinVelocity As Single
    public MaxVelocity As Single
    public MinAngularVelocity As Single
    public MaxAngularVelocity As Single
    public MinRotationVelocity As Single
    public MaxRotationVelocity As Single
    public Rotation As Single
    private ran As New Random()

    public Sub Update()
        Dim removeParticles As New List(Of Particle)
        For Each foundParticle As Particle In Particles
            If foundParticle.Alive = False Then
                removeParticles.Add(foundParticle)
            Else
                foundParticle.Update()
            End If
        Next
        For Each foundParticle As Particle In removeParticles
            Particles.Remove(foundParticle)
        Next
        If Particles.Count < MaxEmission Then
            For i = 0 To Emission
                Dim newParticle As New Particle
                With newParticle
                    .Texture = Texture
                    .StartColor = StartColor
                    .EndColor = EndColor
                    .Location = New Vector2(ran.Next(MinStartLocation.X, MaxStartLocation.X), ran.Next(MinStartLocation.Y, MaxStartLocation.Y))
                    .Size = New Vector2(ran.Next(MinSize.X, MaxSize.X), ran.Next(MinSize.Y, MaxSize.Y))
                    .TotalEnergy = (MaxEnergy - MinEnergy) * ran.NextDouble() + MinEnergy
                    .Direction = (MaxDirection - MinDirection) * ran.NextDouble() + MinDirection
                    .Velocity = (MaxVelocity - MinVelocity) * ran.NextDouble() + MinVelocity
                    .AngularVelocity = (MaxAngularVelocity - MinAngularVelocity) * ran.NextDouble() + MinAngularVelocity
                    .RotationVelocity = (MaxRotationVelocity - MinRotationVelocity) * ran.NextDouble() + MinRotationVelocity
                    .Rotation = Rotation
                End With
                Particles.Add(newParticle)
                If Particles.Count >= MaxEmission Then
                    Exit For
                End If
            Next
        End If
    End Sub

    public Sub Draw()
        For i = 0 To Particles.Count - 1
            Particles(i).Draw()
        Next
    End Sub
End Class