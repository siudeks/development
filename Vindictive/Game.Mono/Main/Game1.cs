using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace GameEngine
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private ScreenManager screenManager;

        public Game1 ()
	    {
            Globals.Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
	    }

        protected override void Initialize()
        {
            this.IsMouseVisible = true;
            Window.AllowUserResizing = true;
            Globals.GameSize = new Vector2(800, 600);
            Globals.Graphics.PreferredBackBufferWidth = Globals.GameSize.X;
            Globals.Graphics.PreferredBackBufferHeight = Globals.GameSize.Y;
            Globals.Graphics.ApplyChanges();
            // Globals.BackBuffer = New RenderTarget2D(Globals.Graphics.GraphicsDevice, Globals.GameSize.X, Globals.GameSize.Y, False, SurfaceFormat.Color,
            //                                         DepthFormat.None, 0, RenderTargetUsage.PreserveContents)
            Globals.UserConnection = new UserConnection();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            Globals.SpriteBatch = new SpriteBatch(GraphicsDevice);
            Globals.Content = this.Content;
            Textures.Load();
            Fonts.Load();
            screenManager = new ScreenManager();
            // 'screenManager.AddScreen(New ParticleMaker)
            // 'screenManager.AddScreen(New MainMenu)
            // 'screenManager.AddScreen(New World)
            screenManager.AddScreen(new clsMainMenu());
            // 'screenManager.AddScreen(New World)
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            Globals.WindowFocused = this.IsActive;
            Globals.GameTime = gameTime;
            Input.Update();
            screenManager.Update();
        }

        protected override void Draw(GameTime gameTime)
        {
            // '  Globals.Graphics.GraphicsDevice.SetRenderTarget(Globals.BackBuffer)
            GraphicsDevice.Clear(Color.Black);
            base.Draw(gameTime);
            screenManager.Draw();
            // 'Globals.Graphics.GraphicsDevice.SetRenderTarget(Nothing)
            // 'Globals.SpriteBatch.Begin()
            // 'Globals.SpriteBatch.Draw(Globals.BackBuffer, New Rectangle(0, 0, Globals.Graphics.GraphicsDevice.Viewport.Width,
            // '                                                           Globals.Graphics.GraphicsDevice.Viewport.Height), Color.White)
            // 'Globals.SpriteBatch.End()
        }
    }
}