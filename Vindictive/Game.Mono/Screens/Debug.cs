﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
namespace GameEngine
{
    public class DebugInfo : BaseScreen
    {
        public string Screens = string.Empty;
        public string FocusScreen = string.Empty;
        private int fps;
        private int fpsCounter;
        private double fpsTimer;
        private string fpsText = "\"\"";
        private Rectangle bgSize;

        public DebugInfo ()
        {
        Name = "Debug";
        State = ScreenState.Hidden;
        GrabFocus = false;
        }

        public override void HandleInput()
        {   
            base.HandleInput();
            if (Input.KeyPressed(Keys.F3))
            {
                if (State == ScreenState.Active)
                {
                    State = ScreenState.Hidden;
                }
                else if (State == ScreenState.Hidden)
                {
                    State = ScreenState.Active;
                }
                
            }
        }
    }

    public Overrides Sub Update()
        MyBase.Update()
        'If Screens.Length > 0 Then
        '    Screens = Screens.Substring(0, Screens.Length - 2)
        'End If
        Dim txtWidth As Integer = 0
        Dim txtHeight As Integer = 0
        txtWidth = Fonts.Verdana_8.MeasureString(fpsText).X
        If Fonts.Verdana_8.MeasureString(Screens).X > txtWidth Then
            txtWidth = Fonts.Verdana_8.MeasureString(Screens).X
        End If
        If Fonts.Verdana_8.MeasureString(FocusScreen).X > txtWidth Then
            txtWidth = Fonts.Verdana_8.MeasureString(FocusScreen).X
        End If
        txtHeight = Fonts.Verdana_8.MeasureString(fpsText).Y * 3
        bgSize = New Rectangle(0, 0, txtWidth + 20, txtHeight + 20)
    End Sub

    public Overrides Sub Draw()
        MyBase.Draw()
        If Globals.GameTime.TotalGameTime.TotalMilliseconds > fpsTimer Then
            fps = fpsCounter
            fpsTimer = Globals.GameTime.TotalGameTime.TotalMilliseconds + 1000
            fpsCounter = 1
            fpsText = "FPS: " & fps
        Else
            fpsCounter += 1
        End If
        Globals.SpriteBatch.Begin()
        Globals.SpriteBatch.Draw(Textures.Dot, bgSize, Color.Black * 0.6F)
        Globals.SpriteBatch.DrawString(Fonts.Verdana_8, fpsText, New Vector2(10, 10), Color.White)
        Globals.SpriteBatch.DrawString(Fonts.Verdana_8, Screens, New Vector2(10, 22), Color.White)
        Globals.SpriteBatch.DrawString(Fonts.Verdana_8, FocusScreen, New Vector2(10, 34), Color.White)
        Globals.SpriteBatch.End()
    End Sub
    }
}