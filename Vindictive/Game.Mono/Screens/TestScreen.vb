﻿public Class TestScreen
    Inherits BaseScreen

    private floatingText As String = "Hello World"
    private textLocation As New Vector2(50, 50)
    private textDirection As Single
    private textBounds As Rectangle
    private ran As New Random

    public Sub New()
        Name = "TestScreen"
        textDirection = ran.[Next](360)
    End Sub

    public Overrides Sub HandleInput()
        MyBase.HandleInput()
    End Sub

    public Overrides Sub Update()
        MyBase.Update()
        textBounds = New Rectangle(textLocation.X, textLocation.Y, Fonts.Georgia_16.MeasureString(floatingText).X,
                                   Fonts.Georgia_16.MeasureString(floatingText).Y)
        If Not New Rectangle(0, 0, Globals.GameSize.X, Globals.GameSize.Y).Contains(textBounds) Then
            textDirection += 45
            If textDirection > 360 Then
                textDirection -= 360
            End If
        End If
        textLocation.X += Math.Cos(MathHelper.ToRadians(textDirection)) * 2
        textLocation.Y += Math.Sin(MathHelper.ToRadians(textDirection)) * 2
    End Sub

    public Overrides Sub Draw()
        MyBase.Draw()
        Globals.SpriteBatch.Begin()
        Globals.SpriteBatch.DrawString(Fonts.Georgia_16, floatingText, textLocation, Color.White)
        Globals.SpriteBatch.End()
    End Sub
End Class

