﻿public Class clsChat
    Inherits BaseScreen

    private t2dMenu As Texture2D
    private t2dVolume As Texture2D
    private t2dVolumeDisabled As Texture2D
    private myFont As SpriteFont = Fonts.Gisha_16
    private chatFont As SpriteFont
    private lstClients As New List(Of String)
    private chatHistory As New List(Of String)

    private ChatLine As String = ""
    private oldKeyState As KeyboardState
    private keyState As KeyboardState
    private ChatCursorTime As Integer = 0
    private ChatCursorAlpha As Double = 0
    private ChatCursorFade As Boolean = False
    private PingTime As Integer = 0
    private PingText As Integer = 0
    private PingWait As Integer = -5000
    private Pong As Boolean = True

    private keyPressTime As Integer = 0
    private keyRepeatTime As Integer = 0

    private Mute As Boolean = False

    private oldMouseState As MouseState
    private currentMouseState As MouseState

    private soundOne As SoundEffect
    private soundOneInstance As SoundEffectInstance

    public Sub New()
        State = ScreenState.FadeIn
        Name = "Chat"

        chatFont = Globals.Content.Load(Of SpriteFont)("Fonts\ChatFont")
        t2dMenu = Globals.Content.Load(Of Texture2D)("System\Menu")
        t2dVolume = Globals.Content.Load(Of Texture2D)("VolumeNormal")
        t2dVolumeDisabled = Globals.Content.Load(Of Texture2D)("VolumeDisabled")

        soundOne = Globals.Content.Load(Of SoundEffect)("beep")
        soundOneInstance = soundOne.CreateInstance

        AddHandler Globals.UserConnection.PacketHandler.ConnectedUsers, AddressOf UserPacket
        AddHandler Globals.UserConnection.PacketHandler.Broadcast, AddressOf BroadcastPacket
        AddHandler Globals.UserConnection.PacketHandler.Ping, AddressOf PingPacket
        Globals.UserConnection.Send(PacketID.Connect, Globals.UserConnection.UserName)
    End Sub

    public Sub UserPacket(data As String)
        lstClients.Clear()
        For Each clientName As String In data.Split("|")
            If clientName.Length > 0 Then
                lstClients.Add(clientName)
            End If
        Next
    End Sub

    public Sub BroadcastPacket(data As String)
        Dim splitData() As String = data.Split("|")
        If splitData(0) <> Globals.UserConnection.UserName And Mute = False Then soundOneInstance.Play()
        chatHistory.Add(splitData(0) & ": " & splitData(1))
    End Sub

    public Sub PingPacket(data As String)
        PingText = Globals.GameTime.TotalGameTime.TotalMilliseconds - PingTime
        PingWait = Globals.GameTime.TotalGameTime.TotalMilliseconds
        Pong = True
    End Sub

    public Overrides Sub HandleInput()
        oldMouseState = currentMouseState
        currentMouseState = Mouse.GetState
        oldKeyState = keyState
        keyState = Keyboard.GetState
        If keyState.GetPressedKeys.Count > 0 And oldKeyState = keyState Then
            keyPressTime += Globals.GameTime.ElapsedGameTime.TotalMilliseconds
            keyRepeatTime += Globals.GameTime.ElapsedGameTime.TotalMilliseconds
        Else
            keyPressTime = 0
        End If
        If keyPressTime > 550 And keyRepeatTime > 40 Then
            keyRepeatTime = 0
            oldKeyState = Nothing
        End If
        Dim isShiftDown As Boolean = False
        If keyState.IsKeyDown(Keys.LeftShift) Or keyState.IsKeyDown(Keys.RightShift) Then isShiftDown = True
        For Each foundKey As Keys In keyState.GetPressedKeys
            If oldKeyState.IsKeyUp(foundKey) Then
                If foundKey = Keys.LeftShift Or foundKey = Keys.RightShift Or foundKey = Keys.LeftAlt Or foundKey = Keys.RightAlt Or foundKey = Keys.LeftControl Or foundKey = Keys.RightControl Or foundKey = Keys.Tab Then
                    Continue For
                ElseIf foundKey = Keys.Back Then
                    If ChatLine.Length > 0 Then ChatLine = ChatLine.Remove(ChatLine.Length - 1, 1)
                ElseIf foundKey = Keys.Enter Then
                    If ChatLine.Length > 0 Then
                        If Globals.UserConnection.isConnected Then Globals.UserConnection.Send(PacketID.Broadcast, Globals.UserConnection.UserName & "|" & ChatLine)
                        ChatLine = ""
                    End If
                Else
                    If ChatLine.Length < 200 Then
                        ChatLine += ConvertKeyToChar(foundKey, isShiftDown)
                    End If
                    Exit For
                End If
            End If
        Next

        Dim screenHeight As Integer = Globals.Graphics.GraphicsDevice.Viewport.Height
        If Mouse.GetState.LeftButton = ButtonState.Pressed And oldMouseState.LeftButton = ButtonState.Released And Mouse.GetState.X > 288 And Mouse.GetState.X < 303 And Mouse.GetState.Y > screenHeight - 182 And Mouse.GetState.Y < screenHeight - 168 Then
            If Mute Then
                Mute = False
            Else
                Mute = True
            End If
        End If
    End Sub

    public Overrides Sub Update()
        MyBase.Update()
        If Globals.UserConnection.isConnected And Globals.GameTime.TotalGameTime.TotalMilliseconds - PingWait > 5000 And Pong = True Then
            Pong = False
            PingTime = Globals.GameTime.TotalGameTime.TotalMilliseconds
            PingWait = Globals.GameTime.TotalGameTime.TotalMilliseconds
            Globals.UserConnection.Send(PacketID.Ping, "")
        End If

        If Globals.UserConnection.isConnected = False And State = ScreenState.Active Then
            ScreenManager.UnloadScreen("World")
            ScreenManager.UnloadScreen("Chat")
            ScreenManager.AddScreen(New clsMainMenu)
        End If
    End Sub

    public Overrides Sub Draw()
        MyBase.Draw()
        Globals.SpriteBatch.Begin()

        Dim sRect As Rectangle
        Dim dRect As Rectangle
        Dim dColor As Color

        Dim screenHeight As Integer = Globals.Graphics.GraphicsDevice.Viewport.Height

        ' Draw Transparent background for menu leaving 3 pixels less on all sides for the slighty rounded corners.
        sRect = New Rectangle(0, 0, 0, 0)
        dColor = New Color(76, 76, 96)
        ' dColor.A = 200
        dRect = New Rectangle(10, screenHeight - 185, 300, 155)
        Globals.SpriteBatch.Draw(t2dMenu, dRect, sRect, dColor * Alpha)

        dRect = New Rectangle(10, screenHeight - 25, 300, 20)
        Globals.SpriteBatch.Draw(t2dMenu, dRect, sRect, dColor * Alpha)

        dRect = New Rectangle(315, screenHeight - 185, 90, 180)
        Globals.SpriteBatch.Draw(t2dMenu, dRect, sRect, dColor * Alpha)

        Dim chatLineDisplay As String = ""
        If chatFont.MeasureString(ChatLine).X > 275 Then
            For xInt = ChatLine.Length - 1 To 0 Step -1
                If chatFont.MeasureString(chatLineDisplay).X > 280 Then Exit For
                chatLineDisplay = ChatLine.Substring(xInt, ChatLine.Length - xInt)
            Next
        Else
            chatLineDisplay = ChatLine
        End If
        Globals.SpriteBatch.DrawString(chatFont, chatLineDisplay, New Vector2(15, screenHeight - 23), Color.White * Alpha)

        If Globals.GameTime.TotalGameTime.TotalMilliseconds - ChatCursorTime > 20 Then
            If ChatCursorFade Then
                ChatCursorAlpha -= 0.1
            Else
                ChatCursorAlpha += 0.1
            End If
            If ChatCursorAlpha > 1 Then
                ChatCursorFade = True
                ChatCursorAlpha = 1
            ElseIf ChatCursorAlpha < 0 Then
                ChatCursorFade = False
                ChatCursorAlpha = 0
            End If
            ChatCursorTime = Globals.GameTime.TotalGameTime.TotalMilliseconds
        End If

        Globals.SpriteBatch.DrawString(chatFont, "_", New Vector2(15 + chatFont.MeasureString(chatLineDisplay).X, screenHeight - 23), Color.White * ChatCursorAlpha * Alpha)
        Dim FontY As Integer = screenHeight - 183
        If chatHistory.Count > 0 Then
            For xInt = getHistory.Count - 1 To 0 Step -1
                If getHistory(xInt).Substring(0, 1) <> " " Then
                    Globals.SpriteBatch.DrawString(chatFont, getHistory(xInt), New Vector2(15, FontY), Color.White * Alpha)
                    Dim uName As String = getHistory(xInt).Substring(0, InStr(getHistory(xInt), " ", CompareMethod.Text))
                    Dim uColor As Color = Color.Lime
                    If uName.Trim = "Server:" Then
                        uColor = Color.Yellow
                    End If
                    Globals.SpriteBatch.DrawString(chatFont, uName, New Vector2(15, FontY), uColor * Alpha)
                Else
                    Globals.SpriteBatch.DrawString(chatFont, getHistory(xInt), New Vector2(15, FontY), Color.White * Alpha)
                End If
                ' SBatch.DrawString(chatFont, getHistory(xInt), New Vector2(15, FontY), Color.White * Alpha)
                FontY += chatFont.MeasureString(getHistory(xInt)).Y
            Next
        End If
        Do While chatHistory.Count > 100
            chatHistory.RemoveAt(0)
        Loop

        FontY = screenHeight - 178
        For xInt = 0 To lstClients.Count - 1
            Globals.SpriteBatch.DrawString(chatFont, lstClients(xInt), New Vector2(320, FontY), Color.Cyan * Alpha)
            FontY += chatFont.MeasureString("String Size").Y
        Next

        Globals.SpriteBatch.DrawString(chatFont, "Ping:" & PingText, New Vector2(20, 20), Color.Cyan * Alpha)

        If Mute Then
            sRect = New Rectangle(0, 0, 24, 24)
            dRect = New Rectangle(286, screenHeight - 185, 20, 20)
            Globals.SpriteBatch.Draw(t2dVolume, dRect, sRect, New Color(60, 60, 60) * Alpha)
        Else
            sRect = New Rectangle(0, 0, 24, 24)
            dRect = New Rectangle(286, screenHeight - 185, 20, 20)
            Globals.SpriteBatch.Draw(t2dVolume, dRect, sRect, Color.White * Alpha)
        End If
        Globals.SpriteBatch.End()
    End Sub

    private Function getHistory() As List(Of String)
        Dim returnList As New List(Of String)
        Dim numLines As Integer = 0
        For xInt = chatHistory.Count - 1 To 0 Step -1
            Dim parse As String = parseText(chatHistory(xInt))
            numLines += parse.Split(ControlChars.Lf).Count
            Dim tmpList As New List(Of String)
            For zInt = 0 To parse.Split(ControlChars.Lf).Count - 1
                Dim sParse() As String = parse.Split(ControlChars.Lf)
                tmpList.Add(sParse(zInt))
            Next
            For zInt = tmpList.Count - 1 To 0 Step -1
                returnList.Add(tmpList(zInt))
            Next
            If numLines > 9 Then
                For yInt = 1 To numLines - 9
                    returnList.RemoveAt(returnList.Count - 1)
                Next
                Return returnList
            End If
        Next
        Return returnList
    End Function

    private Function parseText(text As String) As String
        If text Is Nothing Then Return ""
        Dim line As String = ""
        Dim returnString As String = ""
        Dim wordSplit As String() = text.Split(" ")
        For Each word As String In wordSplit
            If chatFont.MeasureString(word).X > 280 Then
                For xInt = 0 To word.Length - 1
                    If chatFont.MeasureString(line).X > 280 Then
                        returnString = returnString + line + ControlChars.Lf + "  "
                        line = "  "
                    End If
                    line += word.Substring(xInt, 1)
                Next
                line += " "
                Continue For
            End If
            If chatFont.MeasureString(line + word).X > 280 Then
                returnString = returnString + line + ControlChars.Lf + "  "
                line = "  "
            End If
            line = line + word + " "
        Next
        Return returnString + line
    End Function

    public static Function ConvertKeyToChar(key As Keys, shift As Boolean) As String
        Select Case key
            Case Keys.Space
                Return " "
            Case Keys.Enter

            Case Keys.Tab

                ' D-Numerics (strip above the alphabet) 
            Case Keys.D0
                Return If(shift, ")", "0")
            Case Keys.D1
                Return If(shift, "!", "1")
            Case Keys.D2
                Return If(shift, "@", "2")
            Case Keys.D3
                Return If(shift, "#", "3")
            Case Keys.D4
                Return If(shift, "$", "4")
            Case Keys.D5
                Return If(shift, "%", "5")
            Case Keys.D6
                Return If(shift, "^", "6")
            Case Keys.D7
                Return If(shift, "&", "7")
            Case Keys.D8
                Return If(shift, "*", "8")
            Case Keys.D9
                Return If(shift, "(", "9")

                ' Numpad 
            Case Keys.NumPad0
                Return "0"
            Case Keys.NumPad1
                Return "1"
            Case Keys.NumPad2
                Return "2"
            Case Keys.NumPad3
                Return "3"
            Case Keys.NumPad4
                Return "4"
            Case Keys.NumPad5
                Return "5"
            Case Keys.NumPad6
                Return "6"
            Case Keys.NumPad7
                Return "7"
            Case Keys.NumPad8
                Return "8"
            Case Keys.NumPad9
                Return "9"
            Case Keys.Add
                Return "+"
            Case Keys.Subtract
                Return "-"
            Case Keys.Multiply
                Return "*"
            Case Keys.Divide
                Return "/"
            Case Keys.[Decimal]
                Return "."

                ' Alphabet 
            Case Keys.A
                Return If(shift, "A", "a")
            Case Keys.B
                Return If(shift, "B", "b")
            Case Keys.C
                Return If(shift, "C", "c")
            Case Keys.D
                Return If(shift, "D", "d")
            Case Keys.E
                Return If(shift, "E", "e")
            Case Keys.F
                Return If(shift, "F", "f")
            Case Keys.G
                Return If(shift, "G", "g")
            Case Keys.H
                Return If(shift, "H", "h")
            Case Keys.I
                Return If(shift, "I", "i")
            Case Keys.J
                Return If(shift, "J", "j")
            Case Keys.K
                Return If(shift, "K", "k")
            Case Keys.L
                Return If(shift, "L", "l")
            Case Keys.M
                Return If(shift, "M", "m")
            Case Keys.N
                Return If(shift, "N", "n")
            Case Keys.O
                Return If(shift, "O", "o")
            Case Keys.P
                Return If(shift, "P", "p")
            Case Keys.Q
                Return If(shift, "Q", "q")
            Case Keys.R
                Return If(shift, "R", "r")
            Case Keys.S
                Return If(shift, "S", "s")
            Case Keys.T
                Return If(shift, "T", "t")
            Case Keys.U
                Return If(shift, "U", "u")
            Case Keys.V
                Return If(shift, "V", "v")
            Case Keys.W
                Return If(shift, "W", "w")
            Case Keys.X
                Return If(shift, "X", "x")
            Case Keys.Y
                Return If(shift, "Y", "y")
            Case Keys.Z
                Return If(shift, "Z", "z")

                ' Oem 
            Case Keys.OemOpenBrackets
                Return If(shift, "{", "[")
            Case Keys.OemCloseBrackets
                Return If(shift, "}", "]")
            Case Keys.OemComma
                Return If(shift, "<", ",")
            Case Keys.OemPeriod
                Return If(shift, ">", ".")
            Case Keys.OemMinus
                Return If(shift, "_", "-")
            Case Keys.OemPlus
                Return If(shift, "+", "=")
            Case Keys.OemQuestion
                Return If(shift, "?", "/")
            Case Keys.OemSemicolon
                Return If(shift, ":", ";")
            Case Keys.OemQuotes
                Return If(shift, """", "'")
            Case Keys.OemPipe
                Return If(shift, "", "\")
                'Return If(shift, "|", "\")
            Case Keys.OemTilde
                Return If(shift, "~", "`")
        End Select

        Return ""
    End Function

End Class
