﻿public Class ScreenTemplate ' Rename Class
    Inherits BaseScreen

    public Sub New()
        Name = "" ' Rename

        ' Change these defaults if needed
        '        TransitionTime = 750;
        '        GrabFocus = true;
        '        
    End Sub

    public Overrides Sub HandleInput()
        MyBase.HandleInput()
    End Sub

    public Overrides Sub Update()
        MyBase.Update()
    End Sub

    public Overrides Sub Draw()
        MyBase.Draw()
    End Sub
End Class
