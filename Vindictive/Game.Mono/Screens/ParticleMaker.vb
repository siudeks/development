﻿public Class ParticleMaker
    Inherits BaseScreen

    private trackbarList As New List(Of Trackbar)()
    private static scale As Double = 0.40625
    private static yPos As Integer = 13
    private startColorR As New Trackbar(10, 8, scale, "Start Color R", 0, 255, True)
    private startColorG As New Trackbar(10, yPos * 1 + 8, scale, "Start Color G", 0, 255, True)
    private startColorB As New Trackbar(10, yPos * 2 + 8, scale, "Start Color B", 0, 255, True)
    private endColorR As New Trackbar(10, yPos * 4 + 8, scale, "End Color R", 0, 255, True)
    private endColorG As New Trackbar(10, yPos * 5 + 8, scale, "End Color G", 0, 255, True)
    private endColorB As New Trackbar(10, yPos * 6 + 8, scale, "End Color B", 0, 255, True)
    private emission As New Trackbar(10, yPos * 8 + 8, scale, "Emission", 1, 150, True)
    private maxEmission As New Trackbar(10, yPos * 9 + 8, scale, "Max Particles", 1, 15000, True)
    private minStartX As New Trackbar(10, yPos * 11 + 8, scale, "Min Loc X", 200, Globals.GameSize.X, True)
    private maxStartX As New Trackbar(10, yPos * 12 + 8, scale, "Max Loc X", 200, Globals.GameSize.X, True)
    private minStartY As New Trackbar(10, yPos * 13 + 8, scale, "Min Loc Y", 0, Globals.GameSize.Y, True)
    private maxStartY As New Trackbar(10, yPos * 14 + 8, scale, "Max Loc Y", 0, Globals.GameSize.Y, True)
    private minSizeX As New Trackbar(10, yPos * 16 + 8, scale, "Min Size X", 1, 50, True)
    private maxSizeX As New Trackbar(10, yPos * 17 + 8, scale, "Max Size X", 1, 50, True)
    private minSizeY As New Trackbar(10, yPos * 18 + 8, scale, "Min Size Y", 1, 50, True)
    private maxSizeY As New Trackbar(10, yPos * 19 + 8, scale, "Max Size Y", 1, 50, True)
    private minEnergy As New Trackbar(10, yPos * 21 + 8, scale, "Min Energy", 1, 200, True)
    private maxEnergy As New Trackbar(10, yPos * 22 + 8, scale, "Max Energy", 1, 200, True)
    private minDirection As New Trackbar(10, yPos * 24 + 8, scale, "Min Direction", 0, 360, True)
    private maxDirection As New Trackbar(10, yPos * 25 + 8, scale, "Max Direction", 0, 360, True)
    private minVelocity As New Trackbar(10, yPos * 27 + 8, scale, "Min Velocity", -10, 10)
    private maxVelocity As New Trackbar(10, yPos * 28 + 8, scale, "Max Velocity", -10, 10)
    private minAngularVelocity As New Trackbar(10, yPos * 29 + 8, scale, "Min Angular V", -5, 5)
    private maxAngularVelocity As New Trackbar(10, yPos * 30 + 8, scale, "Max Angular V", -5, 5)
    private minRotationVelocity As New Trackbar(10, yPos * 31 + 8, scale, "Min Rotation V", -10, 10)
    private maxRotationVelocity As New Trackbar(10, yPos * 32 + 8, scale, "Max Rotation V", -10, 10)
    private rotation As New Trackbar(10, yPos * 34 + 8, scale, "Rotation", 0, 360, True)
    private emitter As New ParticleEmitter()

    public Sub New()
        Name = "Particle Maker"
        trackbarList.Add(startColorR)
        trackbarList.Add(startColorG)
        trackbarList.Add(startColorB)
        trackbarList.Add(endColorR)
        trackbarList.Add(endColorG)
        trackbarList.Add(endColorB)
        trackbarList.Add(emission)
        trackbarList.Add(maxEmission)
        trackbarList.Add(minStartX)
        trackbarList.Add(maxStartX)
        trackbarList.Add(minStartY)
        trackbarList.Add(maxStartY)
        trackbarList.Add(minSizeX)
        trackbarList.Add(maxSizeX)
        trackbarList.Add(minSizeY)
        trackbarList.Add(maxSizeY)
        trackbarList.Add(minEnergy)
        trackbarList.Add(maxEnergy)
        trackbarList.Add(minDirection)
        trackbarList.Add(maxDirection)
        trackbarList.Add(minVelocity)
        trackbarList.Add(maxVelocity)
        trackbarList.Add(minAngularVelocity)
        trackbarList.Add(maxAngularVelocity)
        trackbarList.Add(minRotationVelocity)
        trackbarList.Add(maxRotationVelocity)
        trackbarList.Add(rotation)
        emitter = New ParticleEmitter()
        minVelocity.Value = 0
        maxVelocity.Value = 0
        minAngularVelocity.Value = 0
        maxAngularVelocity.Value = 0
        minRotationVelocity.Value = 0
        maxRotationVelocity.Value = 0
    End Sub

    public Overrides Sub HandleInput()
        MyBase.HandleInput()
    End Sub

    public Overrides Sub Update()
        MyBase.Update()
        For Each trackbar As Trackbar In trackbarList
            trackbar.Update()
        Next
        If minStartX.Value > maxStartX.Value Then maxStartX.Value = minStartX.Value
        If minStartY.Value > maxStartY.Value Then maxStartY.Value = minStartY.Value
        If minSizeX.Value > maxSizeX.Value Then maxSizeX.Value = minSizeX.Value
        If minSizeY.Value > maxSizeY.Value Then maxSizeY.Value = minSizeY.Value
        If minEnergy.Value > maxEnergy.Value Then maxEnergy.Value = minEnergy.Value
        If minDirection.Value > maxDirection.Value Then maxDirection.Value = minDirection.Value
        If minVelocity.Value > maxVelocity.Value Then maxVelocity.Value = minVelocity.Value
        If minAngularVelocity.Value > maxAngularVelocity.Value Then maxAngularVelocity.Value = minAngularVelocity.Value
        If minRotationVelocity.Value > maxRotationVelocity.Value Then maxRotationVelocity.Value = minRotationVelocity.Value
        emitter.StartColor = New Color(CByte(startColorR.Value), CByte(startColorG.Value), CByte(startColorB.Value))
        emitter.EndColor = New Color(CByte(endColorR.Value), CByte(endColorG.Value), CByte(endColorB.Value))
        emitter.Emission = CInt(emission.Value)
        emitter.MaxEmission = CInt(maxEmission.Value)
        emitter.MinStartLocation = New Vector2(minStartX.Value, minStartY.Value)
        emitter.MaxStartLocation = New Vector2(maxStartX.Value, maxStartY.Value)
        emitter.MinSize = New Vector2(minSizeX.Value, minSizeY.Value)
        emitter.MaxSize = New Vector2(maxSizeX.Value, maxSizeY.Value)
        emitter.MinEnergy = minEnergy.Value
        emitter.MaxEnergy = maxEnergy.Value
        emitter.MinDirection = minDirection.Value
        emitter.MaxDirection = maxDirection.Value
        emitter.MinVelocity = minVelocity.Value
        emitter.MaxVelocity = maxVelocity.Value
        emitter.MinAngularVelocity = minAngularVelocity.Value
        emitter.MaxAngularVelocity = maxAngularVelocity.Value
        emitter.MinRotationVelocity = minRotationVelocity.Value
        emitter.MaxRotationVelocity = maxRotationVelocity.Value
        emitter.Rotation = rotation.Value
        emitter.Update()
    End Sub

    public Overrides Sub Draw()
        MyBase.Draw()
        Globals.SpriteBatch.Begin()
        Globals.SpriteBatch.Draw(Textures.BG, New Rectangle(100, 0, Globals.GameSize.X, Globals.GameSize.Y), Color.White)
        emitter.Draw()
        Globals.SpriteBatch.Draw(Textures.Dot, New Rectangle(0, 0, 200, Globals.GameSize.Y), Color.SlateGray)
        For Each trackbar As Trackbar In trackbarList
            trackbar.Draw()
        Next
        Globals.SpriteBatch.End()
    End Sub
End Class