﻿using System.Collections.Generic;
namespace GameEngine
{
    public enum ScreenState
    {
        FadeIn,
        FadeOut,
        Active,
        Shutdown,
        Hidden
    }

    public class ScreenManager
    {
        private static List<BaseScreen> screens = new List<BaseScreen>();
        private static List<BaseScreen> newScreens = new List<BaseScreen>();
        private DebugInfo debugScreen = new DebugInfo();

        public ScreenManager()
        {
            AddScreen(debugScreen);
        }

        public void Update()
        {
            var addRemoveScreens = true;
            foreach (var screen in screens)
            {
                if (screen.State == ScreenState.FadeOut)
                {
                    addRemoveScreens = false;
                    break;
                }
            }

            if (addRemoveScreens)
            {
                debugScreen.Screens = "Screens: ";
                var removeScreens = new List<BaseScreen>();
                foreach (var screen in screens)
                {
                    if (screen.State == ScreenState.Shutdown)
                        removeScreens.Add(screen);
                    else
                    {
                        debugScreen.Screens += screen.Name + ", ";
                        screen.Focused = false;
                    };
                }

                foreach (var screen in removeScreens)
                    screens.Remove(screen);

                foreach (var screen in newScreens)
                    screens.Add(screen);

                newScreens.Clear();
                // Remove Debug Screen and place it back on top
                screens.Remove(debugScreen);
                screens.Add(debugScreen);
                debugScreen.FocusScreen = "Focused Screen: ";
                if (screens.Count > 0)
                {
                    for (int i = screens.Count; i == 0; i--)
                    {
                        if (screens[i].GrabFocus)
                        {
                            screens[i].Focused = true;
                            debugScreen.FocusScreen = "Focused Screen: " + screens[i].Name;
                            break;
                        }
                    }
                }
            }

            foreach (var screen in screens)
            {
                if (Globals.WindowFocused)
                {
                    screen.HandleInput();
                }
                screen.Update();
            }
        }

        public void Draw()
        {
            foreach (var screen in screens)
            {
                if (screen.State != ScreenState.Hidden && screen.State != ScreenState.Shutdown)
                    screen.Draw();
            }
        }

        public static void AddScreen(BaseScreen screen)
        {
            newScreens.Add(screen);
        }

        public static void UnloadScreen(string screenName)
        {
            foreach (var foundScreen in screens)
            {
                if (foundScreen.Name == screenName)
                {
                    foundScreen.Unload();
                    break;
                }
            }
        }
    }
}