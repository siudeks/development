﻿using Microsoft.Xna.Framework;
namespace GameEngine
{
    public abstract class BaseScreen
    {
        public string Name = "";
        public ScreenState State = ScreenState.FadeIn;
        public double FadeTime;
        public float Alpha;
        public float Position;
        public int TransitionTime = 750;
        public bool Focused = false;
        public bool GrabFocus = true;

        public virtual void HandleInput()
        {
        }

        public virtual void Update()
        {
            Transition();
        }

        public virtual void Draw()
        {
        }

        public virtual void Unload()
        {
            State = ScreenState.FadeOut;
        }

        protected void Transition()
        {
            if (State == ScreenState.FadeIn)
            {
                if (FadeTime < TransitionTime)
                {
                    FadeTime += Globals.GameTime.ElapsedGameTime.TotalMilliseconds;
                    Alpha = MathHelper.Clamp((float)FadeTime / TransitionTime, 0, 1);
                    Position = Alpha;
                }
                else
                {
                    Position = 1;
                    Alpha = 1;
                    FadeTime = 0;
                    State = ScreenState.Active;
                }
            }
            else if (State == ScreenState.FadeOut)
            {
                if (FadeTime < TransitionTime)
                {
                    FadeTime += Globals.GameTime.ElapsedGameTime.TotalMilliseconds;
                    Alpha = MathHelper.Clamp(1 - (float)FadeTime / TransitionTime, 0, 1);
                    Position = MathHelper.Clamp((float)FadeTime / TransitionTime + 1, 0, 2);
                }
                else
                {
                    Position = 2;
                    Alpha = 0;
                    FadeTime = 0;
                    State = ScreenState.Shutdown;
                }
            }
            else if (State == ScreenState.Shutdown)
            {
                return;
            }
        }
    }
}