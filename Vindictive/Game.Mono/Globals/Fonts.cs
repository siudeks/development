﻿namespace GameEngine
{
    using Microsoft.Xna.Framework.Graphics;

    public class Fonts
    {
        public static SpriteFont Georgia_16;
        public static SpriteFont Gisha_16;
        public static SpriteFont Verdana_8;
        public static SpriteFont Microsoft_Sans_Serif_8;

        public static void Load()
        {
            Georgia_16 = Globals.Content.Load<SpriteFont>(@"Fonts\Georgia_16");
            Gisha_16 = Globals.Content.Load<SpriteFont>(@"Fonts\Gisha_16");
            Verdana_8 = Globals.Content.Load<SpriteFont>(@"Fonts\Verdana_8");
            Microsoft_Sans_Serif_8 = Globals.Content.Load<SpriteFont>(@"Fonts\Microsoft_Sans_Serif_8");
        }
    }
}