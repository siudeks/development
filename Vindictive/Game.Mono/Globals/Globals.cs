﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
namespace GameEngine
{
    public static class Globals
    {
        public static ContentManager Content;
        public static GraphicsDeviceManager Graphics;
        public static SpriteBatch SpriteBatch;
        public static GameTime GameTime;
        public static bool WindowFocused;
        public static Vector2 GameSize;
        public static UserConnection UserConnection;
        // public static BackBuffer As RenderTarget2D

        public static byte[] Compress(byte[] input)
        {
            byte[] output;
            using (var ms = new MemoryStream())
            using (var gs = new GZipStream(ms, CompressionMode.Compress))
            {
                gs.Write(input, 0, input.Length);
                gs.Close();
                output = ms.ToArray();
            }
            return output;
        }

        public static byte[] Decompress(byte[] input)
        {
            var output = new List<byte>();
            using (var ms = new MemoryStream())
            using (var gs = new GZipStream(ms, CompressionMode.Decompress))
            {
                var readByte = gs.ReadByte();
                while (readByte != -1)
                {
                    output.Add((byte)readByte);
                    readByte = gs.ReadByte();
                }
            }
            return output.ToArray();
        }
    }
}
