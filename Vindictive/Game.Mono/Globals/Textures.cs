﻿using Microsoft.Xna.Framework.Graphics;
namespace GameEngine
{
    public class Textures
    {
        public static Texture2D Dot;
        public static Texture2D TrackBar;
        public static Texture2D BG;
        public static Texture2D Particle;

        public static Texture2D Menu;

        public static Texture2D Stone;
        public static Texture2D Stone2;
        public static Texture2D Grass;
        public static Texture2D Grass2;
        public static Texture2D Grass3;
        public static Texture2D Grass4;

        public static void Load()
        {
            Dot = Globals.Content.Load<Texture2D>(@"Misc\Dot");
            TrackBar = Globals.Content.Load<Texture2D>(@"UI\Trackbar");
            BG = Globals.Content.Load<Texture2D>(@"Misc\bg"); ;
            Particle = Globals.Content.Load<Texture2D>(@"Misc\Particle");
            Menu = Globals.Content.Load<Texture2D>(@"System\Menu");
            Stone = Globals.Content.Load<Texture2D>(@"Misc\Stone");
            Stone2 = Globals.Content.Load<Texture2D>(@"Misc\Stone2");
            Grass = Globals.Content.Load<Texture2D>(@"Misc\Grass");
            Grass2 = Globals.Content.Load<Texture2D>(@"Misc\Grass2");
            Grass3 = Globals.Content.Load<Texture2D>(@"Misc\Grass3");
            Grass4 = Globals.Content.Load<Texture2D>(@"Misc\Grass4");
        }
    }
}