﻿Imports System.Net.Sockets
Imports System.Text
Imports System.IO
Imports System.IO.Compression

Public Class UserConnection

    Public Event Connected(sender As UserConnection)
    Public Event Disconnected(sender As UserConnection)
    Public Event LineReceived(sender As UserConnection, packetID As Byte, packetData As String)

    Public ID As Guid = Guid.NewGuid
    Public IP As String
    Public UserName As String = ""

    Private client As TcpClient
    Private readBuffer(1024) As Byte
    Private packetBuffer As New StringBuilder


    Public Sub New(tcpClient As TcpClient)
        client = tcpClient
        IP = client.Client.RemoteEndPoint.ToString
        RaiseEvent Connected(Me)
        client.GetStream.BeginRead(readBuffer, 0, readBuffer.Length, AddressOf ReadStream, Nothing)
    End Sub

    Private Sub ReadStream(ar As IAsyncResult)
        Dim numBytes As Integer

        Try
            SyncLock client.GetStream
                numBytes = client.GetStream.EndRead(ar)
            End SyncLock
        Catch ex As Exception
            RaiseEvent Disconnected(Me)
            Return
        End Try

        If numBytes > 0 Then
            Dim uEncode As New UTF8Encoding
            Dim data As String = uEncode.GetString(readBuffer, 0, numBytes)
            packetBuffer.Append(data)
        Else
            RaiseEvent Disconnected(Me)
            Return
        End If

        Do While packetBuffer.ToString.Contains("-")
            Dim endOfPacket As Integer = packetBuffer.ToString.IndexOf("-")
            Dim packetData As String = packetBuffer.ToString.Substring(0, endOfPacket)
            packetBuffer.Remove(0, endOfPacket + 1)
            ParsePacket(packetData)
        Loop

        Try
            SyncLock client.GetStream
                client.GetStream.BeginRead(readBuffer, 0, readBuffer.Length, AddressOf ReadStream, Nothing)
            End SyncLock
        Catch e As Exception
            RaiseEvent Disconnected(Me)
        End Try
    End Sub

    Public Sub Send(id As Byte, data As String)
        If client.Connected Then
            SyncLock client.GetStream
                Dim dataArray() As Byte = System.Text.Encoding.UTF8.GetBytes(data)
                Dim byteArray(dataArray.Length) As Byte
                byteArray(0) = id
                dataArray.CopyTo(byteArray, 1)

                Dim loadedPacket As String
                If id = PacketID.MapData Then
                    Dim cArray() As Byte = Compress(dataArray)
                    Dim loadArray(cArray.Length) As Byte
                    loadArray(0) = id
                    cArray.CopyTo(loadArray, 1)
                    loadedPacket = Convert.ToBase64String(loadArray)
                Else
                    loadedPacket = Convert.ToBase64String(byteArray)
                End If

                Dim writer As New IO.StreamWriter(client.GetStream)
                writer.Write(loadedPacket & "-")
                writer.Flush()
            End SyncLock
        End If
    End Sub

    Public Sub CloseConnection()
        client.Close()
    End Sub

    Public Sub ParsePacket(packet As String)
        Dim dataBytes() As Byte = Convert.FromBase64String(packet)
        Dim packetID As Byte = dataBytes(0)
        Dim packetData As String = System.Text.Encoding.UTF8.GetString(dataBytes, 1, dataBytes.Length - 1)
        RaiseEvent LineReceived(Me, packetID, packetData)
    End Sub


    Public Static Function Compress(input As Byte()) As Byte()
        Dim output As Byte()
        Using ms As New MemoryStream()
            Using gs As New GZipStream(ms, CompressionMode.Compress)
                gs.Write(input, 0, input.Length)
                gs.Close()
                output = ms.ToArray()
            End Using
            ms.Close()
        End Using
        Return output
    End Function
End Class

Enum PacketID As Byte
    Disconnect = 0
    Connect = 1
    ConnectedUsers = 2
    Broadcast = 3
    Location = 50
    MapData = 100
    TileData = 101
    Ping = 255
End Enum
