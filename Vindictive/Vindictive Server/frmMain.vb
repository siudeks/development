﻿Imports System.Threading
Imports System.Net
Imports System.Net.Sockets
Imports System.Text

public Class frmMain

    private myThread As Thread
    private myListener As TcpListener
    private myClients As New List(Of UserConnection)
    private Status As New List(Of String)
    private lstAdd As New List(Of String)
    private lstRemove As New List(Of String)

    private map(5000, 5000) As Byte

    private Sub frmMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        myListener.Stop()
        End
    End Sub

    private Sub frmMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Show()
        myThread = New Thread(AddressOf ListenThread)
        myThread.IsBackground = True
        myThread.Start()
        UpdateStatus("Server Active...")
        txtChat.Focus()

        For y = 0 To 5000
            For x = 0 To 5000
                map(x, y) = 0
            Next
        Next
    End Sub

    private Sub ListenThread()
        Try
            myListener = New TcpListener(IPAddress.Any, 8191)
            myListener.Start()
            Do
                Dim newClient As New UserConnection(myListener.AcceptTcpClient)
                AddHandler newClient.Connected, AddressOf OnConnected
                AddHandler newClient.Disconnected, AddressOf OnDisconnected
                AddHandler newClient.LineReceived, AddressOf OnLineReceived
                myClients.Add(newClient)
                OnConnected(newClient)
            Loop
        Catch ex As Exception

        End Try
    End Sub

    private Sub OnConnected(sender As UserConnection)
        UpdateStatus("New Client Connected")
    End Sub

    private Sub OnDisconnected(sender As UserConnection)
        UpdateStatus(sender.IP & " (" & sender.UserName & ")" & " >> " & "Disconnected...")
        sender.CloseConnection()
        myClients.Remove(sender)
        lstRemove.Add(sender.UserName)
        For Each foundClient As UserConnection In myClients
            Dim strData As String = ""
            For xint = 0 To myClients.Count - 1
                strData = strData & myClients(xint).UserName & "|"
            Next
            foundClient.Send(PacketID.ConnectedUsers, strData)
            foundClient.Send(PacketID.Broadcast, "Server|" & sender.UserName & " disconnected.")
        Next
    End Sub

    private Sub OnLineReceived(sender As UserConnection, id As Byte, data As String)
        Select Case id
            Case PacketID.Connect
                sender.UserName = data
                lstAdd.Add(sender.UserName)
                For Each foundClient As UserConnection In myClients
                    Dim strData As String = ""
                    For xint = 0 To myClients.Count - 1
                        strData = strData & myClients(xint).UserName & "|"
                    Next
                    foundClient.Send(PacketID.ConnectedUsers, strData)
                    foundClient.Send(PacketID.Broadcast, "Server|" & sender.UserName & " connected.")
                Next
            Case PacketID.Disconnect
                Call OnDisconnected(sender)
            Case PacketID.Broadcast
                For Each foundClient As UserConnection In myClients
                    foundClient.Send(PacketID.Broadcast, data)
                Next
            Case PacketID.Ping
                sender.Send(PacketID.Ping, "")
            Case PacketID.TileData
                Dim cdata() As String = data.Split(",")
                If cdata(2) = 1 Then
                    map(cdata(0), cdata(1)) = 1
                End If
                For Each foundClient As UserConnection In myClients
                    foundClient.Send(PacketID.TileData, data)
                Next
            Case PacketID.Location
                For Each foundClient As UserConnection In myClients
                    foundClient.Send(PacketID.Location, data)
                Next
            Case PacketID.MapData
                Dim mapData As New StringBuilder
                For y = 0 To 500
                    For x = 0 To 500
                        mapData.Append(x & "," & y & "," & map(x, y) & "-")
                    Next
                Next
                sender.Send(PacketID.MapData, mapData.ToString)
        End Select
        If id = "255" Then
            'data = "Ping"
            'UpdateStatus(sender.IP & " (" & sender.UserName & ")" & " >> " & command & ": " & data)
        Else
            UpdateStatus(sender.IP & " (" & sender.UserName & ")" & " >> " & Command() & ": " & data)
        End If
    End Sub

    private Sub UpdateStatus(text As String)
        Status.Add(text)
    End Sub

    private Sub txtChat_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtChat.KeyDown
        If e.KeyCode = Keys.Enter And txtChat.Text.Length > 0 Then
            For Each foundClient As UserConnection In myClients
                foundClient.Send(PacketID.Broadcast, "Server|" & txtChat.Text)
            Next
            txtLog.AppendText("Server >> " & txtChat.Text)
            txtLog.AppendText(vbCrLf)
            txtChat.Text = ""
        End If
    End Sub

    private Sub txtChat_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtChat.KeyPress
        If e.KeyChar = Chr(13) Then e.Handled = True
    End Sub

    private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick

        Dim index As Integer = 0
        While index < Status.Count
            txtLog.AppendText(Status(0))
            txtLog.AppendText(vbCrLf)
            Status.RemoveAt(0)
            If txtLog.Text.Length > 50000 Then
                txtLog.ReadOnly = False
                txtLog.Select(0, txtLog.Text.Length - 50000)
                txtLog.SelectedText = ""
                txtLog.Select(txtLog.Text.Length, 0)
                txtLog.ScrollToCaret()
                txtLog.ReadOnly = True
            End If
        End While

        If lstAdd.Count > 0 Then
            lstClients.Items.Add(lstAdd(0))
            lstAdd.RemoveAt(0)
        End If

        If lstRemove.Count > 0 Then
            lstClients.Items.Remove(lstRemove(0))
            lstRemove.RemoveAt(0)
        End If
    End Sub


    private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        For x = 0 To 5000000
            Dim index As Integer = 0
            While index < myClients.Count
                myClients(index).Send(PacketID.Broadcast, "Server|" & x)
                index += 1
            End While
        Next
    End Sub


End Class