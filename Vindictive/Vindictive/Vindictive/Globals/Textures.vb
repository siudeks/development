﻿public Class Textures
    public static Dot As Texture2D
    public static TrackBar As Texture2D
    public static BG As Texture2D
    public static Particle As Texture2D

    public static Menu As Texture2D

    public static Stone As Texture2D
    public static Stone2 As Texture2D
    public static Grass As Texture2D
    public static Grass2 As Texture2D
    public static Grass3 As Texture2D
    public static Grass4 As Texture2D


    public static Sub Load()
        Dot = Globals.Content.Load(Of Texture2D)("Misc\Dot")
        TrackBar = Globals.Content.Load(Of Texture2D)("UI\\Trackbar")
        BG = Globals.Content.Load(Of Texture2D)("Misc\\bg")
        Particle = Globals.Content.Load(Of Texture2D)("Misc\\Particle")

        Menu = Globals.Content.Load(Of Texture2D)("System\Menu")

        Stone = Globals.Content.Load(Of Texture2D)("Misc\Stone")
        Stone2 = Globals.Content.Load(Of Texture2D)("Misc\Stone2")
        Grass = Globals.Content.Load(Of Texture2D)("Misc\Grass")
        Grass2 = Globals.Content.Load(Of Texture2D)("Misc\Grass2")
        Grass3 = Globals.Content.Load(Of Texture2D)("Misc\Grass3")
        Grass4 = Globals.Content.Load(Of Texture2D)("Misc\Grass4")

    End Sub
End Class