﻿Imports System.IO
Imports System.IO.Compression

public Class Globals
    public static Content As ContentManager
    public static Graphics As GraphicsDeviceManager
    public static SpriteBatch As SpriteBatch
    public static GameTime As GameTime
    public static WindowFocused As Boolean
    public static GameSize As Vector2
    public static UserConnection As UserConnection
    ' public static BackBuffer As RenderTarget2D

    public static Function Compress(input As Byte()) As Byte()
        Dim output As Byte()
        Using ms As New MemoryStream()
            Using gs As New GZipStream(ms, CompressionMode.Compress)
                gs.Write(input, 0, input.Length)
                gs.Close()
                output = ms.ToArray()
            End Using
            ms.Close()
        End Using
        Return output
    End Function

    public static Function Decompress(input As Byte()) As Byte()
        Dim output As New List(Of Byte)
        Using ms As New MemoryStream(input)
            Using gs As New GZipStream(ms, CompressionMode.Decompress)
                Dim readByte As Integer = gs.ReadByte()
                While readByte <> -1
                    output.Add(CType(readByte, Byte))
                    readByte = gs.ReadByte()
                End While
                gs.Close()
            End Using
            ms.Close()
        End Using
        Return output.ToArray()
    End Function
End Class
