﻿public Class Fonts
    public static Georgia_16 As SpriteFont
    public static Gisha_16 As SpriteFont
    public static Verdana_8 As SpriteFont
    public static Microsoft_Sans_Serif_8 As SpriteFont

    public static Sub Load()
        Georgia_16 = Globals.Content.Load(Of SpriteFont)("Fonts\Georgia_16")
        Gisha_16 = Globals.Content.Load(Of SpriteFont)("Fonts\Gisha_16")
        Verdana_8 = Globals.Content.Load(Of SpriteFont)("Fonts\Verdana_8")
        Microsoft_Sans_Serif_8 = Globals.Content.Load(Of SpriteFont)("Fonts\Microsoft_Sans_Serif_8")
    End Sub
End Class