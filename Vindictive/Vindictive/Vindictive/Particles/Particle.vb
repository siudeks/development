﻿public Class Particle
    public Texture As Texture2D
    public StartColor As Color
    public EndColor As Color
    public Location As Vector2
    public Size As Vector2
    public Energy As Integer
    public TotalEnergy As Integer
    public Direction As Single
    public Velocity As Single
    public AngularVelocity As Single
    public RotationVelocity As Single
    public Rotation As Single
    public Alive As Boolean = True
    private ParticleColor As Color

    public Sub Update()
        If Energy < TotalEnergy Then
            Direction += AngularVelocity
            If Direction > 360 Then
                Direction -= 360
            End If
            Location.X += Math.Cos(MathHelper.ToRadians(Direction)) * Velocity
            Location.Y += Math.Sin(MathHelper.ToRadians(Direction)) * Velocity
            Rotation += RotationVelocity
            ParticleColor = New Color(CByte(MathHelper.Lerp(StartColor.R, EndColor.R, Energy / TotalEnergy)),
                                      CByte(MathHelper.Lerp(StartColor.G, EndColor.G, Energy / TotalEnergy)),
                                      CByte(MathHelper.Lerp(StartColor.B, EndColor.B, Energy / TotalEnergy)))
            ParticleColor = ParticleColor * (1 - Energy / TotalEnergy)
        Else
            Alive = False
        End If
        Energy += 1
    End Sub

    public Sub Draw()
        Globals.SpriteBatch.Draw(Texture, New Rectangle(Location.X, Location.Y, Size.X, Size.Y),
                                 New Rectangle(0, 0, Texture.Width, Texture.Height), ParticleColor, MathHelper.ToRadians(Rotation),
                                 New Vector2(Texture.Width / 2, Texture.Height / 2), SpriteEffects.None, 0)
    End Sub
End Class