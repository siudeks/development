﻿public Class Input
    private static currentKeyState As KeyboardState
    private static lastKeyState As KeyboardState
    private static currentMouseState As MouseState
    private static lastMouseState As MouseState
    public static MousePosition As Point

    public static Sub Update()
        lastKeyState = currentKeyState
        currentKeyState = Keyboard.GetState()
        lastMouseState = currentMouseState
        currentMouseState = Mouse.GetState()
        MousePosition.X = currentMouseState.X
        MousePosition.Y = currentMouseState.Y
        'MousePosition.X = Math.Floor(Globals.GameSize.X / Globals.Graphics.GraphicsDevice.Viewport.Width * currentMouseState.X)
        'MousePosition.Y = Math.Floor(Globals.GameSize.Y / Globals.Graphics.GraphicsDevice.Viewport.Height * currentMouseState.Y)
    End Sub

    public static Function KeyDown(key As Keys) As Boolean
        Return currentKeyState.IsKeyDown(key)
    End Function

    public static Function KeyPressed(key As Keys) As Boolean
        If currentKeyState.IsKeyDown(key) And lastKeyState.IsKeyUp(key) Then
            return true;
        End If
        return false;
    End Function

    public static Function LeftDown() As Boolean
        If currentMouseState.LeftButton = ButtonState.Pressed Then
            return true;
        End If
        return false;
    End Function

    public static Function RightDown() As Boolean
        If currentMouseState.RightButton = ButtonState.Pressed Then
            return true;
        End If
        return false;
    End Function

    public static Function MiddleDown() As Boolean
        If currentMouseState.MiddleButton = ButtonState.Pressed Then
            return true;
        End If
        return false;
    End Function

    public static Function LeftPressed() As Boolean
        If currentMouseState.LeftButton = ButtonState.Pressed AndAlso lastMouseState.LeftButton = ButtonState.Released Then
            return true;
        End If
        return false;
    End Function

    public static Function RightPressed() As Boolean
        If currentMouseState.RightButton = ButtonState.Pressed AndAlso lastMouseState.RightButton = ButtonState.Released Then
            return true;
        End If
        return false;
    End Function

    public static Function MiddlePressed() As Boolean
        If currentMouseState.MiddleButton = ButtonState.Pressed AndAlso lastMouseState.MiddleButton = ButtonState.Released Then
            return true;
        End If
        return false;
    End Function

    public static Function ScrollUp() As Boolean
        If currentMouseState.ScrollWheelValue > lastMouseState.ScrollWheelValue Then
            return true;
        End If
        return false;
    End Function

    public static Function ScrollDown() As Boolean
        If currentMouseState.ScrollWheelValue < lastMouseState.ScrollWheelValue Then
            return true;
        End If
        return false;
    End Function
End Class
