﻿public Class clsMainMenu
    Inherits BaseScreen

    private myFont As SpriteFont = Fonts.Gisha_16
    private MenuColor As Color = Color.White
    private cursorFlash As Boolean = True
    private MenuList As New List(Of String)
    private UserName As String = ""

    private oldKeyState As KeyboardState
    private keyState As KeyboardState

    private window As New Window(0, 0, 202, 88, Color.MidnightBlue, Color.CornflowerBlue, True)

    public Sub New()
        State = ScreenState.FadeIn
        MenuList.Add("User Name:")
        Name = "MainMenu"
    End Sub

    public Overrides Sub HandleInput()
        oldKeyState = keyState
        keyState = Keyboard.GetState
        If Focused = True And State = ScreenState.Active Then
            For Each foundKey As Keys In keyState.GetPressedKeys
                If oldKeyState.IsKeyUp(foundKey) Then
                    If foundKey = Keys.Back Then
                        If UserName.Length > 0 Then UserName = UserName.Remove(UserName.Length - 1, 1)
                    ElseIf foundKey = Keys.Enter Then
                        If UserName.Length > 0 Then
                            ScreenManager.UnloadScreen("MainMenu")
                            Dim NewScreen As New clsConnect
                            NewScreen.UserName = UserName
                            ScreenManager.AddScreen(NewScreen)
                        End If
                    Else
                        If foundKey >= 48 And foundKey <= 57 Then
                            If UserName.Length < 11 Then
                                UserName += foundKey.ToString.Substring(1, 1)
                            End If
                        End If
                        If foundKey >= 65 And foundKey <= 90 Then
                            If UserName.Length < 11 Then
                                UserName += foundKey.ToString.ToLower
                                UserName = StrConv(UserName, VbStrConv.ProperCase)
                            End If
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    public Overrides Sub Update()
        MyBase.Update()
        UpdateCursorFlash()
    End Sub

    public Overrides Sub Draw()
        MyBase.Draw()
        Globals.SpriteBatch.Begin()

        Dim width As Integer = 202
        Dim height As Integer = 88
        Dim locX As Integer = (Globals.Graphics.GraphicsDevice.Viewport.Width - width) / 2 * Position
        Dim locY As Integer = (Globals.Graphics.GraphicsDevice.Viewport.Height - height) / 2

        Window.Draw(locX, locY, Alpha)

        Dim sRect As New Rectangle(0, 0, 64, 24)
        Dim dRect As New Rectangle(locX + 19, locY + 19 + myFont.MeasureString(MenuList.Item(0)).Y, width - 38, myFont.MeasureString(MenuList.Item(0)).Y - 6)
        Globals.SpriteBatch.Draw(Textures.Menu, dRect, sRect, Color.CornflowerBlue * Alpha)

        If State = ScreenState.Active Then
            Dim centerX As Integer = (width - myFont.MeasureString(MenuList.Item(0)).X) / 2
            Globals.SpriteBatch.DrawString(myFont, MenuList.Item(0), New Vector2(locX + centerX, locY + 16), Color.White * Alpha, 0, New Vector2(0, 0), 1, SpriteEffects.None, 0)
            centerX = (width - myFont.MeasureString(UserName).X) / 2
            Globals.SpriteBatch.DrawString(myFont, UserName, New Vector2(locX + centerX, locY + 16 + myFont.MeasureString(UserName).Y), Color.White * Alpha, 0, New Vector2(0, 0), 1, SpriteEffects.None, 0)
            Globals.SpriteBatch.DrawString(myFont, "_", New Vector2(locX + centerX + myFont.MeasureString(UserName).X, locY + 16 + myFont.MeasureString("_").Y), MenuColor * Alpha, 0, New Vector2(0, 0), 1, SpriteEffects.None, 0)
        End If

        Globals.SpriteBatch.End()
    End Sub

    private Sub UpdateCursorFlash()
        If cursorFlash Then
            MenuColor.B -= 10
            MenuColor.G -= 10
            MenuColor.R -= 10
            If MenuColor.B < 75 Or MenuColor.R < 75 Or MenuColor.G < 75 Then
                cursorFlash = False
            End If
        Else
            MenuColor.B += 10
            MenuColor.G += 10
            MenuColor.R += 10
            If MenuColor.B >= 245 Or MenuColor.R >= 245 Or MenuColor.G >= 245 Then
                cursorFlash = True
            End If
        End If
    End Sub
End Class
