﻿public Class MainMenu
    Inherits BaseScreen

    public static MenuColor As Color = New Color(223, 208, 186)
    private menuSelectAlpha As Double = 1
    private menuFlash As Byte = 0
    private menuSelect As Integer = 0
    private menuEntries As New List(Of MenuEntry)
    private window As Window

    Dim menuWidth As Integer = 0
    Dim menuHeight As Integer = 0

    public Sub New()
        Dim NewEntry As MenuEntry
        NewEntry = New MenuEntry
        NewEntry.Text = "New Game"
        MenuEntries.Add(NewEntry)
        NewEntry = New MenuEntry
        NewEntry.Text = "Load Game"
        NewEntry.Enabled = False
        MenuEntries.Add(NewEntry)
        NewEntry = New MenuEntry
        NewEntry.Text = "Options"
        NewEntry.Enabled = False
        MenuEntries.Add(NewEntry)
        NewEntry = New MenuEntry
        NewEntry.Text = "Exit Game"
        MenuEntries.Add(NewEntry)
        Name = "MainMenu"



        
        For xInt = 0 To menuEntries.Count - 1
            If menuWidth < Fonts.Georgia_16.MeasureString(menuEntries.Item(xInt).Text).X Then menuWidth = Fonts.Georgia_16.MeasureString(menuEntries.Item(xInt).Text).X
            menuHeight += Fonts.Georgia_16.MeasureString(menuEntries.Item(xInt).Text).Y
        Next
        menuWidth += 64
        menuHeight += 32

        window = New Window(0, 0, menuWidth, menuHeight, Color.Aqua, Color.Aqua, True)
    End Sub

    public Overrides Sub HandleInput()
        If Focused = True And State = ScreenState.Active Then
            If Input.KeyPressed(Keys.Up) Then
                menuSelect -= 1
                If menuSelect < 0 Then menuSelect = menuEntries.Count - 1
                Do Until menuEntries(menuSelect).Enabled = True
                    menuSelect -= 1
                    If menuSelect < 0 Then menuSelect = menuEntries.Count - 1
                Loop
            End If
            If Input.KeyPressed(Keys.Down) Then
                menuSelect += 1
                If menuSelect > menuEntries.Count - 1 Then menuSelect = 0
                Do Until menuEntries(menuSelect).Enabled = True
                    menuSelect += 1
                    If menuSelect > menuEntries.Count - 1 Then menuSelect = 0
                Loop
            End If
            If Input.KeyPressed(Keys.Enter) Then
                Select Case menuSelect
                    Case 0
                        'ScreenManager.UnloadScreen("BackGround")
                        ScreenManager.UnloadScreen("MainMenu")
                        'ScreenManager.NewScreens.Add(New Gameplay)
                        'ScreenManager.NewScreens.Add(New Overlay)
                    Case 1

                    Case 2

                    Case 3
                        End
                End Select
            End If
            If Input.KeyPressed(Keys.F1) Then
                Dim rnd As New Random
                MenuColor.R = rnd.Next(0, 255)
                MenuColor.G = rnd.Next(0, 255)
                MenuColor.B = rnd.Next(0, 255)
            End If
        End If
    End Sub

    public Overrides Sub Draw()
        MyBase.Draw()
        Globals.SpriteBatch.Begin()
        Dim menuX As Integer = (800 - menuWidth) / 2 * Position
        Dim menuY As Integer = 450 - menuHeight - 35
        window.Draw(menuX, menuY, Alpha)
        Dim fontY As Integer = 16
        For xInt = 0 To menuEntries.Count - 1
            If xInt = menuSelect Then
                If menuFlash = 0 Then
                    menuSelectAlpha -= 0.02
                    If menuSelectAlpha <= 0.65 Then
                        menuFlash = 1
                    End If
                Else
                    menuSelectAlpha += 0.02
                    If menuSelectAlpha >= 1.3 Then
                        menuFlash = 0
                    End If
                End If
            End If
            Dim centerX As Integer = (menuWidth - Fonts.Georgia_16.MeasureString(menuEntries.Item(xInt).Text).X) / 2
            If State = ScreenState.Active Then
                Globals.SpriteBatch.DrawString(Fonts.Georgia_16, menuEntries.Item(xInt).Text, New Vector2(menuX + centerX + 1, menuY + fontY + 2), Color.Black * Alpha, 0, New Vector2(0, 0), 1, SpriteEffects.None, 0)
                If xInt = menuSelect Then
                    Globals.SpriteBatch.DrawString(Fonts.Georgia_16, menuEntries.Item(xInt).Text, New Vector2(menuX + centerX, menuY + fontY), MenuColor * menuSelectAlpha * Alpha, 0, New Vector2(0, 0), 1, SpriteEffects.None, 0)
                ElseIf menuEntries.Item(xInt).Enabled = True Then
                    Globals.SpriteBatch.DrawString(Fonts.Georgia_16, menuEntries.Item(xInt).Text, New Vector2(menuX + centerX, menuY + fontY), Color.White * Alpha, 0, New Vector2(0, 0), 1, SpriteEffects.None, 0)
                Else
                    Globals.SpriteBatch.DrawString(Fonts.Georgia_16, menuEntries.Item(xInt).Text, New Vector2(menuX + centerX, menuY + fontY), Color.Gray * Alpha, 0, New Vector2(0, 0), 1, SpriteEffects.None, 0)
                End If
            End If
            fontY += Fonts.Georgia_16.MeasureString(menuEntries.Item(xInt).Text).Y
        Next
        Globals.SpriteBatch.End()
    End Sub
End Class
