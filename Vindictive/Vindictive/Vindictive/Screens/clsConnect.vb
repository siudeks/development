﻿public Class clsConnect
    Inherits BaseScreen


    private myFont As SpriteFont = Fonts.Gisha_16

    private Status As String = " "

    public UserName As String

    private sendConnect As Boolean = False
    private timeConnect As Integer

    private window As New Window(0, 0, 215, 60, Color.MidnightBlue, Color.CornflowerBlue, True)

    public Sub New()
        TransitionTime = 375
        State = ScreenState.FadeIn
        Name = "Connect"
        AddHandler Globals.UserConnection.ConnectionStatus, AddressOf UpdateStatus
    End Sub

    public Sub UpdateStatus(data As String)
        Status = data
    End Sub

    public Overrides Sub Update()
        MyBase.Update()
        If Globals.GameTime.TotalGameTime.TotalMilliseconds - timeConnect < 1500 And timeConnect > 0 Then Return
        If sendConnect = False Then
            Globals.UserConnection.Connect(UserName)
            sendConnect = True
        End If
        If Status = "Connected" Then
            ScreenManager.UnloadScreen("Connect")
            ScreenManager.AddScreen(New World)
            ScreenManager.AddScreen(New clsChat)
        ElseIf Status = "Unable to connect" Then
            ScreenManager.UnloadScreen("Connect")
            ScreenManager.AddScreen(New clsMainMenu)
        End If
        timeConnect = Globals.GameTime.TotalGameTime.TotalMilliseconds
    End Sub

    public Overrides Sub Draw()
        Globals.SpriteBatch.Begin()

        Dim width As Integer = 215
        Dim height As Integer = 60
        Dim locX As Integer = (Globals.Graphics.GraphicsDevice.Viewport.Width - width) / 2 * Position
        Dim locY As Integer = (Globals.Graphics.GraphicsDevice.Viewport.Height - height) / 2

        window.Draw(locX, locY, Alpha)

        If State = ScreenState.Active Then
            Dim centerX As Integer = (width - myFont.MeasureString(Status).X) / 2
            Globals.SpriteBatch.DrawString(myFont, Status, New Vector2(locX + centerX, locY + 16), Color.White * Alpha, 0, New Vector2(0, 0), 1, SpriteEffects.None, 0)
        End If
        Globals.SpriteBatch.End()
    End Sub
End Class
