﻿public Class TestScreen2
    Inherits BaseScreen

    private window As New Rectangle(50, 50, 400, 400)
    private camera As New Vector2(0, 0)

    private renderSize As Integer

    private lastPos As New Vector2(0, 0)
    private newRender As RenderTarget2D

    private map(1000, 1000) As Texture2D

    private mapx As Integer = 0
    private mapy As Integer = 0

    private xPos As Integer = 0
    private yPos As Integer = 0

    private w As New Window(0, 0, 406, 406, Color.CornflowerBlue, Color.CornflowerBlue, False)

    Dim r As Integer
    Dim t As Integer

    public Sub New()
        camera.X = window.Center.X - 23

        r = Math.Ceiling((window.Center.Y - window.Y) / 44) * 44
        t = Math.Ceiling((window.Center.X - window.X) / 44) * 44

        camera.Y = window.Center.Y - 22 - (r + t)

        renderSize = (r + t) / 44


        Name = "TestScreen"
        Dim rand As New Random
        For x = 0 To 1000
            For y = 0 To 1000
                If rand.Next(100) < 75 Then
                    map(x, y) = Textures.Stone
                Else
                    map(x, y) = Textures.Stone2
                End If
            Next
        Next
    End Sub

    public Overrides Sub HandleInput()
        MyBase.HandleInput()
        If Input.KeyDown(Keys.W) Then
            yPos -= Math.Floor(Globals.GameTime.ElapsedGameTime.TotalMilliseconds / 8)
        End If
        If Input.KeyDown(Keys.S) Then
            yPos += Math.Floor(Globals.GameTime.ElapsedGameTime.TotalMilliseconds / 8)
          
        End If
        If Input.KeyDown(Keys.A) Then
            xPos -= Math.Floor(Globals.GameTime.ElapsedGameTime.TotalMilliseconds / 8)
        End If
        If Input.KeyDown(Keys.D) Then
            xPos += Math.Floor(Globals.GameTime.ElapsedGameTime.TotalMilliseconds / 8)
        End If


        If Input.MiddlePressed Then
            lastPos.X = Input.MousePosition.X
            lastPos.Y = Input.MousePosition.Y
        End If

        If Input.MiddleDown Then
            xPos += Input.MousePosition.X - lastPos.X
            yPos += Input.MousePosition.Y - lastPos.Y
            lastPos.X = Input.MousePosition.X
            lastPos.Y = Input.MousePosition.Y
        End If

        If yPos <= 22 * -1 And xPos <= 22 * -1 Then
            yPos += 22
            xPos += 22
            mapx += 1
        End If
        If yPos >= 22 And xPos >= 22 Then
            yPos -= 22
            xPos -= 22
            'mapy += 1
            mapx -= 1
        End If

        If yPos <= 22 * -1 And xPos >= 22 Then
            yPos += 22
            xPos -= 22
            mapy += 1
        End If
        If yPos >= 22 And xPos <= 22 * 1 Then
            yPos -= 22
            xPos += 22
            mapy -= 1
        End If
        If yPos <= 44 * -1 Then
            yPos += 44
            mapy += 1
            mapx += 1
        End If
        If yPos >= 44 Then
            yPos -= 44
            mapy -= 1
            mapx -= 1
        End If
        If xPos <= 44 * -1 Then
            xPos += 44
            mapy -= 1
            mapx += 1
        End If
        If xPos >= 44 Then
            xPos -= 44
            mapy += 1
            mapx -= 1
        End If

    End Sub

    public Overrides Sub Update()
        MyBase.Update()
    End Sub

    public Overrides Sub Draw()
        MyBase.Draw()

        '  NewRenderTarget()
        Globals.SpriteBatch.Begin()
        Dim dColor As Color = Color.White
        Dim mX As Integer = Math.Floor((Input.MousePosition.Y - camera.Y + window.Y) / 44 + (Input.MousePosition.X - 22 - camera.X - window.X) / 44) - renderSize + mapx
        Dim mY As Integer = Math.Floor((Input.MousePosition.Y - camera.Y - window.Y) / 44 - (Input.MousePosition.X - 22 - camera.X - window.X) / 44) - renderSize + mapy

        Dim newX As Integer
        Dim newY As Integer

        For y = 0 To renderSize * 2
            For x = 0 To renderSize * 2
                newX = mapx + x - renderSize
                newY = mapy + y - renderSize
                If newX < 0 Or newY < 0 Or newX > 1000 Or newY > 1000 Then

                Else
                    If mX = newX And mY = newY And window.Contains(Input.MousePosition) Then
                        dColor = Color.Red
                    Else
                        dColor = Color.White
                    End If
                    Dim tileRect As New Rectangle((x - y) * 22 + xPos + camera.X, (x + y) * 22 + yPos + camera.Y, 44, 44)
                    If window.Intersects(tileRect) Then
                        Globals.SpriteBatch.Draw(map(newX, newY), tileRect, dColor)
                        'Else
                        '    Globals.SpriteBatch.Draw(map(newX, newY), tileRect, dColor)
                    End If
                End If
            Next
        Next

        Globals.SpriteBatch.End()
        ' EndNewRender()
        Globals.SpriteBatch.Begin()

        ' w.Draw(window.X - 3, window.Y - 3, 1)

        Globals.SpriteBatch.DrawString(Fonts.Microsoft_Sans_Serif_8, mX & "-" & mY, New Vector2(10, 420), Color.White)
        Globals.SpriteBatch.DrawString(Fonts.Microsoft_Sans_Serif_8, mapx & "-" & mapy, New Vector2(10, 410), Color.White)
        Globals.SpriteBatch.DrawString(Fonts.Microsoft_Sans_Serif_8, xPos & "-" & yPos, New Vector2(10, 400), Color.White)
        Globals.SpriteBatch.Draw(Textures.Dot, New Rectangle(window.Center.X, window.Center.Y, 1, 1), dColor)
        Globals.SpriteBatch.End()
    End Sub

    Protected Sub NewRenderTarget()
        If newRender Is Nothing Then
            newRender = New RenderTarget2D(Globals.Graphics.GraphicsDevice, window.Width, window.Height)
        End If
        Globals.Graphics.GraphicsDevice.SetRenderTarget(newRender)
        Globals.Graphics.GraphicsDevice.Clear(Color.black)
    End Sub

    Protected Sub EndNewRender()
        ' Globals.Graphics.GraphicsDevice.SetRenderTarget(Globals.backBuffer)
        Globals.Graphics.GraphicsDevice.SetRenderTarget(Nothing)
        Globals.SpriteBatch.Begin()
        Globals.SpriteBatch.Draw(newRender, New Rectangle(window.X, window.Y, window.Width, window.Height), Color.White)
        Globals.SpriteBatch.End()
    End Sub
End Class

