﻿public Class World
    Inherits BaseScreen

    private bounds As New Rectangle(0, 0, 400, 300)
    private border As New Window(0, 0, 412, 312, Color.CornflowerBlue, Color.White, False)
    private camera As Camera

    private lastPos As Point
    private newRender As RenderTarget2D

    private map(5000, 5000) As Texture2D


    private DoRenderTarget As Boolean = False

    private MoveWindow As Boolean = False
    private ResizeWindow As Boolean = False
    private CropWindow As Boolean = True

    private loc As Point
    private oset As Point

    private MoveDir As Dir
    private Moving As Boolean = False
    private MoveTime As Double

    private Enum Dir
        None = 0
        N = 1
        NE = 2
        E = 3
        SE = 4
        S = 5
        SW = 6
        W = 7
        NW = 8
    End Enum


    public Sub New()
        Name = "World"
        Dim rand As New Random
        'For x = 0 To 1000
        '    For y = 0 To 1000
        '        If rand.Next(1000) < 3 Then
        '            map(x, y) = Textures.Stone
        '        ElseIf rand.Next(1000) < 6 Then
        '            map(x, y) = Textures.Stone2
        '        ElseIf rand.Next(1000) < 250 Then
        '            map(x, y) = Textures.Grass
        '        ElseIf rand.Next(1000) < 500 Then
        '            map(x, y) = Textures.Grass2
        '        ElseIf rand.Next(1000) < 750 Then
        '            map(x, y) = Textures.Grass3
        '        Else
        '            map(x, y) = Textures.Grass4
        '        End If
        '    Next
        'Next
        camera = New Camera(bounds.Width, bounds.Height)
        camera.MapPosition.X = 0
        camera.MapPosition.Y = 0

        AddHandler Globals.UserConnection.PacketHandler.MapData, AddressOf MapPacket
        AddHandler Globals.UserConnection.PacketHandler.TileData, AddressOf TilePacket
        AddHandler Globals.UserConnection.PacketHandler.Location, AddressOf LocationPacket
        Globals.UserConnection.Send(PacketID.MapData, "")
    End Sub

    public Overrides Sub HandleInput()
        Dim location As Point = camera.MapPosition
        Dim offset As New Point(camera.TileOffset.X, camera.TileOffset.Y)
        MyBase.HandleInput()
        If Input.KeyPressed(Keys.F1) Then
            If CropWindow Then
                CropWindow = False
            Else
                CropWindow = True
            End If
            camera.Update()
        End If
        If Input.KeyPressed(Keys.F2) Then
            If DoRenderTarget Then
                DoRenderTarget = False
            Else
                DoRenderTarget = True
            End If
        End If
        If Input.KeyPressed(Keys.F4) Then
            'camera.MapPosition.X = 10
            'camera.MapPosition.Y = 15
            camera.Update()
        End If


        If Globals.UserConnection.isConnected And Input.LeftPressed And bounds.Contains(Input.MousePosition) Then
            If camera.MapMouse(bounds).X >= 0 And camera.MapMouse(bounds).Y >= 0 Then
                If Globals.UserConnection.isConnected Then Globals.UserConnection.Send(PacketID.TileData, camera.MapMouse(bounds).X & "," & camera.MapMouse(bounds).Y & "," & 1)
            End If
        End If

        If ResizeWindow = False And New Rectangle(border.bounds.X + border.bounds.Width - 9, border.bounds.Y + border.bounds.Height - 9, 9, 9).Contains(Input.MousePosition) Then
            lastPos = Input.MousePosition
            ResizeWindow = True
        ElseIf MoveWindow = False And border.bounds.Contains(Input.MousePosition) And bounds.Contains(Input.MousePosition) = False And Input.LeftPressed Then
            lastPos = Input.MousePosition
            MoveWindow = True
        End If

        If Input.LeftDown And ResizeWindow Then
            Dim sizeX As Integer = MathHelper.Clamp(Input.MousePosition.X - lastPos.X, -5000, Globals.Graphics.GraphicsDevice.Viewport.Width - bounds.Width - 12)
            Dim sizeY As Integer = MathHelper.Clamp(Input.MousePosition.Y - lastPos.Y, -5000, Globals.Graphics.GraphicsDevice.Viewport.Height - bounds.Height - 12)

            border.bounds.Width += sizeX
            border.bounds.Height += sizeY

            bounds.Width += sizeX
            bounds.Height += sizeY

            camera.Size.Width += sizeX
            camera.Size.Height += sizeY
            camera.Update()
            lastPos = Input.MousePosition
        End If
        If Input.LeftDown And MoveWindow Then
            bounds.X += Input.MousePosition.X - lastPos.X
            bounds.Y += Input.MousePosition.Y - lastPos.Y
            lastPos = Input.MousePosition
        End If
        If Input.LeftDown = False Then
            MoveWindow = False
            ResizeWindow = False
        End If

        'If Input.RightPressed Then
        '    lastPos = Input.MousePosition
        'End If

        'If Input.RightDown Then
        '    camera.TileOffset.X += Input.MousePosition.X - lastPos.X
        '    camera.TileOffset.Y += Input.MousePosition.Y - lastPos.Y
        '    lastPos = Input.MousePosition
        'Else
        '    camera.TileOffset.X = 0
        '    camera.TileOffset.Y = 0
        'End If

        If Input.RightDown And Moving = False Then
            Dim distanceX = Mouse.GetState.X - Globals.Graphics.GraphicsDevice.Viewport.Width / 2
            Dim distanceY = Mouse.GetState.Y - Globals.Graphics.GraphicsDevice.Viewport.Height / 2
            Dim radians = Math.Atan2(distanceY, distanceX)
            Dim degrees As Double = radians * (180 / Math.PI)
            Select Case degrees
                Case -22.5 To 22.5
                    MoveDir = Dir.NE
                Case 22.5 To 67.5
                    MoveDir = Dir.E
                Case 67.5 To 112.5
                    MoveDir = Dir.SE
                Case 112.5 To 157.5
                    MoveDir = Dir.S
                Case 157.5 To 180
                    MoveDir = Dir.SW
                Case -67.5 To -22.5
                    MoveDir = Dir.N
                Case -112.5 To -67.5
                    MoveDir = Dir.NW
                Case -157.5 To -112.5
                    MoveDir = Dir.W
                Case -180 To -157.5
                    MoveDir = Dir.SW
            End Select
            Moving = True
            MoveTime = Globals.GameTime.TotalGameTime.TotalMilliseconds
        End If

        If Moving = True Then
            Select Case MoveDir
                Case Dir.N
                    camera.TileOffset.Y += 1
                    camera.TileOffset.X -= 1
                Case Dir.NE
                    camera.TileOffset.X -= 1.5
                Case Dir.E
                    camera.TileOffset.Y -= 1
                    camera.TileOffset.X -= 1
                Case Dir.SE
                    camera.TileOffset.Y -= 1.5
                Case Dir.S
                    camera.TileOffset.Y -= 1
                    camera.TileOffset.X += 1
                Case Dir.SW
                    camera.TileOffset.X += 1.5
                Case Dir.W
                    camera.TileOffset.Y += 1
                    camera.TileOffset.X += 1
                Case Dir.NW
                    camera.TileOffset.Y += 1.5
            End Select
        End If
       

        If camera.TileOffset.Y <= 22 * -1 And camera.TileOffset.X <= 22 * -1 Then
            camera.TileOffset.Y = 0
            camera.TileOffset.X = 0
            camera.MapPosition.X += 1
            Moving = False
        End If
        If camera.TileOffset.Y >= 22 And camera.TileOffset.X >= 22 Then
            camera.TileOffset.Y = 0
            camera.TileOffset.X = 0
            camera.MapPosition.X -= 1
            Moving = False
        End If

        If camera.TileOffset.Y <= 22 * -1 And camera.TileOffset.X >= 22 Then
            camera.TileOffset.Y = 0
            camera.TileOffset.X = 0
            camera.MapPosition.Y += 1
            Moving = False
        End If
        If camera.TileOffset.Y >= 22 And camera.TileOffset.X <= 22 * -1 Then
            camera.TileOffset.Y = 0
            camera.TileOffset.X = 0
            camera.MapPosition.Y -= 1
            Moving = False
        End If
        If camera.TileOffset.Y <= 44 * -1 Then
            camera.TileOffset.Y = 0
            camera.MapPosition.Y += 1
            camera.MapPosition.X += 1
            Moving = False
        End If
        If camera.TileOffset.Y >= 44 Then
            camera.TileOffset.Y = 0
            camera.MapPosition.Y -= 1
            camera.MapPosition.X -= 1
            Moving = False
        End If
        If camera.TileOffset.X <= 44 * -1 Then
            camera.TileOffset.X = 0
            camera.MapPosition.Y -= 1
            camera.MapPosition.X += 1
            Moving = False
        End If
        If camera.TileOffset.X >= 44 Then
            camera.TileOffset.X = 0
            camera.MapPosition.Y += 1
            camera.MapPosition.X -= 1
            Moving = False
        End If
        If Globals.UserConnection.isConnected And location <> camera.MapPosition Then
            Globals.UserConnection.Send(PacketID.Location, Globals.UserConnection.UserName & "," & camera.MapPosition.X & "," & camera.MapPosition.Y & "," & camera.TileOffset.X & "," & camera.TileOffset.Y)
        End If
    End Sub

    public Overrides Sub Update()
        MyBase.Update()
       
        bounds.Width = Globals.Graphics.GraphicsDevice.Viewport.Width
        bounds.Height = Globals.Graphics.GraphicsDevice.Viewport.Height
        camera.Size.Width = bounds.Width
        camera.Size.Height = bounds.Height
        camera.Update()
    End Sub

    public Overrides Sub Draw()
        MyBase.Draw()

        If DoRenderTarget Then
            'NewRenderTarget()
            'camera.Size.X = 0
            'camera.Size.Y = 0
        Else
            camera.Size.X = bounds.X
            camera.Size.Y = bounds.Y
        End If
        Globals.SpriteBatch.Begin() '0, BlendState.NonPremultiplied, Nothing, Nothing, Nothing, Textures.white)
        Globals.SpriteBatch.Draw(Textures.Dot, bounds, Color.Black)
        Dim dColor As Color = Color.White

        Dim newX As Integer
        Dim newY As Integer
        Dim blueRect As New Rectangle
        For y = 0 To camera.DrawArraySize
            For x = 0 To camera.DrawArraySize
                newX = camera.MapPosition.X + x - camera.DrawArraySize / 2
                newY = camera.MapPosition.Y + y - camera.DrawArraySize / 2
                If newX < 0 Or newY < 0 Or newX > 5000 Or newY > 5000 Then
                    'Dim tileRect As New Rectangle((x - y) * 22 + camera.TileOffset.X + camera.DrawOffset.X + camera.Size.X, (x + y) * 22 + camera.TileOffset.Y + camera.DrawOffset.Y + camera.Size.Y, 44, 44)
                    'If camera.Size.Intersects(tileRect) Then
                    '    Globals.SpriteBatch.Draw(Textures.Grass, tileRect, Color.Black)
                    'End If
                Else
                    If camera.MapMouse(bounds).X = newX And camera.MapMouse(bounds).Y = newY And bounds.Contains(Input.MousePosition) Then
                        dColor = Color.Red
                    Else
                        dColor = Color.White
                    End If
                    Dim tileRect As New Rectangle((x - y) * 22 + camera.TileOffset.X + camera.DrawOffset.X + camera.Size.X, (x + y) * 22 + camera.TileOffset.Y + camera.DrawOffset.Y + camera.Size.Y, 44, 44)
                    If map(newX, newY) IsNot Nothing Then
                        If CropWindow And camera.Size.Intersects(tileRect) Then
                            Globals.SpriteBatch.Draw(map(newX, newY), tileRect, dColor)
                        ElseIf CropWindow = False Then
                            Globals.SpriteBatch.Draw(map(newX, newY), tileRect, dColor)
                        End If
                    End If
                    If loc.X = newX And loc.Y = newY Then
                        blueRect = tileRect
                    End If
                End If
            Next
        Next

        Globals.SpriteBatch.Draw(Textures.Dot, New Rectangle(blueRect.X + 12 - oset.X, blueRect.Y + 12 - oset.Y, 20, 20), Color.Blue)

        Globals.SpriteBatch.End()
        'If DoRenderTarget Then EndNewRender()
        Globals.SpriteBatch.Begin()

        '  border.Draw(bounds.X - 6, bounds.Y - 6, 1)

        Globals.SpriteBatch.DrawString(Fonts.Microsoft_Sans_Serif_8, "Mouse Pos: " & camera.MapMouse(bounds).X & "-" & camera.MapMouse(bounds).Y, New Vector2(10, 110), Color.White)
        Globals.SpriteBatch.DrawString(Fonts.Microsoft_Sans_Serif_8, "Camera Center: " & camera.MapPosition.X & "-" & camera.MapPosition.Y, New Vector2(10, 120), Color.White)
        Globals.SpriteBatch.DrawString(Fonts.Microsoft_Sans_Serif_8, "Camera Offset: " & camera.TileOffset.X & "-" & camera.TileOffset.Y, New Vector2(10, 130), Color.White)

        Globals.SpriteBatch.Draw(Textures.Dot, New Rectangle(bounds.Center.X - 5, bounds.Center.Y, 11, 1), dColor)
        Globals.SpriteBatch.Draw(Textures.Dot, New Rectangle(bounds.Center.X, bounds.Center.Y - 5, 1, 11), dColor)
        Globals.SpriteBatch.End()
    End Sub

    private Sub MapPacket(data As String)
        For Each mapData As String In data.Split("-")
            Dim splitData() As String = mapData.Split(",")
            If splitData(2) = 0 Then
                map(splitData(0), splitData(1)) = Textures.Grass
            Else
                map(splitData(0), splitData(1)) = Textures.Stone
            End If
        Next
    End Sub

    private Sub TilePacket(data As String)
        Dim splitData() As String = data.Split(",")
        If splitData(2) = 1 Then
            map(splitData(0), splitData(1)) = Textures.Stone
        End If
    End Sub

    private Sub LocationPacket(data As String)
        Dim splitData() As String = data.Split(",")
        If splitData(0) <> Globals.UserConnection.UserName Then
            loc.X = splitData(1)
            loc.Y = splitData(2)
            oset.X = splitData(3)
            oset.Y = splitData(4)
        End If
    End Sub

    'Protected Sub NewRenderTarget()
    '    If newRender Is Nothing Then
    '        newRender = New RenderTarget2D(Globals.Graphics.GraphicsDevice, bounds.Width, bounds.Height)
    '    ElseIf newRender.Bounds.Width <> bounds.Width And newRender.Bounds.Height <> bounds.Height Then
    '        newRender.Dispose()
    '        newRender = New RenderTarget2D(Globals.Graphics.GraphicsDevice, bounds.Width, bounds.Height)
    '    End If
    '    Globals.Graphics.GraphicsDevice.SetRenderTarget(newRender)
    '    Globals.Graphics.GraphicsDevice.Clear(Color.Black)
    'End Sub

    'Protected Sub EndNewRender()
    '    ' Globals.Graphics.GraphicsDevice.SetRenderTarget(Globals.backBuffer)
    '    Globals.Graphics.GraphicsDevice.SetRenderTarget(Nothing)
    '    Globals.Graphics.GraphicsDevice.Clear(Color.CornflowerBlue)
    '    Globals.SpriteBatch.Begin()
    '    Globals.SpriteBatch.Draw(newRender, New Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height), New Rectangle(0, 0, bounds.Width, bounds.Height), Color.White)
    '    Globals.SpriteBatch.End()
    'End Sub
End Class



public Class Camera

    public TileOffset As Vector2 'xpos ypos
    public DrawOffset As Point 'camera
    public MapPosition As Point 'mapx mapy
    public Size As Rectangle
    public DrawArraySize As Integer

    public Sub New(width As Integer, height As Integer)
        Size.Width = width
        Size.Height = height
        Update()
    End Sub

    public Sub Update()
        Dim widthDistance As Integer = Math.Ceiling((Size.Width / 2) / 44) * 44
        Dim heightDistance As Integer = Math.Ceiling((Size.Height / 2) / 44) * 44

        DrawOffset.X = Size.Width / 2 - 22
        DrawOffset.Y = Size.Height / 2 - 22 - (widthDistance + heightDistance)

        DrawArraySize = (widthDistance + heightDistance) / 44 * 2
    End Sub

    public ReadOnly Property MapMouse(bounds As Rectangle) As Point
        Get
            MapMouse.X = Math.Floor((Input.MousePosition.Y - bounds.Y - DrawOffset.Y) / 44 + (Input.MousePosition.X - bounds.X - 22 - DrawOffset.X) / 44) - DrawArraySize / 2 + MapPosition.X
            MapMouse.Y = Math.Floor((Input.MousePosition.Y - bounds.Y - DrawOffset.Y) / 44 - (Input.MousePosition.X - bounds.X - 22 - DrawOffset.X) / 44) - DrawArraySize / 2 + MapPosition.Y
        End Get
    End Property
End Class