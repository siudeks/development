﻿public Class Trackbar
    public Value As Single
    public Name As String
    private bounds As Rectangle
    private selectPosition As Rectangle
    private min As Single
    private max As Single
    private wholeNumber As Boolean
    private mousePressLoc As Point
    private scale As Single

    public Sub New(x As Integer, y As Integer, scale As Double, text As String, minValue As Single, maxValue As Single, Optional wholeNumber As Boolean = False)
        bounds = New Rectangle(x, y, 160 * scale, 32 * scale)
        selectPosition = New Rectangle(x, y, 32 * scale, 32 * scale)
        Name = text
        min = minValue
        max = maxValue
        Value = minValue
        Me.scale = scale
        Me.wholeNumber = wholeNumber
    End Sub

    public Sub Update()
        If Input.LeftPressed Then
            mousePressLoc = Input.MousePosition
        End If
        If Input.LeftDown And bounds.Contains(Input.MousePosition) And bounds.Contains(mousePressLoc) Then
            selectPosition.X = MathHelper.Clamp(Input.MousePosition.X - selectPosition.Width / 2, bounds.X, bounds.X + bounds.Width - selectPosition.Width)
            Value = (max - min) * (selectPosition.X - bounds.X) / (128 * scale) + min
            If wholeNumber Then
                Value = CInt(Value)
            Else
                Value = Math.Round(Value, 2)
            End If
        End If
        If bounds.Contains(Input.MousePosition) Then
            If Input.ScrollUp Then
                If wholeNumber Then
                    Value = MathHelper.Clamp(Value - 1, min, max)
                Else
                    Value = Math.Round(MathHelper.Clamp(Value - 0.01, min, max), 2)
                End If
            End If
            If Input.ScrollDown Then
                If wholeNumber Then
                    Value = MathHelper.Clamp(Value + 1, min, max)
                Else
                    Value = Math.Round(MathHelper.Clamp(Value + 0.01, min, max), 2)
                End If
            End If
        End If
        Dim findX As Single = Value - min
        findX = findX / (max - min)
        findX = (128 * scale) * findX
        findX += bounds.X
        selectPosition.X = CInt(findX)
    End Sub

    public Sub Draw()
        Globals.SpriteBatch.Draw(Textures.TrackBar, bounds, New Rectangle(0, 0, 160, 32), Color.White)
        Globals.SpriteBatch.Draw(Textures.TrackBar, selectPosition, New Rectangle(160, 0, 32, 32), Color.White)
        Globals.SpriteBatch.DrawString(Fonts.Microsoft_Sans_Serif_8, Name & ": " & Value, New Vector2(bounds.X + bounds.Width,
                CInt(bounds.Y + (bounds.Height / 2) - (Fonts.Microsoft_Sans_Serif_8.MeasureString(Name).Y / 2))), Color.White)
    End Sub
End Class