public Class Game1
    Inherits Microsoft.Xna.Framework.Game

    private screenManager As ScreenManager

    public Sub New()
        Globals.Graphics = New GraphicsDeviceManager(Me)
        Content.RootDirectory = "Content"
    End Sub

    Protected Overrides Sub Initialize()
        Me.IsMouseVisible = True
        Window.AllowUserResizing = True
        Globals.GameSize = New Vector2(800, 600)
        Globals.Graphics.PreferredBackBufferWidth = Globals.GameSize.X
        Globals.Graphics.PreferredBackBufferHeight = Globals.GameSize.Y
        Globals.Graphics.ApplyChanges()
        'Globals.BackBuffer = New RenderTarget2D(Globals.Graphics.GraphicsDevice, Globals.GameSize.X, Globals.GameSize.Y, False, SurfaceFormat.Color,
        '                                        DepthFormat.None, 0, RenderTargetUsage.PreserveContents)
        Globals.UserConnection = New UserConnection
        MyBase.Initialize()
    End Sub

    Protected Overrides Sub LoadContent()
        Globals.SpriteBatch = New SpriteBatch(GraphicsDevice)
        Globals.Content = Me.Content
        Textures.Load()
        Fonts.Load()
        screenManager = New ScreenManager
        'screenManager.AddScreen(New ParticleMaker)
        'screenManager.AddScreen(New MainMenu)
        'screenManager.AddScreen(New World)
        screenManager.AddScreen(New clsMainMenu)
        'screenManager.AddScreen(New World)
    End Sub

    Protected Overrides Sub Update(gameTime As GameTime)
        MyBase.Update(gameTime)
        Globals.WindowFocused = Me.IsActive
        Globals.GameTime = gameTime
        Input.Update()
        screenManager.Update()
    End Sub

    Protected Overrides Sub Draw(gameTime As GameTime)
        '  Globals.Graphics.GraphicsDevice.SetRenderTarget(Globals.BackBuffer)
        GraphicsDevice.Clear(Color.Black)
        MyBase.Draw(gameTime)
        screenManager.Draw()
        'Globals.Graphics.GraphicsDevice.SetRenderTarget(Nothing)
        'Globals.SpriteBatch.Begin()
        'Globals.SpriteBatch.Draw(Globals.BackBuffer, New Rectangle(0, 0, Globals.Graphics.GraphicsDevice.Viewport.Width,
        '                                                           Globals.Graphics.GraphicsDevice.Viewport.Height), Color.White)
        'Globals.SpriteBatch.End()
    End Sub
End Class