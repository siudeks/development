﻿public Enum ScreenState
    FadeIn
    FadeOut
    Active
    Shutdown
    Hidden
End Enum

public Class ScreenManager
    private static screens As New List(Of BaseScreen)
    private static newScreens As New List(Of BaseScreen)
    private debugScreen As New DebugInfo

    public Sub New()
        AddScreen(debugScreen)
    End Sub

    public Sub Update()
        Dim addRemoveScreens As Boolean = True
        For Each screen As BaseScreen In screens
            If screen.State = ScreenState.FadeOut Then
                addRemoveScreens = False
                Exit For
            End If
        Next
        If addRemoveScreens = True Then
            debugScreen.Screens = "Screens: "
            Dim removeScreens As New List(Of BaseScreen)
            For Each screen As BaseScreen In screens
                If screen.State = ScreenState.Shutdown Then
                    removeScreens.Add(screen)
                Else
                    debugScreen.Screens += screen.Name + ", "
                    screen.Focused = False
                End If
            Next
            For Each screen As BaseScreen In removeScreens
                screens.Remove(screen)
            Next
            For Each screen As BaseScreen In newScreens
                screens.Add(screen)
            Next
            newScreens.Clear()
            ' Remove Debug Screen and place it back on top
            screens.Remove(debugScreen)
            screens.Add(debugScreen)
            debugScreen.FocusScreen = "Focused Screen: "
            If screens.Count > 0 Then
                For i = screens.Count - 1 To 0 Step -1
                    If screens(i).GrabFocus Then
                        screens(i).Focused = True
                        debugScreen.FocusScreen = "Focused Screen: " + screens(i).Name
                        Exit For
                    End If
                Next
            End If
        End If
        For Each screen As BaseScreen In screens
            If Globals.WindowFocused Then
                screen.HandleInput()
            End If
            screen.Update()
        Next
    End Sub

    public Sub Draw()
        For Each screen As BaseScreen In screens
            If screen.State <> ScreenState.Hidden And screen.State <> ScreenState.Shutdown Then
                screen.Draw()
            End If
        Next
    End Sub

    public static Sub AddScreen(screen As BaseScreen)
        newScreens.Add(screen)
    End Sub

    public static Sub UnloadScreen(screenName As String)
        For Each foundScreen As BaseScreen In screens
            If foundScreen.Name = screenName Then
                foundScreen.Unload()
                Exit For
            End If
        Next
    End Sub
End Class