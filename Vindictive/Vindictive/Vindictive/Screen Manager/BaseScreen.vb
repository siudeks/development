﻿public MustInherit Class BaseScreen
    public Name As String = ""
    public State As ScreenState = ScreenState.FadeIn
    public FadeTime As Double
    public Alpha As Single
    public Position As Single
    public TransitionTime As Integer = 750
    public Focused As Boolean = False
    public GrabFocus As Boolean = True

    public Overridable Sub HandleInput()
    End Sub

    public Overridable Sub Update()
        Transition()
    End Sub

    public Overridable Sub Draw()
    End Sub

    public Overridable Sub Unload()
        State = ScreenState.FadeOut
    End Sub

    Protected Sub Transition()
        If State = ScreenState.FadeIn Then
            If FadeTime < TransitionTime Then
                FadeTime += Globals.GameTime.ElapsedGameTime.TotalMilliseconds
                Alpha = MathHelper.Clamp(FadeTime / TransitionTime, 0, 1)
                Position = Alpha
            Else
                Position = 1
                Alpha = 1
                FadeTime = 0
                State = ScreenState.Active
            End If
        ElseIf State = ScreenState.FadeOut Then
            If FadeTime < TransitionTime Then
                FadeTime += Globals.GameTime.ElapsedGameTime.TotalMilliseconds
                Alpha = MathHelper.Clamp(1 - FadeTime / TransitionTime, 0, 1)
                Position = MathHelper.Clamp(FadeTime / TransitionTime + 1, 0, 2)
            Else
                Position = 2
                Alpha = 0
                FadeTime = 0
                State = ScreenState.Shutdown
            End If
        ElseIf State = ScreenState.Shutdown Then
            Return
        End If
    End Sub
End Class