﻿Imports System.Net.Sockets
Imports System.Text

public Class UserConnection

    public Event ConnectionStatus(status As String)
    public Event Connected()
    public Event Disconnected()
    public Event LineReceived(packetID As Byte, packetData As String)

    public PacketHandler As PacketHandler
    public UserName As String = ""

    private client As TcpClient
    private readBuffer(1024) As Byte
    private packetBuffer As New StringBuilder

    public Sub New()
        PacketHandler = New PacketHandler(Me)
    End Sub

    public Sub Connect(user As String)
        UserName = user
        RaiseEvent ConnectionStatus("Connecting...")
        client = New TcpClient
        client.BeginConnect("127.0.0.1", 8191, AddressOf StartConnection, Nothing)
        'client.BeginConnect("127.0.0.1", 8191, AddressOf Connected, Nothing)
    End Sub

    public ReadOnly Property isConnected As Boolean
        Get
            If client Is Nothing Then return false;
            Return client.Connected
        End Get
    End Property

    private Sub StartConnection(ar As IAsyncResult)
        Try
            client.EndConnect(ar)
            client.GetStream.BeginRead(readBuffer, 0, readBuffer.Length, AddressOf ReadStream, Nothing)
            RaiseEvent ConnectionStatus("Connected")
        Catch ex As Exception
            RaiseEvent ConnectionStatus("Unable to connect")
        End Try
    End Sub

    private Sub ReadStream(ar As IAsyncResult)
        Dim numBytes As Integer
        Try
            SyncLock client.GetStream
                numBytes = client.GetStream.EndRead(ar)
            End SyncLock
        Catch ex As Exception
            'RaiseEvent Disconnected(Me)
            Return
        End Try

        If numBytes > 0 Then
            Dim uEncode As New UTF8Encoding
            Dim data As String = uEncode.GetString(readBuffer, 0, numBytes)
            packetBuffer.Append(data)
        Else
            'RaiseEvent Disconnected(Me)
            Return
        End If

        Do While packetBuffer.ToString.Contains("-")
            Dim endOfPacket As Integer = packetBuffer.ToString.IndexOf("-")
            Dim packetData As String = packetBuffer.ToString.Substring(0, endOfPacket)
            packetBuffer.Remove(0, endOfPacket + 1)
            ParsePacket(packetData)
        Loop

        Try
            SyncLock client.GetStream
                client.GetStream.BeginRead(readBuffer, 0, readBuffer.Length, AddressOf ReadStream, Nothing)
            End SyncLock
        Catch e As Exception
            'RaiseEvent Disconnected(Me)
        End Try
    End Sub

    public Sub Send(packetID As Byte, packetData As String)
        SyncLock client.GetStream
            Dim dataArray() As Byte = System.Text.Encoding.UTF8.GetBytes(packetData)
            Dim byteArray(dataArray.Length) As Byte
            byteArray(0) = packetID
            dataArray.CopyTo(byteArray, 1)
            Dim loadedPacket As String = Convert.ToBase64String(byteArray)

            Dim writer As New IO.StreamWriter(client.GetStream)
            writer.Write(loadedPacket & "-")
            writer.Flush()
        End SyncLock
    End Sub

    public Sub CloseConnection()
        client.Close()
    End Sub

    public Sub ParsePacket(packet As String)
        Dim dataBytes() As Byte = Convert.FromBase64String(packet)
        Dim newPacket As NetworkPacket
        newPacket.PacketID = dataBytes(0)

        If newPacket.PacketID = PacketID.MapData Then
            Dim dArray(dataBytes.Length - 2) As Byte
            Array.ConstrainedCopy(dataBytes, 1, dArray, 0, dataBytes.Length - 1)

            Dim pArray() As Byte = Globals.Decompress(dArray)
            newPacket.PacketData = System.Text.Encoding.UTF8.GetString(pArray, 0, pArray.Length - 1)
        Else
            newPacket.PacketData = System.Text.Encoding.UTF8.GetString(dataBytes, 1, dataBytes.Length - 1)
        End If
        RaiseEvent LineReceived(newPacket.PacketID, newPacket.PacketData)
    End Sub
End Class


public Enum PacketID As Byte
    Disconnect = 0
    Connect = 1
    ConnectedUsers = 2
    Broadcast = 3
    Location = 50
    MapData = 100
    TileData = 101
    Ping = 255
End Enum

public Structure NetworkPacket
    public PacketID As PacketID
    public PacketData As String
End Structure
