﻿public Class PacketHandler


    public Event Disconnect(packetData As String)
    public Event Connect(packetData As String)
    public Event ConnectedUsers(packetData As String)
    public Event Broadcast(packetData As String)
    public Event Location(packetData As String)
    public Event MapData(packetData As String)
    public Event TileData(packetData As String)
    public Event Ping(packetData As String)

    public Sub New(user As UserConnection)
        AddHandler user.LineReceived, AddressOf IncomingPacket
    End Sub

    public Sub IncomingPacket(ByVal id As Byte, data As String)
        Select Case id
            Case PacketID.Disconnect
                RaiseEvent Disconnect(data)
            Case PacketID.Connect
                RaiseEvent Connect(data)
            Case PacketID.ConnectedUsers
                RaiseEvent ConnectedUsers(data)
            Case PacketID.Broadcast
                RaiseEvent Broadcast(data)
            Case PacketID.Location
                RaiseEvent Location(data)
            Case PacketID.MapData
                RaiseEvent MapData(data)
            Case PacketID.TileData
                RaiseEvent TileData(data)
            Case PacketID.Ping
                RaiseEvent Ping(data)
        End Select
    End Sub

End Class
