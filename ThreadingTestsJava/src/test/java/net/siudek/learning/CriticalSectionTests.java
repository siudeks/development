package net.siudek.learning;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

public class CriticalSectionTests {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void ShouldNotWaitWithoutGettingMonitor() throws InterruptedException {
        Object lock = new Object();
        exception.expect(IllegalMonitorStateException.class);
        lock.wait();
    }

    @Test(timeout = 100)
    public void ShouldWaitOnMonitor() throws Exception {
        final Object locker = new Object();
        Callable callable = new Callable() {

            @Override
            public Object call() throws Exception {
                synchronized (locker) {
                    locker.notify();
                }
                return null;
            }
        };

        synchronized (locker) {
            Executors.newFixedThreadPool(1).submit(callable);
            locker.wait();
        }
    }

}
