package net.siudek.learning;

import org.junit.Test;

public class CancellationTests {

	@Test(timeout = 100)
	public void ShouldCancelWorkingThread() throws InterruptedException {
		Thread thread = new Thread(new SpinningLogic());
		thread.start();

		thread.interrupt();
		thread.join();
	}

	class SpinningLogic implements Runnable {

		public void run() {
			long initial = System.nanoTime();
			long expectedInNano = initial + 1 * 1000 * 1000 * 1000; // 1 sec later.
			while (System.nanoTime() < expectedInNano) {
				if (Thread.interrupted()) {
					Thread.currentThread().interrupt();
					return;
				}
			}
		}
	}

	@Test(timeout = 100)
	public void ShouldCancelWaitingActivity() throws InterruptedException {
		Thread thread = new Thread(new WaitingLogic());
		thread.start();

		thread.interrupt();
		thread.join();
	}

	class WaitingLogic implements Runnable {

		public void run() {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}

}
