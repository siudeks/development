package net.siudek.learning;

import org.junit.Assert;
import org.junit.Test;


public final class DeadLockTests {
	
    @Test
    public void ShouldLockThread() throws InterruptedException
    {
        Object syncRoot1 = new Object();
        Object syncRoot2 = new Object();
        
        Thread thread1 = new Thread(new Logic(syncRoot1, syncRoot2));
        Thread thread2 = new Thread(new Logic(syncRoot2, syncRoot1));

        thread1.start();
        thread2.start();

        thread1.join(100);
        Assert.assertTrue(thread1.isAlive());
    }

    final class Logic implements Runnable {
    	private final Object obj1;
    	private final Object obj2;
    	public Logic(Object obj1, Object obj2){
    		this.obj1 = obj1;
    		this.obj2 = obj2;
    	}
    	
		public void run() {
			DoubleLock();
		}

	    private void DoubleLock()
	    {
	        synchronized (obj1)
	        {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				synchronized (obj2)
	            {
	            }
	        }
	    }
}

}
