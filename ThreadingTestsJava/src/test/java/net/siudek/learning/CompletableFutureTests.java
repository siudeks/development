package net.siudek.learning;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

public class CompletableFutureTests {
  @Test
  public void shouldContinue() throws InterruptedException, ExecutionException {
    CompletableFuture<Integer> promise = CompletableFuture.supplyAsync(() -> 1);
    assertThat(promise.get(), equalTo(1));
  }
}