﻿using Microsoft.Owin.Hosting;
using System;
using Owin;
using System.Net.Http;

namespace OwinTests
{
    public class Program
    {
        static void Main(string[] args)
        {
            var options = new StartOptions { };
            options.Urls.Add("localhost");
            using (var server = WebApp.Start(options, Run))
            {
                var client = new HttpClient();
                var result = await client.GetAsync("http://localhost");
                Console.WriteLine(result)
                Console.ReadLine();
            }
        }

        private static void Run(IAppBuilder app)
        {
            app.Run(context =>
            {
                context.Response.ContentType = "text/plain";
                return context.Response.WriteAsync("Hello, world.");
            });
        }

        private stati
    }
}
