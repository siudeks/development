﻿using Microsoft.Owin.Testing;
using NUnit.Framework;
using Owin;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using System;
using Microsoft.Owin.Hosting;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Hubs;
using System.Collections.Generic;

namespace OwinTests.Push
{
    [TestFixture]
    public sealed class SignalRShould
    {
        public class Stock
        {
            public string Symbol { get; set; }
            public decimal Price { get; set; }
        }

        [Test]
        public async Task PushMessage()
        {
            using (var server = TestServer.Create(Build))
            {
                var response = await server.HttpClient.GetAsync("/");

                var hubConnection = new HubConnection(server.BaseAddress.ToString());
                hubConnection.TraceLevel = TraceLevels.All;
                hubConnection.TraceWriter = Console.Out;
                var stockTickerHubProxy = hubConnection.CreateHubProxy("StockTickerHub");
                stockTickerHubProxy.On<Stock>("UpdateStockPrice", stock => Console.WriteLine("Stock update for {0} new price {1}", stock.Symbol, stock.Price));
                await hubConnection.Start();

                Assert.That(await response.Content.ReadAsStringAsync(), Is.EqualTo("Hello, world."));
            }
        }

        private static void Build(IAppBuilder app)
        {
            //app.UseCors(CorsOptions.AllowAll);

            var hubConfiguration = new HubConfiguration();
            hubConfiguration.EnableDetailedErrors = true;
            hubConfiguration.Resolver.Register(typeof(StockTickerHub), () => new StockTickerHub());
            app.MapSignalR(hubConfiguration);


            app.Run(context =>
            {
                context.Response.ContentType = "text/plain";
                return context.Response.WriteAsync("Hello, world.");
            });
        }

        [HubName("StockTickerHub")]
        public class StockTickerHub : Hub
        {
            public StockTickerHub()
            {

            }
            public override Task OnConnected()
            {
                return base.OnConnected();
            }
        }

        class Resolver : IDependencyResolver
        {
            public Resolver()
            {

            }
            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public object GetService(Type serviceType)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<object> GetServices(Type serviceType)
            {
                throw new NotImplementedException();
            }

            public void Register(Type serviceType, IEnumerable<Func<object>> activators)
            {
                throw new NotImplementedException();
            }

            public void Register(Type serviceType, Func<object> activator)
            {
                throw new NotImplementedException();
            }
        }
    }
}
