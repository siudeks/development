﻿namespace Enumerables
{
    using NUnit.Framework;
    using System.Collections.Generic;

    [TestFixture]
    public sealed class EnumeratorTests
    {
        [Test]
        public void ShouldCreateDefferenEnumerators()
        {
            var enumerator1 = GetEnumerable().GetEnumerator();
            var enumerator2 = GetEnumerable().GetEnumerator();

            Assert.That(enumerator1, Is.Not.SameAs(enumerator2));
        }

        private IEnumerable<int> GetEnumerable()
        {
            yield break;
        }
    }
}
