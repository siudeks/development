﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntellisenseDemo.Extensions
{
    public static class SugesstionExtension
    {
        public static IEnumerable<string> Suggestions<T>(this T user) where T : class
        {
            var query = from p in user.GetType().GetProperties()
                        select p.Name;
            return query.AsEnumerable();
        }
    }
}
