/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package contracts;

/**
 *
 * @author ssiudek
 */
public class Customer {
  private PersonModel name;

    /**
     * @return the name
     */
    public PersonModel getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(PersonModel name) {
        this.name = name;
    }
}