/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package contracts;

/**
 *
 * @author ssiudek
 */
public class OrderDTO {
  private String customerFirstName;
  private String customerLastName;
  private String billingStreet;
  private String billingCity;

    /**
     * @return the customerFirstName
     */
    public String getCustomerFirstName() {
        return customerFirstName;
    }

    /**
     * @param customerFirstName the customerFirstName to set
     */
    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    /**
     * @return the customerLastName
     */
    public String getCustomerLastName() {
        return customerLastName;
    }

    /**
     * @param customerLastName the customerLastName to set
     */
    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    /**
     * @return the billingStreet
     */
    public String getBillingStreet() {
        return billingStreet;
    }

    /**
     * @param billingStreet the billingStreet to set
     */
    public void setBillingStreet(String billingStreet) {
        this.billingStreet = billingStreet;
    }

    /**
     * @return the billingCity
     */
    public String getBillingCity() {
        return billingCity;
    }

    /**
     * @param billingCity the billingCity to set
     */
    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }
}