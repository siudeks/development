/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contracts;

import static org.junit.Assert.*;
import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;

/**
 *
 * @author ssiudek
 */
public class MapPropertyTests {

    @Test
    public void shouldMapProperties() {
        PersonModel source = new PersonModel();
        source.setFirstName("Harry");
        source.setLastName("Potter");
        ModelMapper mapper = new ModelMapper();
        mapper.addMappings(
                new PropertyMap<PersonModel, PersonModel>() {
                    protected void configure() {
                        map().setFirstName(source.getFirstName());
                        map().setLastName(source.getLastName());
                    }
                });
        PersonModel target = mapper.map(source, PersonModel.class);

        assertTrue(target.getFirstName().equals(source.getFirstName()));
        assertTrue(target.getLastName().equals(source.getLastName()));
        assertNotSame(target, source);
    }

}
