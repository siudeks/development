package net.siudek.learning.threading;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

public class FutureTasks {

    @Test(timeout = 100)
    public void canTestFutureWhenCancelled() throws InterruptedException {
        CompletableFuture<Boolean> promise = new CompletableFuture<>();

        promise.cancel(false);
        assertThat(promise.isCompletedExceptionally(), is(true));
        assertThat(promise.isCancelled(), is(true));
        assertThat(promise.isDone(), is(true));
    }

    @Test(timeout = 100)
    public void canTestFutureWhenDone() throws InterruptedException, ExecutionException {
        CompletableFuture<Boolean> promise = new CompletableFuture<>();

        promise.complete(Boolean.TRUE);

        assertThat(promise.isCompletedExceptionally(), is(false));
        assertThat(promise.isCancelled(), is(false));
        assertThat(promise.isDone(), is(true));
        assertThat(promise.get(), is(true));
    }

    @Test(timeout = 100)
    public void canTestFutureWhenProgress() throws InterruptedException, ExecutionException {
        CompletableFuture<Boolean> promise = new CompletableFuture<>();

        assertThat(promise.isCompletedExceptionally(), is(false));
        assertThat(promise.isCancelled(), is(false));
        assertThat(promise.isDone(), is(false));
    }

    @Test(timeout = 100)
    public void shouldHandleFutureResponse() throws InterruptedException, ExecutionException {
        CountDownLatch waitHandle = new CountDownLatch(1);
        CompletableFuture<Boolean> promise = new CompletableFuture<>();
        promise.thenAcceptAsync(o -> {
                waitHandle.countDown();
        });
        promise.complete(Boolean.TRUE);
        waitHandle.await();

        assertThat(promise.get(), is(true));
    }

}
