package net.siudek.learning.streams;

import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.Assert;
import org.junit.Test;

public class CustomIterableTest {
  @Test
  public void shouldReturnKnownDays(){
    LocalDate now = LocalDate.of(2000, 1, 1);
    Iterable<LocalDate> iterable = new CustomIterable(now);
    
    Stream<LocalDate> stream = StreamSupport.stream(iterable.spliterator(),false);
    Optional<LocalDate> r = stream
      .limit(1000000000)
      .findFirst();
    
    Assert.assertThat(r.isPresent(), equalTo(true));
  }
}
