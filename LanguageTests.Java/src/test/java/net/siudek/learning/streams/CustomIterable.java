package net.siudek.learning.streams;

import java.time.LocalDate;
import java.util.Iterator;

public final class CustomIterable implements Iterable<LocalDate> {

  private final LocalDate now;
  public CustomIterable (LocalDate now) {
    this.now = now;
  }
  
  @Override
  public Iterator<LocalDate> iterator() {
    return new CustomIterator(now);
  }
  
  private static class CustomIterator implements Iterator<LocalDate> {

    private final LocalDate current;
    public CustomIterator(LocalDate start) {
      this.current = nextItem(start.minusDays(1));
    }
    
    @Override
    public boolean hasNext() {
      return true;
    }

    @Override
    public LocalDate next() {
      return nextItem(current);
    }
    
    /**
     * Returns next date which fulfill requirements of iteration.
     * @param current
     * @return
     */
    private static LocalDate nextItem(LocalDate current) {
      while (true) {
        current = current.plusDays(1);
        switch (current.getDayOfWeek()) {
          case FRIDAY:
          case TUESDAY:
            return current;
          default:
        }
      }
    }
    
  }

}
