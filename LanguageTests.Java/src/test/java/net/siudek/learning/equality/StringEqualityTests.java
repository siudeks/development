package net.siudek.learning.equality;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StringEqualityTests {

    @Test
    public void shouldBeEqual() {
        String str1 = "New string";
        String str2 = "New string";
        boolean areEqualByMethod = str1.equals(str2);
        boolean areEqualByOperator = str1 == str2;

        assertTrue(areEqualByMethod);
        assertTrue(areEqualByOperator);
    }

    @Test
    public void shouldNotBeEqual() {
        String str1 = "NewString";
        String str2a = "New";
        String str2b = "String";
        boolean areEqualByMethod = str1.equals(str2a + str2b);
        boolean areEqualByOperator = str1 == str2a + str2b;

        assertTrue(areEqualByMethod);
        assertFalse(areEqualByOperator);
    }
}
