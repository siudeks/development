package net.siudek.learning.equality;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import org.junit.Test;

public class OptionEquality {
    @Test
    public void shouldBeEqualWhenEmpty(){
        Optional<Void> empty1 = Optional.empty();
        Optional<Void> empty2 = Optional.empty();

        assertThat(empty1, sameInstance(empty2));
    }

    @Test
    public void shouldBeEqualWhenTheSameValue(){
        Optional<Integer> value1 = Optional.of(1);
        Optional<Integer> value2 = Optional.of(1);

//        value1.ifPresent();
        assertThat(value1, equalTo(value2));
        assertThat(value1.get(), equalTo(value2.get()));
        assertThat(value1, sameInstance(value2));
    }
}
