package a.b.c

/**
 * Created by slawomir.siudek on 2015-03-18.
 */
class JavaClassTest extends spock.lang.Specification {
    def "Should instaniate Java class" () {
        when: "create and instance"
        def instance = new JavaClass()
        then:
        instance != null
    }
}
