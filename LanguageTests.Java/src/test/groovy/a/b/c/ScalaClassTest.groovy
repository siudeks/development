package a.b.c

/**
 * Created by slawomir.siudek on 2015-03-18.
 */
class ScalaClassTest extends spock.lang.Specification {
    def "should instaniate Scala class" () {
    when:
    def instance = new ScalaClass()
    then:
    instance != null
    }
}
