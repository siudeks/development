package servlet;

import java.util.Enumeration;

import java.io.PrintWriter;
import java.io.IOException;
import java.io.ByteArrayOutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.JMSException;
import javax.jms.QueueSession;
import javax.jms.QueueBrowser;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;

@WebServlet("/Browser")
public class Browser extends HttpServlet {
	private String _message;
	private QueueSession _session;

	public void init() {
		try {
			QueueConnectionFactory connFactory = new ActiveMQConnectionFactory();
			QueueConnection queueConn = connFactory.createQueueConnection();
			_session = queueConn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			queueConn.start();
		} catch (Exception ex) {
			_message = ex2str(ex);
		}
	}

	public static String ex2str(Throwable t) {
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			PrintWriter pw = new PrintWriter(os);
			t.printStackTrace(pw);
			pw.flush();
			return new String(os.toByteArray());
		} catch (Throwable e) {
			return t.toString();
		}
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();

		writer.println("<html>");
		writer.println("<head>");
		writer.println("<title>JMS Servlet</title>");
		writer.println("</head>");
		writer.println("<body bgcolor=white>");

		writer.println("<h1>Messages on queue/queue0</h1>");

		if (_session == null) {
			writer.println(_message);
		} else {
			try {
				Queue queue = _session.createQueue("queue0");
				QueueBrowser browser = _session.createBrowser(queue);
				Enumeration _iterator = browser.getEnumeration();
				writer.println("<table>");
				while (_iterator.hasMoreElements()) {
					Message msg = (Message) _iterator.nextElement();
					writer.println("<tr><td>" + msg + "</td></tr>");
				}
				browser.close();
				writer.println("</table>");
			} catch (JMSException ex) {
				writer.println(ex.toString());
			}
		}

		writer.println("</body>");
		writer.println("</html>");
	}
}