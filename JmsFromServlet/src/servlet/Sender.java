package servlet;
                                                                           
import java.io.PrintWriter;
import java.io.Reader;
import java.io.IOException;
                                                                           
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
                                                                           
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.JMSException;
import javax.jms.QueueSession;
import javax.jms.QueueSender;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
                                                                           
import org.apache.activemq.ActiveMQConnectionFactory;
                                                                           
@WebServlet("/Sender")
public class Sender extends HttpServlet
{
	private String _message;
	private QueueSender  _sender;
    private QueueSession _session;
                                                                           
    public void init()
    {
       try
       {
          QueueConnectionFactory connFactory = new ActiveMQConnectionFactory();
          QueueConnection queueConn = connFactory.createQueueConnection();
          _session = queueConn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
          Queue queue = _session.createQueue("queue0");
          _sender = _session.createSender(queue);
          queueConn.start();
       }
       catch (Exception ex)
       {
    	   _message = Receiver.ex2str(ex);
       }
    }
                                                                           
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
       response.setContentType("text/html");
       PrintWriter writer = response.getWriter();
                                                                          
       writer.println("<html>");
       writer.println("<head>");
       writer.println("<title>JMS Servlet</title>");
       writer.println("</head>");
       writer.println("<body bgcolor=white>");
                                                                          
       if (_message == null)
           writer.println("I only speak POST");
       else
           writer.println(_message);
                                                                          
       writer.println("</body>");
       writer.println("</html>");
    }
                                                                           
    public void doPost(HttpServletRequest request,
        HttpServletResponse response)
        throws IOException, ServletException
    {
       response.setContentType("text/html");
       PrintWriter writer = response.getWriter();
                                                                          
       writer.println("<html>");
       writer.println("<head>");
       writer.println("<title>JMS Servlet</title>");
       writer.println("</head>");
       writer.println("<body bgcolor=white>");
                                                                          
       writer.println("<h1>Status</h1>");
                                                                          
       if (_sender == null) {
          writer.println(_message);
       } else {
          try {
             Reader reader = request.getReader();
             String text = "";
             while (true) {
                int i = reader.read();
                if (i == -1) break;
                text += (char) i;
             }
                                                                        
             int i1 = text.indexOf("=");
             int i2 = text.indexOf("&");
                                                                        
             text = text.substring(i1 == -1 ? 0 : i1+1, i2 == -1 ?
                 text.length() : i2);
                                                                        
             TextMessage msg;
             msg = _session.createTextMessage(text);
             _sender.send(msg);
             writer.println("Just sent " + msg.getText());
          } catch (JMSException ex) {
             writer.println(ex.toString());
          }
       }
    }
}