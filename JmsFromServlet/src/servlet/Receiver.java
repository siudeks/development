package servlet;

import java.io.PrintWriter;
import java.io.IOException;
import java.io.ByteArrayOutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.JMSException;
import javax.jms.QueueSession;
import javax.jms.QueueReceiver;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;

@WebServlet("/Receiver")
public class Receiver extends HttpServlet {

	private String _message;
	private QueueReceiver _receiver;

	public void init() {
		try {
			QueueConnectionFactory connFactory = new ActiveMQConnectionFactory();
			QueueConnection queueConn = connFactory.createQueueConnection();
			QueueSession session = queueConn.createQueueSession(false,
					Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue("queue0");
			_receiver = session.createReceiver(queue);
			queueConn.start();
		} catch (Exception ex) {
			_message = ex2str(ex);
		}
	}

	public static String ex2str(Throwable t) {
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			PrintWriter pw = new PrintWriter(os);
			t.printStackTrace(pw);
			pw.flush();
			return new String(os.toByteArray());
		} catch (Throwable e) {
			return t.toString();
		}
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();

		writer.println("<html>");
		writer.println("<head>");
		writer.println("<title>JMS Servlet</title>");
		writer.println("</head>");
		writer.println("<body bgcolor=white>");

		writer.println("<h1>Message on queue/queue0</h1>");

		if (_receiver == null) {
			writer.println(_message);
		} else {
			try {
				Message msg = _receiver.receiveNoWait();
				if (msg != null) {
					if (msg instanceof TextMessage) {
						writer.println(((TextMessage) msg).getText());
					} else {
						writer.println("not text message");
					}
				} else {
					writer.println("no message on queue");
				}
			} catch (JMSException ex) {
				writer.println(ex.toString());
			}
		}

		writer.println("<h1>Send New Message</h1>");

		writer.print("<FORM method=POST ");
		writer.println("action=\"http://localhost:8080/services/sender\">");
		writer.print("<input type=text ");
		writer.println("name=send size=20 maxlength=\"800\">");
		writer.println("<input type=\"submit\" name=post value=\"send\">");
		writer.println("</FORM>");

		writer.println("</body>");
		writer.println("</html>");
	}
}