﻿namespace TransactionTests
{
    using NUnit.Framework;
    using System;
    using System.Transactions;

    [TestFixture]
    public sealed class ImplicitTransactionTests
    {
        [Test]
        public void ShouldCommitTransaction1()
        {
            var operation = new Operation();

            using (var scope = new TransactionScope())
            {
                Transaction.Current.EnlistVolatile(operation, EnlistmentOptions.None);
                scope.Complete();
            }
        }
    }

    class Operation : IEnlistmentNotification
    {
        public void Commit(Enlistment enlistment)
        {
            enlistment.Done();
        }

        public void InDoubt(Enlistment enlistment)
        {
            //enlistment.Done();
        }

        public void Prepare(PreparingEnlistment preparingEnlistment)
        {
            //preparingEnlistment.ForceRollback();
        }

        public void Rollback(Enlistment enlistment)
        {
            //enlistment.Done();
        }
    }
}
