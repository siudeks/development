﻿using System;
using System.ServiceModel;

namespace Authorization.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var service = new ServiceHost(typeof(MyService)))
            {
                service.Open();
                Console.WriteLine("Press ENTER to exit");
                Console.ReadLine();
            }
        }
    }
}
