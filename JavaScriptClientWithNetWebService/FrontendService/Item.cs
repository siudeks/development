﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FrontendService
{
    [DataContract]
    public class Item : IEquatable<Item>
    {
        [DataMember(Name = "name")]
        public string Name;

        public bool Equals(Item other)
        {
            return Name == other.Name;
        }
    }
}
