﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FrontendService
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = new ServiceHost(new SessionService());
            host.Open();

            Console.WriteLine("The service is ready at {0}",
            host.Description.Endpoints[0].ListenUri);
            Console.WriteLine("Press <Enter> to stop the service.");
            Console.ReadLine();

            //Close the ServiceHost.
            host.Close();
        }
    }
}
