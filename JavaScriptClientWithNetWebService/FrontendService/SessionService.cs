﻿using System;
using System.Collections.Concurrent;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Linq;
using System.Collections.Generic;

namespace FrontendService
{
    [ServiceBehavior(ConcurrencyMode=ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.Single)]
    public sealed class SessionService : ISessionService
    {
        private ConcurrentBag<Item> _items = new ConcurrentBag<Item>();

        [WebGet(UriTemplate = "/items", ResponseFormat = WebMessageFormat.Json)]
        public Items GetItems()
        {
            var items = new Items();
            items.AddRange(_items);

            return items;
        }

        [WebInvoke(UriTemplate = "/items", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        public Item AddNewUser()
        {
            var item = new Item 
            {
                Name = Guid.NewGuid().ToString()
            };
            _items.Add(item);
            return item;
        }
    }
}
