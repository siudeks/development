﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FrontendService
{
    [CollectionDataContract(Name = "users", Namespace = "")]
    public class Items : List<Item>
    {
    }
}
