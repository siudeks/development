﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace FrontendService
{
    
    [ServiceContract]
    public interface ISessionService
    {
        [OperationContract]
        Items GetItems();

        [OperationContract]
        Item AddNewUser();
    }
}
