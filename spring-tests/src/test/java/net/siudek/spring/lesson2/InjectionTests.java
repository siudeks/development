package net.siudek.spring.lesson2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"config.xml", "mock-config.xml"})
public class InjectionTests {

    @Inject
    BeanWithDependency bean;

    @Inject
    MockRepo mockRepo;

    @Test
    public void shouldInjectDependency(){
        expect(mockRepo.singleDependency.process(1))
                .andReturn(2);

        replay(mockRepo.singleDependency);

        assertThat(bean.process(1), is(2));
    }
}
