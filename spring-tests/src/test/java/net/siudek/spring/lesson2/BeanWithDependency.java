package net.siudek.spring.lesson2;

public class BeanWithDependency {
    public int process(int entry) {
        return singleDependency.process(entry);
    }

    private SingleDependency singleDependency;
    public void setSingleDependency(SingleDependency singleDependency) {
        this.singleDependency = singleDependency;
    }
}
