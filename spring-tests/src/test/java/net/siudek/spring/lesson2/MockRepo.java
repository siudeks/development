package net.siudek.spring.lesson2;

import org.springframework.context.annotation.Bean;

import static org.easymock.EasyMock.createMock;

public class MockRepo {

    public final SingleDependency singleDependency;

    public MockRepo() {
        singleDependency = createMock(SingleDependency.class);
    }

    @Bean(name="someDependency")
    public SingleDependency getSingleDependency(){
        return singleDependency;
    }


}
