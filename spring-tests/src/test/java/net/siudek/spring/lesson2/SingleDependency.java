package net.siudek.spring.lesson2;

public interface SingleDependency {
    int process(int entry);
}
