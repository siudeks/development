package net.siudek.spring.lesson1;

import org.springframework.stereotype.Component;

@Component
public class StrategySecond implements CustomServiceStrategy<Integer> {
    @Override
    public Integer process(Integer entry) {
        return entry * 10;
    }
}
