package net.siudek.spring.lesson1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class CustomServiceImpl implements CustomService {
    @Autowired
    private Collection<CustomServiceStrategy<Integer>> strategies;

    public Integer process(Integer entry) {
        Integer result = entry;
        for (CustomServiceStrategy<Integer> strategy : strategies)
            result = strategy.process(result);

        return result;
    }
}
