package net.siudek.spring.lesson1;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "config.xml")
public class DependencyTest {

    @Autowired
    private CustomService bean1;

    @Test
    public final void ShouldInjectImplementations() {
        assertThat(bean1.process(1), is(110));
    }
}
