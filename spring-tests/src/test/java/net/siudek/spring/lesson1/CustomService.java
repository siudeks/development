package net.siudek.spring.lesson1;

public interface CustomService {
    Integer process(Integer entry);
}
