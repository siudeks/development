package net.siudek.spring.lesson1;

public interface CustomServiceStrategy<T> {
    T process(T entry);
}
