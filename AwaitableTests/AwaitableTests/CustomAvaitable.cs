﻿namespace Awaitables
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public sealed class CustomAvaitable
    {
        public MyAwaiter GetAwaiter()
        {
            return new MyAwaiter();
        }
    }

    public class MyAwaiter : INotifyCompletion
    {
        public bool IsCompleted { get; private set; }

        public MyAwaiter()
        {
        }
        public void OnCompleted(Action continuation)
        {
            ThreadPool.QueueUserWorkItem(o => continuation());
        }

        public int GetResult()
        {
            return 3;
        }
    }
}
