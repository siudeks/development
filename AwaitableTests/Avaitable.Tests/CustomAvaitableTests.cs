﻿namespace Awaitables
{
    using System;
    using System.Threading;
    using NUnit.Framework;

    [TestFixture]
    public class CustomAvaitableTests
    {
        [Test]
        public async void ShouldInvokeAsynchronously()
        {
            var threadId = Thread.CurrentThread.ManagedThreadId;
            var custom = new CustomAvaitable();
            var result = await custom;
            var threadIdAfterAwait = Thread.CurrentThread.ManagedThreadId;

            Assert.AreNotEqual(threadId, threadIdAfterAwait);
            Assert.That(result, Is.EqualTo(3));
        }
    }
}
