﻿using System;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Input;

namespace JoinDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var downs = Observable.FromEventPattern<MouseEventArgs>(this, "MouseDown");
            var ups = Observable.FromEventPattern<MouseEventArgs>(this, "MouseUp");
            var allMovies = Observable.FromEventPattern<MouseEventArgs>(this, "MouseMove");

            var dragPositions = downs.Join(
                allMovies,
                down => ups,
                move => allMovies,
                (StylusDownEventArgs, move) => move.EventArgs.GetPosition(this));
            
            dragPositions
                .Subscribe(o => Title = String.Format("Down x:{0} y:{1}", o.X, o.Y));
        }
    }
}
