﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;

namespace Net.Siudek
{
    [TestFixture]
    public sealed class SingleAssigmentDisposableTests
    {
        [Test]
        public void ShouldDisposeUnderlyingItem()
        {
            var disposable = Substitute.For<IDisposable>();
            var disposer = new SingleAssignmentDisposable();
            disposer.Disposable = disposable;

            disposable.DidNotReceive().Dispose();

            disposer.Dispose();
            disposable.Received().Dispose();
        }

        [Test]
        public void ShouldDisposeUnderlyingItemInLazyManner()
        {
            var disposer = new SingleAssignmentDisposable();
            disposer.Dispose();

            var disposable = Substitute.For<IDisposable>();
            disposer.Disposable = disposable;

            disposable.Received().Dispose();
        }

        [Test]
        public void ShouldNotAllowChangeUnderlyingDisposable()
        {
            var disposer = new SingleAssignmentDisposable();

            disposer.Disposable = Substitute.For<IDisposable>();
            Assert.Throws<InvalidOperationException>(() => { disposer.Disposable = Substitute.For<IDisposable>(); });
        }
    }
}
