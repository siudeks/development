﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace Net.Siudek
{
    [TestFixture]
    public sealed class SwitchTests
    {
        [Test]
        public void ShouldSwitchToNext()
        {
            var uberSource = new Subject<IObservable<int>>();
            var source1 = new Subject<int>();
            var source2 = new Subject<int>();

            var result = new List<int>();
            var source = uberSource.Switch();
            source.ObserveOn(Scheduler.Immediate).Subscribe(o => result.Add(o));

            uberSource.OnNext(source1);
            source1.OnNext(1);

            uberSource.OnNext(source2);
            source1.OnNext(2);
            source2.OnNext(3);

            Assert.That(result, Is.EquivalentTo(new[] { 1, 3 }));
        }

        [Test]
        public void ShouldSwitchToNextWhenFirstFinished()
        {
            var uberSource = new Subject<IObservable<int>>();
            var source1 = new Subject<int>();
            var source2 = new Subject<int>();

            var result = new List<int>();
            var source = uberSource.Switch();
            source.ObserveOn(Scheduler.Immediate).Subscribe(o => result.Add(o));

            uberSource.OnNext(source1);
            source1.OnNext(1);
            source1.OnCompleted();

            uberSource.OnNext(source2);
            source2.OnNext(2);

            Assert.That(result, Is.EquivalentTo(new[] { 1, 2 }));
        }
        [Test]
        public void ShouldDisposePrevSource()
        {
            var uberSource = new Subject<IObservable<int>>();
            var source1 = new Subject<int>();
            var source2 = new Subject<int>();

            var result = new List<int>();
            var source = uberSource.Switch();
            source.ObserveOn(Scheduler.Immediate).Subscribe(o => result.Add(o));

            uberSource.OnNext(source1);
            Assert.That(source1.HasObservers);

            uberSource.OnNext(source2);
            Assert.That(source1.HasObservers, Is.Not.True);
        }

    }
}
