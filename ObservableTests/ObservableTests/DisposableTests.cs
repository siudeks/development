﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Siudek
{
    [TestFixture]
    public sealed class DisposableTests
    {
        [Test]
        public void ShouldNotINvokeDisposeTwice()
        {
            var disposable = new Disposable();
            var disposer = System.Reactive.Disposables.Disposable.Create(disposable.Dispose);
            disposer.Dispose();
            disposer.Dispose();

            Assert.That(disposable.Count, Is.EqualTo(1));
        }

        class Disposable : IDisposable
        {
            public int Count { get; private set; }

            public void Dispose()
            {
                Count++;
            }
        }
    }
}
