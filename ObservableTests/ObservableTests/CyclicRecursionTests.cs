﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Concurrency;
using System.Diagnostics;
using System.Reactive.Disposables;

namespace Net.Siudek
{
    [TestFixture]
    public sealed class CyclicRecursionTests
    {
        [Test]
        public void ShouldSkipCyclicInvocations()
        {
            var source = new Subject<int>();
            var dependency = new Subject<int>();

            var immediateSource =  source.ObserveOn(Scheduler.Immediate);
            var immediateDependency = source.ObserveOn(Scheduler.Immediate);

            int counter = 0;
            var disposer = default(IDisposable);
            var source1Action = default(Action<int>);
            source1Action = o =>
                {
                    disposer.Dispose();
                    counter++;
                    if (o == 10) Assert.Fail();
                    dependency.OnNext(o + 1);
                    disposer = immediateSource.Subscribe(source1Action);
                };
            disposer = immediateSource.Subscribe(source1Action);


            immediateDependency
                .Subscribe(o =>
                {
                    source.OnNext(o + 1);
                });


            source.OnNext(1);

            source.OnNext(101);


            Assert.That(counter, Is.EqualTo(2));
        }
    }
}
