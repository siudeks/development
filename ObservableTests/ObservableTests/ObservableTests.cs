﻿using System;
using System.Reactive.Linq;
using NSubstitute;
using System.Threading;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;
using System.Reactive.Subjects;
using System.Collections.Generic;
using System.Reactive.Disposables;

namespace Net.Siudek
{
    [TestFixture]
    public class ObservableTests
    {
        [Test]
        public void ShouldHandleSubscription()
        {
            const string expectedValue = "published value";
            var observer = Substitute.For<IObserver<string>>();

            var source = new NotificationThroughDelegate();
            Observable.FromEvent<string>(h => source.OnAction += h, h => source.OnAction -= h)
                .Where(o => o != null)
                .Subscribe(observer);

            source.OnAction(expectedValue);

            observer.Received()
                .OnNext(expectedValue);
        }

        [Test]
        public void ShouldNotHandleCancellation()
        {
            var observer = Substitute.For<IObserver<string>>();

            var source = new NotificationThroughDelegate();
            var tokenSource = new CancellationTokenSource();
            Observable.FromEvent<string>(h => source.OnAction += h, h => source.OnAction -= h)
                .Subscribe(observer, tokenSource.Token);

            tokenSource.Cancel();

            Assert.IsNull(source.OnAction);
        }

        [Test]
        public void ShouldNotReceiveDataAfterException()
        {
            var observed = new NotificationThroughEvent<object>();
            var listener = Substitute.For<IObserver<object>>();

            var source =
                Observable.Throw<object>(new Exception())
                .Concat(Observable.Return(new object()));

            source.Subscribe(listener);

            listener.DidNotReceiveWithAnyArgs().OnNext(Arg.Any<object>());
        }

        [Test]
        public void ShouldContinueAfterException()
        {
            var observed = new NotificationThroughEvent<object>();
            var listener = Substitute.For<IObserver<object>>();

            var source =
                Observable.Throw<object>(new Exception())
                .OnErrorResumeNext(Observable.Return(new object()));

            source.Subscribe(listener);

            listener.ReceivedWithAnyArgs().OnNext(Arg.Any<object>());
        }

        [Test]
        public void ShouldHandleOverlappedBuffers()
        {
            var results = new List<int[]>();
            Observable.
                Range(1, 4)
                .Buffer(3, 2)
                .Subscribe(o => results.Add(o.ToArray()));

            Assert.That(results.Count, Is.EqualTo(2));
            CollectionAssert.AreEquivalent(results[0], new[] { 1, 2, 3 });
            CollectionAssert.AreEquivalent(results[1], new[] { 3, 4 });
        }

        [Test]
        public void ShouldHandleOverlappedWindows()
        {
            var results = new List<List<int>>();
            Observable.
                Range(1, 4)
                .Window(3, 1)
                .Subscribe(o =>
                    {
                        var item = new List<int>();
                        results.Add(item);
                        o.Subscribe(i => item.Add(5));
                    });

            Assert.That(results.Count, Is.EqualTo(5));
            CollectionAssert.AreEquivalent(results[0], new[] { 1, 2, 3 });
            CollectionAssert.AreEquivalent(results[1], new[] { 2, 3, 4 });
            CollectionAssert.AreEquivalent(results[2], new[] { 3, 4 });
            CollectionAssert.AreEquivalent(results[3], new[] { 4 });
            CollectionAssert.AreEquivalent(results[4], Enumerable.Empty<int>());
        }

        [Test]
        public void ShouldReceiveCompletedFromManyOnlyWhenAllCOmpletes()
        {
            Assert.Fail("Need to be implemented");
        }

        [Test]
        public void ShouldReceiveExceptionFromManyWhenFirstThrowsException()
        {
            Assert.Fail("Need to be implemented");
        }

        [Test]
        public void ShouldNotCloseSubscriptionAfterSingleReturn()
        {
            var listener = Substitute.For<IObserver<int>>();
            Observable.Create<int>(o =>
            {
                o.OnNext(1);
                return Disposable.Empty;
            })
            .Subscribe(listener);

            listener.Received().OnNext(1);
            listener.DidNotReceive().OnCompleted();
            listener.DidNotReceive().OnError(Arg.Any<Exception>());
        }
    }
}
