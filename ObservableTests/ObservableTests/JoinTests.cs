﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive;

namespace Net.Siudek
{
    [TestFixture]
    public sealed class JoinTests
    {
        [Test]
        public void ShouldJoinWindowsWithPoints()
        {
            var subjectLStart = new Subject<string>();
            var subjectRStart = new Subject<string>();
            var subjectLEnd = new Subject<Unit>();

            var joined = subjectLStart.Join(
                subjectRStart,
                o => subjectLEnd,
                o => Observable.Empty<Unit>(),
                (o1, o2) => new { L = o1, R = o2 });

            var results = new List<string>();
            joined.Subscribe(o =>
                {
                    results.Add(o.L);
                    results.Add(o.R);
                });


            subjectRStart.OnNext("A");

            subjectLStart.OnNext("1");
            subjectRStart.OnNext("B");

            subjectRStart.OnNext("C");

            subjectLEnd.OnNext(Unit.Default);

            subjectRStart.OnNext("D");

            subjectLStart.OnNext("2");
            subjectRStart.OnNext("E");

            Assert.That(results, Is.EquivalentTo(new[] { "1", "B", "1", "C", "2", "E" }));
        }

        [Test]
        public void ShouldRepeatRight()
        {
            var subjectLStart = new Subject<string>();
            var subjectRStart = new Subject<string>();
            var subjectLEnd = new Subject<Unit>();

            var joined = subjectLStart.Join(
                subjectRStart,
                o => subjectLEnd,
                o => Observable.Never<Unit>(),
                (o1, o2) => new { L = o1, R = o2 });

            var results = new List<string>();
            joined.Subscribe(o =>
            {
                results.Add(o.L);
                results.Add(o.R);
            });


            subjectLStart.OnNext("1");
            subjectRStart.OnNext("A");

            subjectLEnd.OnNext(Unit.Default);

            subjectLStart.OnNext("2");
            subjectRStart.OnNext("B");

            Assert.That(results, Is.EquivalentTo(new[] { "1", "A", "2", "A", "2", "B" }));
        }
    }
}
