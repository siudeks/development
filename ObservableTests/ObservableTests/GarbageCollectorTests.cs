﻿using System.Reactive.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace Net.Siudek
{
    [TestFixture]
    public sealed class GarbageCollectorTests
    {
        [Test]
        public void ShouldNotReleaseObserver()
        {
            var observable = new Subject<int>();
            var observer = new Subject<int>();
            var observerRef = new WeakReference<Subject<int>>(observer);
            Assert.That(observerRef.TryGetTarget(out observer), Is.True);

            observable.Select(o => o).Subscribe(observer);

            observer = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();

            Assert.That(observerRef.TryGetTarget(out observer), Is.False);
        }
    }
}
