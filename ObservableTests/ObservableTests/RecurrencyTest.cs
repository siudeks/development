﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace Net.Siudek
{
    [TestFixture]
    public sealed class RecurrencyTest
    {
        [Test]
        public void ShouldSkipCyclicInvocations()
        {
            var subject = new Subject<int>();

            var items = new List<int>();

            subject
                .SubscribeOn(Scheduler.Immediate)
                .Subscribe(o =>
                {
                    if (o >= 10) Assert.Fail();
                    subject.OnNext(o + 1);
                });

            subject.OnNext(0);
            Assert.Pass();
        }

        class SimpleObservable<T> : IObservable<T>
        {
            private readonly IObservable<T> source;
            public SimpleObservable(IObservable<T> source)
            {
                this.source = source;
            }

            public IDisposable Subscribe(IObserver<T> observer)
            {
                return source.SubscribeSafe(new SimpleObserver<T>(source, observer));
            }
        }

        class SimpleObserver<T> : IObserver<T>
        {
            private readonly IObservable<T> observable;
            private readonly IObserver<T> observer;
            public SimpleObserver(IObservable<T> observable, IObserver<T> observer)
            {
                this.observable = observable;
                this.observer = observer;
            }

            public void OnCompleted()
            {
                observer.OnCompleted();
            }

            public void OnError(Exception error)
            {
                observer.OnError(error);
            }

            public void OnNext(T value)
            {
                observer.OnNext(value);
            }
        }
    }
}
