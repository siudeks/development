﻿namespace Net.Siudek
{
    using System;

    public class NotificationThroughEvent<T>
    {
        public event EventHandler<EventArgs<T>> OnEvent;

        public void RaiseEvent(T data)
        {
            OnEvent(this, new EventArgs<T>(data));
        }

    }

    public class EventArgs<T> : EventArgs
    {
        public T Data { get; private set; }

        public EventArgs(T data)
        {
            Data = data;
        }
    }
}
