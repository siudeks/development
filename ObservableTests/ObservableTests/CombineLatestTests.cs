﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Joins;
using System.Reactive.Linq;

namespace Net.Siudek
{
    [TestFixture]
    public sealed class CombineLatestTests
    {
        [Test]
        public void ShouldCombineTwoSources()
        {
            var source1 = new Subject<int>();
            var source2 = new Subject<double>();

            var pattern = source1.CombineLatest(source2, (o1, o2) => o1 + o2);

            var results = new List<double>();

            pattern.Subscribe(o => results.Add(o));

            source1.OnNext(1);
            Assert.That(results, Is.Empty);

            source1.OnNext(2);
            Assert.That(results, Is.Empty);

            source2.OnNext(3);
            Assert.That(results, Is.EquivalentTo( new [] { 5d } ));

            source2.OnNext(4);
            Assert.That(results, Is.EquivalentTo(new[] { 5d, 6d }));
        }
    }
}
