﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;

namespace Net.Siudek
{
    [TestFixture]
    public sealed class SerialDisposableTests
    {
        [Test]
        public void ShouldDisposeWhenChangedUnderlyingDisposable()
        {
            var disposable = new SerialDisposable();

            var disposable1 = Substitute.For<IDisposable>();
            disposable.Disposable = disposable1;
            disposable1.DidNotReceive().Dispose();

            var disposable2 = Substitute.For<IDisposable>();
            disposable.Disposable = disposable2;
            disposable1.Received().Dispose();
            disposable2.DidNotReceive().Dispose();

            var disposable3 = Substitute.For<IDisposable>();
            disposable.Disposable = disposable3;
            disposable2.Received().Dispose();
            disposable3.DidNotReceive().Dispose();

            disposable.Dispose();
            disposable3.Received().Dispose();
        }
    }
}
