﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Threading;
using System.Reactive.Concurrency;

namespace Net.Siudek
{
    [TestFixture]
    public sealed class MergeTests
    {
        [Test]
        public void ShouldMergeStreams()
        {
            var subject1 = new Subject<int>();
            var subject2 = new Subject<int>();

            var merged = subject1.Merge(subject2);

            var items = new List<int>();
            merged
                .ObserveOn(Scheduler.Immediate)
                .Subscribe(o =>
            {
                items.Add(o);
            });

            subject1.OnNext(1);
            subject2.OnNext(2);
            subject2.OnNext(3);
            subject1.OnNext(4);

            Assert.That(items, Is.EquivalentTo(new[] { 1, 2, 3, 4 }));
        }

        [Test]
        public void ShouldContinueOtherStreamWhenFirstIsFinished()
        {
            var subject1 = new Subject<int>();
            var subject2 = new Subject<int>();

            var merged = subject1.Merge(subject2);

            var items = new List<int>();
            merged
                .ObserveOn(Scheduler.Immediate)
                .Subscribe(o =>
                {
                    items.Add(o);
                });

            subject1.OnNext(1);
            subject1.OnCompleted();
            subject2.OnNext(2);
            subject2.OnNext(3);

            Assert.That(items, Is.EquivalentTo(new[] { 1, 2, 3 }));
        }

        [Test]
        public void ShouldNotContinueOtherStreamWhenFirstHasException()
        {
            var subject1 = new Subject<int>();
            var subject2 = new Subject<int>();

            var merged = subject1.Merge(subject2);

            var items = new List<int>();
            merged
                .ObserveOn(Scheduler.Immediate)
                .Subscribe(o => items.Add(o), o => { }, () => { });

            subject1.OnNext(1);
            subject1.OnError(new Exception());
            subject2.OnNext(2);
            subject2.OnNext(3);

            Assert.That(items, Is.EquivalentTo(new[] { 1 }));
        }
    }
}
