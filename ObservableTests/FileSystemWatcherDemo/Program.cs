﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemWatcherDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var watcher = new FileSystemWatcher(path);
            var changes = Observable.FromEventPattern<FileSystemEventArgs>(watcher, "Changed");
            watcher.IncludeSubdirectories = true;
            watcher.EnableRaisingEvents = true;

            var folders =
                from change in changes
                group Path.GetFileName(change.EventArgs.FullPath)
                by Path.GetDirectoryName(change.EventArgs.FullPath);

            folders.Subscribe(f =>
                {
                    Console.WriteLine("New file: {0}", f.Key);
                    f.Subscribe(file => Console.WriteLine("File changed in folder {0}, {1}", f.Key, file));

                });

            Console.WriteLine("Press ENTER to exit");
            Console.ReadLine();
        }
    }
}
