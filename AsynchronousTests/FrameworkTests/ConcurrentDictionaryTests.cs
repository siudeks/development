﻿using NUnit.Framework;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace DictionaryTests
{
    [TestFixture]
    public sealed class ConcurrentDictionaryTests
    {
        [Test, Timeout(100)]
        public void ShouldNoBlockDictionaryInValueFactory()
        {
            var dictionary = new ConcurrentDictionary<int, string>();

            var waitHandle = new CountdownEvent(2);

            Func<string, string> blockingValueFactory = o =>
            {
                waitHandle.Signal();
                waitHandle.Wait();
                return o;
            };

            Action valueGenerator = () => dictionary.GetOrAdd(1, o => blockingValueFactory("a"));

            Task.Run(valueGenerator);

            string value;
            Assert.That(dictionary.TryGetValue(1, out value), Is.False);

            waitHandle.Signal();
            var signaled = waitHandle.Wait(100);

            Assert.IsTrue(signaled);

            Assert.That(dictionary.TryGetValue(1, out value), Is.True);
            Assert.That(value, Is.Not.Null);
        }


    }
}
